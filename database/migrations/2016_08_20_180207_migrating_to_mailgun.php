<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigratingToMailgun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domains', function (Blueprint $table) {
            $table->dropColumn('expire_at');
        });

        Schema::table('campaigns', function (Blueprint $table) {
            $table->integer('recipients_count')->after('domain_id');
        });

        Schema::table('campaigns_spread', function (Blueprint $table) {
            $table->string('mailgun_id')->after('recipient_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
