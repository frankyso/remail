<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dragndrop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->string('type')->after('content_text')->default('EDITOR');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('company_name')->after('password');
        });

        Schema::table('droplets', function (Blueprint $table) {
            $table->string('status')->after('private_ip')->default('ONLINE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->dropColumn('type');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('company_name');
        });

        Schema::table('droplets', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
