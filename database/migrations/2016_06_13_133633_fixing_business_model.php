<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixingBusinessModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('droplets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username');
            $table->string('password');
            $table->string('public_ip');
            $table->string('private_ip');
            $table->timestamps();
        });

        Schema::create('droplets_domain', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('droplet_id');
            $table->integer('domain_id');
            $table->timestamps();
        });

        Schema::table('domains', function (Blueprint $table) {
            $table->dateTime('expire_at')->after('name');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('expire_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('droplets');
        Schema::drop('droplets_domain');
        Schema::table('domains', function (Blueprint $table) {
            $table->dropColumn('expire_at');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dateTime('expire_at')->after('balance');
        });
    }
}
