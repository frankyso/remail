<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smtp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('host');
            $table->string('username');
            $table->string('password');
            
            // $table->string('sender');

            $table->string('reply_to_mail');
            $table->string('reply_to_name');
            $table->string('smtp_secure');
            $table->string('smtp_port');

            $table->string('bounce_host');
            $table->string('bounce_port');
            $table->string('bounce_service');
            $table->string('bounce_option');
            $table->string('bounce_mailbox');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('smtp');
    }
}
