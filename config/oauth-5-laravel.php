<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */

	/**
	 * Consumers
	 */
	'consumers' => [

		'Facebook' => [
			'client_id'     => '',
			'client_secret' => '',
			'scope'         => [],
		],

		'Google' => [
		    'client_id'     => env('GOOGLE_CLIENT_ID','458904039694-3ed6t17gpg6bdrid01glqsv05dip3ida.apps.googleusercontent.com'),
		    'client_secret' => env('GOOGLE_SECRET_ID','iWw634il4FpMswIO0SDCM7ml'),
		    'scope'         => ['userinfo_email', 'userinfo_profile', 'https://www.google.com/m8/feeds/'],
		],

	]

];