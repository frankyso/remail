function countDown()
{
	var counter = 30;
	if(!window.timeoutCounter) window.timeoutCounter = setInterval(function() {
	    if(counter < 0) {
	        $('[data-type="skip-button"]').html("Skip");
    	    $('[data-type="skip-button"]').attr("href", window.skipUrl);
    	    $('[data-type="skip-button"]').removeClass("disabled");
	        window.clearInterval(window.timeoutCounter);
	    } else {
	        $('[data-type="skip-button"]').html(window.waitingText+" ("+ counter.toString() +")");
	        counter--;
	    }
	}, 1000);
}

$(document).ready(function(){
	if (window.campaignType=="FACEBOOK_LIKE") {
		window.fbAsyncInit = function() {
		     FB.init({appId: '', status: true, cookie: true, xfbml: true});
		     FB.Event.subscribe('edge.create', function(href, widget) {
		          document.getElementById("like_form").submit();
		     });

		     countDown();

		     // FB.Event.subscribe('xfbml.render', function(href, widget) {
		     // 		delete window.timeoutCounter;
		     //      	countDown();
		     // });
		};

		(function() {
		     var e = document.createElement('script');
		     e.type = 'text/javascript';
		     e.src = 'https://connect.facebook.net/en_US/all.js';
		     e.async = true;
		     document.getElementById('fb-root').appendChild(e);
		}());
	}
	else
	{
		delete window.timeoutCounter;
		countDown();
	}
});