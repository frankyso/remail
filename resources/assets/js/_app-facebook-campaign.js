$(document).ready(function(){
	$("#autopostURL").blur(function(){
		$("#like-preview").slideUp();
		$(".preview-container").slideDown();
		$("#autopost-preview").slideDown();
		$("#autopost-preview").html("");
		$("#loading-preview").show();
		$.post("https://graph.facebook.com/v1.0/?id="+ encodeURIComponent($(this).val()) +"&scrape=1", function( data ) {
			$("#loading-preview").hide();
			try{
				$('#autopostTitle').val(data.title);
				$('#autopostDescription').val(data.description);
				$('#autopostImageURL').val(data.image[0].url);
				$('#autopostDomain').val(data.url);
				
				$("#autopost-preview").html('<img id="autopost-image-preview" src="'+data.image[0].url+'" alt="" class="img img-responsive"> <h4 id="autopost-preview-title">'+data.title+'</h4> <p id="autopost-preview-description">'+data.description+'</p> <small id="autopost-preview-url">'+data.url+'</small>');
			}catch(e){
				$("#autopost-preview").html('<img id="autopost-image-preview" src="" alt="" class="img img-responsive"> <h4 id="autopost-preview-title">'+data.title+'</h4> <p id="autopost-preview-description">'+data.description+'</p> <small id="autopost-preview-url">'+data.url+'</small>');
			}
		  // $( ".result" ).html( data );
		});
	});

	$("#likeURL").blur(function(){
		$("#autopost-preview").slideUp();
		$(".preview-container").slideDown();
		$("#like-preview").slideDown();
		$("#like-preview").html("");
		$("#loading-preview").show();

		try
		{
			$("#loading-preview").hide();
			$("#like-preview").html('<div class="fb-page" data-href="'+$(this).val()+'" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>');
			FB.XFBML.parse();
		}catch(e)
		{

		}
	});
});