$(document).ready(function(){
	$(".locations").placecomplete().on({
		"placecomplete:selected": function(event,placeResult){
			$("#location_value_container").append("<input type='hidden' name='locations_value["+placeResult.id+"]' class='locations-value-'"+placeResult.id+" value='"+JSON.stringify(placeResult)+"'>");
		},
		"placecomplete:unselected":function(event){
			console.log(event);
			// $(".locations-value-"+placeResult.id).remove();
		}
	});


	/*Dayparts*/
	$("#dayparts-table").dayparts({data: window.dayparts || []});
	$(".dayparts").addClass('table-default').addClass('table-bordered');
	$(".dayparts").find('select').addClass('form-control');
});