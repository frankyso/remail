$(document).ready(function(){
    /*Custom Function Franky So*/
    $('#selectAll').click(function(e){
        var table= $(e.target).closest('table');
        $('td input:checkbox',table).prop('checked',this.checked);
    });


    $('img').each(function(){
        var image = $(this);
        image.error(function(){
            $(this).attr('src', image.data('error'));
        });
    });

    $(".clickable-row").click(function() {
      window.document.location = $(this).data("href");
    });

    $("[data-type='file-preview']").each(function(){
        $(this).change(function(){
            readURL(this, $(this).data('target'));
        });
    });

    if (window.premiumUser==0) {
        $('.premium-feature').click(function(){
            $.toast({
                heading: 'Warning',
                heading: 'Premium Feature',
                text: window.premiumMesssage,
                icon: 'info',
                hideAfter : 5000,
                position: 'bottom-right',
            });
            return false;
        });
    }

    $(".confirm-get").confirm();
    $(".confirm-post").confirm({post:true});

    function readURL(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();            
            reader.onload = function (e) {
                $(target).attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function() {
      $('.scroll-to').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });

    /*Tooltip*/

    $(function () {
      $('[data-toggle="tooltip"]').tooltip({container: '.container'})
    })

    /*Sticky Block*/
    $("#sticker").sticky({topSpacing:15});


    /*Advanced Input*/
    $(":file").filestyle();

    $(".select2").each(function(){        
        $(this).select2({
            theme: "bootstrap",
            tags: $(this).data("tags")||false
        });
    });
    
    $(".select2-place").placecomplete();

    /*TextEditor Block Summernote Block*/
    $("[data-type='textarea']").each(function(){
        $(this).summernote({
            height: 400,
            toolbar: [
                // [groupName, [list of button]]
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['fullscreen', 'codeview']]
            ]
      });
    });

    $(".colorpicker").each(function(){
        $(this).colorpicker({
            colorSelectors: {
                '#777777': '#777777',
                '#337ab7': '#337ab7',
                '#5cb85c': '#5cb85c',
                '#5bc0de': '#5bc0de',
                '#f0ad4e': '#f0ad4e',
                '#d9534f': '#d9534f'
            }
        });
    });

    $('[data-type="fade"]').each(function(){
      $(this).click(function(){
        $($(this).data("in")).slideDown();
        $($(this).data("out")).slideUp();
      });
    });

    $('[data-type="sync"]').each(function(){
      $(this).keyup(function(){
        $($(this).data('target')).text($(this).val());
      });
    });

    /*Date Picker Block*/
    $('.input-daterange').datepicker({
    });
    $('.datepicker').each(function() {
        $(this).datepicker({format: 'yyyy-mm-dd'});
    });


    /*Jquery Knob*/
    $('.dial').each(function () { 
            var elm = $(this);
            var color = elm.attr("data-fgColor");  
            var perc = elm.attr("value");  
            var max = (typeof elm.attr("data-max") === 'undefined') ? 100 : elm.attr("data-max"); elm.attr("");  
            var min = (typeof elm.attr("data-min") === 'undefined') ? 0 : elm.attr("data-min");  
            var unit = (typeof elm.attr("data-unit") === 'undefined') ? "" : elm.attr("data-unit");

            elm.knob({ 
                'value': 0, 
                'min':min,
                'max':max,
                "skin":"tron",
                "readOnly":true,
                "thickness":.1,                 'dynamicDraw': true,                "displayInput":false
            });

            $({value: 0}).animate({ value: perc }, {
                duration: 1000,
                easing: 'swing',
                progress: function () {                  elm.val(Math.ceil(this.value)).trigger('change')
            }
        });

        //circular progress bar color
        $(this).append(function() {
            elm.parent().parent().find('.circular-bar-content').css('color',color);
            elm.parent().parent().find('.circular-bar-content label').text(perc+unit);
        });
    });


    /*analityc with date*/
    $('#analityc').each(function(){
        $(this).highcharts({
            chart: {
                type: 'areaspline'
            },
            title: {
                text: window.analityc.title
            },
            subtitle: {
                text: window.analityc.subtitle
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                min: 0
            },
            tooltip: {
                valueDecimals: 0,
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.0f}'
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },

            credits: {
                        enabled: false
            },
            series: window.analityc.series
        });
    });

    $('.donut-chart').each(function(){
        var currentObject   =   $(this);
        // alert(window['window.donutReturning'])
        currentObject.highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: {
                        enabled: false
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: window[currentObject.data('chart')],
            noData: {
                        style: {
                            // fontWeight: 'bold',
                            // fontSize: '16px',
                            // color: '#333'
                        },
                        useHTML:true
                    },
            lang: {noData: "No Data To Display"}
        });
    });
});



Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };