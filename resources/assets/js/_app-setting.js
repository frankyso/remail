$(document).ready(function(){
	WIFIMUmap.init('map','autocompleteBox',{lat: parseFloat($("#lat").val()), lng: parseFloat($("#lng").val())});
	
	$("#get-current-location").click(function(){
		navigator.geolocation.getCurrentPosition(GetLocation);
		function GetLocation(location) {
			var myLatLng = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
			WIFIMUmap.setMarker(myLatLng);
			WIFIMUmap.setMapCenter(myLatLng);
			WIFIMUmap.setLocationAddress({lat: myLatLng.lat(), lng: myLatLng.lng()});
			WIFIMUmap.setToTextbox({lat: myLatLng.lat(), lng: myLatLng.lng()});
		}

		return false;
	});

	try {
		changePrice();
	}
	catch(err) {
	    
	}

	/*Price*/
   	$('.change-price').on('change', function(){
   		changePrice();
   	});
});

function changePrice()
{
	var session 	= $('#session option:selected').val();
	var contract	= $('#contract option:selected').val();

	
	var price 		= $('#session option:selected').data(contract);

	if (price == 0) {
		price = window.free;
	}
	else
	{
		price = window.currency+price.formatMoney(0)+"<small> / "+window.month+"</small>";
	}

	$("#price-display").html(price);
}


var WIFIMUmap = {
	map:null,
	marker:false,
	location:null,
	autocomplete:null,
	locationAddress:null,
	autocompleteBox:null,

	init:function(map, autocompleteBox ,latLng)
	{
		try {
    		var self = this;
    		this.map 	= map;
    		this.location = latLng;
    		this.autocompleteBox = autocompleteBox;

    		this.map = new google.maps.Map(document.getElementById(this.map), {
    			    center: this.location,
    			    scrollwheel: false,
    			    zoom: 15
    			});

    		this.setMarker(this.location);

    		var options = {
    		  types: ['geocode'],
    		  componentRestrictions: {country: 'id'}
    		};

    		this.autocomplete = new google.maps.places.Autocomplete(document.getElementById(autocompleteBox), options);
    		google.maps.event.addListener(self.autocomplete, 'place_changed', function () {
                var place = self.autocomplete.getPlace();
                self.setMarker(place.geometry.location);
                self.setMapCenter(place.geometry.location);
                self.setToTextbox({
		        		lat: place.geometry.location.lat(),
		        		lng: place.geometry.location.lng()
		        	});
            });
		}
		catch(err) {
		    
		}
	},
	setMarker:function(location){
		var self = this;
		if (this.marker) {
		    this.marker.setPosition(location);
		    self.location = location;
		} else {
		    this.marker = new google.maps.Marker({
		    	position: location,
		      	map: this.map,
		      	draggable:true
		    });

		    google.maps.event.addListener(
		        this.marker,
		        'dragend',
		        function(event) {
		        	location = {
		        		lat: event.latLng.lat(),
		        		lng: event.latLng.lng()
		        	}

		        	self.location = location;
					self.setMapCenter(location);
					self.setLocationAddress(location);
					self.setToTextbox(location);
		        }
		    );
		}
	},
	setMapCenter:function(location){
		this.map.setOptions({
	        center: location,
	        zoom: 15
	    });
	},
	setLocationAddress:function(location){
		// formatted_address
		var self = this;
		$.get("https://maps.googleapis.com/maps/api/geocode/json", { 
			latlng: location.lat+","+location.lng,
			location_type:"ROOFTOP",
			result_type:"street_address",
			key:"AIzaSyCxtzVR8BAw-AW0GsI8-hPiONtcPYyt23Q"
		 } ).done(function( data ) {
		 	self.locationAddress = data.results[0].formatted_address;
		 	$("#"+self.autocompleteBox).val(self.locationAddress);
		});
	},
	setToTextbox:function(location){
		$("#lat").val(location.lat);
		$("#lng").val(location.lng);
	}
}