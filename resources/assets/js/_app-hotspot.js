$(document).ready(function(){
	if ($("#protocol").val()=="dhcp") {
		$('.protocolaffected').prop('readOnly',true);
	} else {
		$('.protocolaffected').prop('readOnly',false);
	};

	$("#protocol").change(function(){
		if ($(this).val()=="dhcp") {
			$('.protocolaffected').prop('readOnly',true);
		} else {
			$('.protocolaffected').prop('readOnly',false);
		};
	});
});