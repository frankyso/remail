@include('merchant._header')
<div class="container mt15">
	<h3>Daftar Penerima : {{$list->name}}</h3>
	<hr>
	<ul class="nav nav-pills">
	  <li><a href="/lists/{{$list_id}}/recipients">Penerima</a></li>
	  <li class="active"><a href="/lists/{{$list_id}}/integrate">Integrasi</a></li>
	  <li><a href="/lists/{{$list_id}}/setting">Pengaturan</a></li>
	</ul>
	<hr>
	<div class="panel panel-default">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Platform</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td width="90%">Google Contact</td>
					<td class="text-right"><span class="label label-default">Segera!</span></td>
				</tr>
				<tr>
					<td>Google Drive</td>
					<td class="text-right"><span class="label label-default">Segera!</span></td>
				</tr>
				<tr>
					<td>Laman Facebook</td>
					<td class="text-right"><span class="label label-default">Segera!</span></td>
				</tr>
				<tr>
					<td>Sales Force</td>
					<td class="text-right"><span class="label label-default">Segera!</span></td>
				</tr>
				<tr>
					<td>Event Brite</td>
					<td class="text-right"><span class="label label-default">Segera!</span></td>
				</tr>
				<tr>
					<td>Zendesk</td>
					<td class="text-right"><span class="label label-default">Segera!</span></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
@include('merchant._footer')