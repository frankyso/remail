@include('merchant._header')
<div class="container mt15">
	<h3>Daftar Penerima : {{$list->name}}</h3>
	<hr>
	<ul class="nav nav-pills">
	  <li class="active"><a href="/lists/{{$list_id}}/recipients">Penerima</a></li>
	  <li><a href="/lists/{{$list_id}}/integrate">Integrasi</a></li>
	  <li><a href="/lists/{{$list_id}}/setting">Pengaturan</a></li>
	</ul>
	<hr>
	<div class="row mb15">
		<div class="col-md-4">
			<div class="btn-group">
				<a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown">Tambahkan Penerima Baru <span class="caret"></span></a>
		    	<ul class="dropdown-menu">
		        	<li><a href="/lists/{{$list_id}}/recipients/create">Tambah Manual</a></li>
		        	<li class="disabled" ><a href="#">Integrated System <small>(Kontak Google, dan lainnya)</small> <span class="label label-danger">Segera!</span></a></li>
		        	<li><a data-toggle="modal" data-target="#import" href="#import">Impor CSV, XLS, XLSX</a></li>
		        </ul>
			</div>
		</div>
		<div class="col-md-8 text-right">
			<form class="form-inline">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Ketikan Kata Kunci" value="{{$q}}" name="q">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
				</div>
				<div class="form-group">
					<div class="btn-group">
						<a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
				    	<ul class="dropdown-menu pull-right">
				        	<li><a href="/lists/{{$list_id}}/recipients/export">Export ke CSV</a></li>
				        	<!-- <li><a href="#" data-toggle="modal" data-target="#import">Import dari CSV</a></li> -->
				        </ul>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Nama</th>
					<th>Email</th>
					<th>Komentar</th>
				</tr>
			</thead>
			<tbody>
				@foreach($recipients as $recipient)
				<tr class="clickable-row" data-href="/lists/{{$list_id}}/recipients/{{$recipient->id}}/edit">
					<td>{{$recipient->name}}</td>
					<td>{{$recipient->email}}</td>
					<td>{{$recipient->comment}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! $recipients->links() !!}
</div>
<form action="/lists/{{$list_id}}/import/excel" class="form" method="post" enctype="multipart/form-data">
<div class="modal fade" tabindex="-1" role="dialog" id="import">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Import dari CSV, XLS, XLSX</h4>
      </div>
      <div class="modal-body">
      		{{ csrf_field() }}
      		{{ method_field('POST') }}
      		<div class="form-group">
      			<label>Berkas</label>
      			<input type="file" name="excel">
      		</div>
      </div>
      <div class="modal-footer">
        <button type="submit" value="1" class="btn btn-primary">Impor Penerima</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>
@include('merchant._footer')