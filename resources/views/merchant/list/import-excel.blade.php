@include('merchant._header')
<div class="container">
	<ul class="nav nav-pills">
	  <li class="active"><a href="/lists/{{$list_id}}/recipients">Penerima</a></li>
	  <li><a href="/lists/{{$list_id}}/integrate">Integrasi</a></li>
	  <li><a href="/lists/{{$list_id}}/setting">Pengaturan</a></li>
	</ul>
	<hr>
	<div class="row mb15">
		<div class="col-md-8 col-md-offset-2">
			<form method="POST" action="/lists/{{$list_id}}/import/excel/{{$filename}}/post">
				{{ csrf_field() }}
				{{ method_field('POST') }}

				<div class="panel panel-primary">
					<div class="panel panel-heading">
						<h4 class="panel-title">Impor penerima dari excel</h4>
						Cocokan kolom kami dengan berkas yang Anda impor
					</div>
					<table class="table table-striped table-bordered">
						<tr>
							<td>
								<div class="form-control">
									Nama
								</div>
							</td>
							<td>
								<select name="name" id="" class="form-control">
									<option value=""></option>
									@foreach($headerRows as $headerRow)
									<option value="{{$headerRow['key']}}">{{$headerRow['value']}}</option>
									@endforeach
								</select>
								{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
							</td>
						</tr>
						<tr>
							<td>
								<div class="form-control">
									Email
								</div>
							</td>
							<td>
								<select name="email" id="" class="form-control">
									@foreach($headerRows as $headerRow)
										<option value="{{$headerRow['key']}}">{{$headerRow['value']}}</option>
									@endforeach
								</select>
								{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
							</td>
						</tr>
						<tr>
							<td>
								<div class="form-control">
									Komentar
								</div>
							</td>
							<td>
								<select name="comment" id="" class="form-control">
									<option value=""></option>
									@foreach($headerRows as $headerRow)
										<option value="{{$headerRow['key']}}">{{$headerRow['value']}}</option>
									@endforeach
								</select>
								{!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
							</td>
						</tr>
					</table>
					<div class="panel-footer">
						<input type="submit" class="btn btn-success" value="Impor Penerima">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')