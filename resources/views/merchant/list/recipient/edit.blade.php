@include('merchant._header')
<div class="container">
	<ul class="nav nav-pills">
	  <li class="active"><a href="/lists/{{$list_id}}/recipients">Penerima</a></li>
	  <li><a href="/lists/{{$list_id}}/integrate">Integrasi</a></li>
	  <li><a href="/lists/{{$list_id}}/setting">Pengaturan</a></li>
	</ul>
	<hr>
	<div class="row mb15">
		<div class="col-md-8 col-md-offset-2">
			<form method="POST" action="/lists/{{$list_id}}/recipients/{{$recipient->id}}">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}

				<h4>Ubah Penerima</h4>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="">Nama</label>
					<input type="text" class="form-control" name="name" value="{{old('name',$recipient->name)}}">
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="">Email</label>
					<input type="text" class="form-control" name="email" value="{{old('email',$recipient->email)}}">
					{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('comment') ? ' has-error' : '' }}">
					<label for="">Komentar</label>
					<input type="text" class="form-control" name="comment" value="{{old('comment',$recipient->comment)}}">
					{!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
					<a href="/lists/{{$list_id}}/recipients/{{$recipient->id}}?_token={{csrf_token()}}&_method=DELETE" class="btn btn-default confirm-post">Hapus</a>
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')