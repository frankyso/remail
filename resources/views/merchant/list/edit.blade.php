@include('merchant._header')
<div class="container">
	<h3>Daftar Penerima : {{$list->name}}</h3>
	<hr>
	<ul class="nav nav-pills">
	  <li><a href="/lists/{{$list->id}}/recipients">Penerima</a></li>
	  <li><a href="/lists/{{$list->id}}/integrate">Integrasi</a></li>
	  <li class="active"><a href="/lists/{{$list->id}}/setting">Pengaturan</a></li>
	</ul>
	<hr>
	<div class="row mb15">
		<div class="col-md-8 col-md-offset-2">
			<form method="POST" action="/lists/{{$list->id}}">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}

				<h4>Daftar Penerima : {{$list->name}}</h4>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="">Judul Daftar Penerima</label>
					<input type="text" class="form-control" name="name" value="{{old('name',$list->name)}}" placeholder="Contoh : Newsletter Universitas Merdeka">
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('listed_reason') ? ' has-error' : '' }}">
					<label for="">Ingatkan penerima bagaimana mereka terdaftar pada daftar ini</label>
					<textarea class="form-control" name="listed_reason" id="" cols="30" rows="3">{{old('listed_reason','Anda menerima pesan ini karena terdaftar newsletter kami',$list->listed_reason)}}</textarea>
					{!! $errors->first('listed_reason', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('company') ? ' has-error' : '' }}">
					<label for="">Nama Perusahaan</label>
					<input type="text" class="form-control" name="company" value="{{old('company',$list->company)}}" placeholder="Remail">
					{!! $errors->first('company', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
					<label for="">Alamat</label>
					<textarea class="form-control" name="address" id="" cols="30" rows="3">{{old('address',$list->address)}}</textarea>
					{!! $errors->first('address', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
					<label for="">Kota</label>
					<input type="text" class="form-control" name="city" value="{{old('city',$list->city)}}" placeholder="Denpasar">
					{!! $errors->first('city', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
					<label for="">No Telp</label>
					<input type="text" class="form-control" name="phone" value="{{old('phone',$list->phone)}}">
					{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('postal_code') ? ' has-error' : '' }}">
					<label for="">Kode Pos (Opsional)</label>
					<input type="text" class="form-control" name="postal_code" value="{{old('postal_code',$list->zip_code)}}">
					{!! $errors->first('postal_code', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('website') ? ' has-error' : '' }}">
					<label for="">Website</label>
					<input type="text" class="form-control" name="website" value="{{old('website',$list->website)}}" placeholder="http://remail.web.id">
					{!! $errors->first('website', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Simpan">
					<a href="/lists/{{$list->id}}?_token={{csrf_token()}}&_method=DELETE" class="btn confirm-post btn-default mr5 btn-sm pull-right">Hapus</a>
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')