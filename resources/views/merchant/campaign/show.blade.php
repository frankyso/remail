@include('merchant._header')
<div class="container mt30">
	<div class="row">
		<div class="col-md-4">
			<a href="/campaigns/{{$campaign->id}}?_token={{csrf_token()}}&_method=DELETE" class="btn confirm-post btn-danger">Hapus</a>
			@if($campaign->status=="WAITING")
			<a href="/campaigns/{{$campaign->id}}?_token={{csrf_token()}}&_method=PATCH&status=SENDING" class="btn btn-primary confirm-post">Mulai Kirim Email</a>
			@endif			
		</div>
		<div class="col-md-2">
			<div class="circular-bar" data-toggle="tooltip" data-placement="top" title="Email Terkirim">
				<input type="text" class="dial" data-fgColor="#18BC9C" data-width="150" data-height="150" data-linecap=round  value="{{$campaign->spread()->sent()->count()}}" data-unit="" data-max="{{$campaign->spread()->count()}}">
				<div class="circular-bar-content">
					<strong>TERKIRIM</strong>
					<label></label>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="circular-bar" data-toggle="tooltip" data-placement="top" title="Email Terbuka">
				<input type="text" class="dial" data-fgColor="#18BC9C" data-width="150" data-height="150" data-linecap=round  value="{{$campaign->spread()->opened()->count()}}" data-unit="" data-max="{{$campaign->spread()->count()}}">
				<div class="circular-bar-content">
					<strong>DIBUKA</strong>
					<label></label>
				</div>
			</div>
		</div>

		<div class="col-md-2">
			<div class="circular-bar" data-toggle="tooltip" data-placement="top" title="Email Bounce">
				<input type="text" class="dial" data-fgColor="#18BC9C" data-width="150" data-height="150" data-linecap=round  value="{{$campaign->spread()->bounced()->count()}}" data-unit="" data-max="{{$campaign->spread()->count()}}">
				<div class="circular-bar-content">
					<strong>BOUNCE</strong>
					<label></label>
				</div>
			</div>
		</div>

		<div class="col-md-2">
			<div class="circular-bar" data-toggle="tooltip" data-placement="top" title="Email Klik">
				<input type="text" class="dial" data-fgColor="#18BC9C" data-width="150" data-height="150" data-linecap=round  value="{{$campaign->spread()->clicked()->count()}}" data-unit="" data-max="{{$campaign->spread()->count()}}">
				<div class="circular-bar-content">
					<strong>DIKLIK</strong>
					<label></label>
				</div>
			</div>
		</div>
	</div>
	
</div>
@include('merchant._footer')