@include('merchant._header')
<div class="container mt15">
	<div class="row mb15">
		<div class="col-md-6">
			<a href="/campaigns/create" class="btn btn-primary">Buat Kampanye Baru</a>
		</div>
		<div class="col-md-6 text-right">
			<form class="form-inline">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Ketikan Kata Kunci" value="{{$q}}" name="q">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
				</div>
			</form>
		</div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Nama</th>
					<th>Penerima</th>
					<th>Template</th>
					<th>Terkirim</th>
					<th>Status</th>
					<th>Dibuat pada</th>
				</tr>
			</thead>
			<tbody>
				@foreach($campaigns as $campaign)
				<tr data-href="/campaigns/{{$campaign->id}}">
					<td>{{$campaign->name}}</td>
					<td>{{$campaign->recipients_count}}</td>
					<td><a href="/templates/{{@$campaign->template->id}}/edit">{{@$campaign->template->name}}</a></td>
					<td>{{$campaign->spread()->sent()->count()}}</td>
					<td width="20%">
						{!!$campaign->status()!!}
						<div class="pull-right">
							{!!$campaign->action()!!}
							<a href="/campaigns/{{$campaign->id}}" class="btn btn-xs btn-default"><span class="fa fa-eye"></span></a>
						</div>
					</td>
					<td>{{$campaign->created_at->format('d M Y')}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! $campaigns->links() !!}
</div>
@include('merchant._footer')