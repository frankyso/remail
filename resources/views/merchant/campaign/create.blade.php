@include('merchant._header')
<div class="container">
	<div class="row mb15">
		<div class="col-md-8 col-md-offset-2">
			<form method="POST" action="/campaigns">
				{{ csrf_field() }}
				{{ method_field('POST') }}

				<h4>Buat Kampanye</h4>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="">Nama Kampanye</label>
					<input type="text" class="form-control" name="name" value="{{old('name')}}">
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('tags') ? ' has-error' : '' }}">
					<label for="">Daftar Penerima</label>
					<select class="form-control select2" name="list">
						@foreach(Auth::user()->lists as $list)
							<option value="{{$list->id}}">{{$list->name}}</option>
						@endforeach
					</select>
					{!! $errors->first('tags', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('template') ? ' has-error' : '' }}">
					<label for="">Template</label>
					<select class="form-control select2" name="template">
						@foreach(Auth::user()->templates as $template)
							<option value="{{$template->id}}">{{$template->name}}</option>
						@endforeach
					</select>
					{!! $errors->first('template', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('sender_email') ? ' has-error' : '' }}">
					<label for="">Email Pengirim</label>
					<div class="row">
					<div class="col-md-8">
						<input type="text" class="form-control" name="sender_email" value="{{old('name')}}">
					</div>
					<div class="col-md-4">
						<select name="domain" class="form-control">
							@foreach(Auth::user()->domains()->active()->get() as $domain)
								<option value="{{$domain->id}}">{{'@'.\App\Http\Utilities\RemailHelper::normalize_url($domain->name)}}</option>
							@endforeach
						</select>
					</div>
					</div>
					{!! $errors->first('sender_email', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('sender_name') ? ' has-error' : '' }}">
					<label for="">Nama Pengirim</label>
					<input type="text" class="form-control" name="sender_name" value="{{old('name')}}">
					{!! $errors->first('sender_name', '<p class="help-block">:message</p>') !!}
				</div>
				<!-- <div class="form-group {{ $errors->has('stmp') ? ' has-error' : '' }}">
					<label for="">Server Email</label>
					<select class="form-control select2" name="smtp">
						@foreach(Auth::user()->smtp as $smtp)
							<option value="{{$smtp->id}}">{{$smtp->name}}</option>
						@endforeach
					</select>
					{!! $errors->first('stmp', '<p class="help-block">:message</p>') !!}
				</div> -->
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Simpan">
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')