    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=582972771771438";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <script src="/js/all.js"></script>
  
    @if(Session::has('toasts'))
    <script type="text/javascript">
        @foreach(Session::get('toasts') as $toast)
          $.toast({
              heading: '{{ $toast['title'] }}',
              text: '{{ $toast['message'] }}',
              icon: '{{$toast['level']}}',
              hideAfter : 5000,
              position: 'top-right',
          });          
        @endforeach
    </script>
    @endif
  </body>
</html>