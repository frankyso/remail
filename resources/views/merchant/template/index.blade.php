@include('merchant._header')
<div class="container mt15">
	<div class="row mb15">
		<div class="col-md-6">
			<a href="/templates/types" class="btn btn-primary">Buat Template Baru</a>
		</div>
		<div class="col-md-6 text-right">
			<form class="form-inline">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Ketikan Kata Kunci" value="{{$q}}" name="q">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
				</div>
			</form>
		</div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th width="25%">Nama Template</th>
					<th>Subject</th>
				</tr>
			</thead>
			<tbody>
				@foreach($templates as $template)
				<tr class="clickable-row" @if($template->type=="DRAGNDROP") data-href="/templates/dragndrop/{{$template->id}}/edit" @else data-href="/templates/{{$template->id}}/edit" @endif>
					<td>{{$template->name}}</td>
					<td>{{$template->subject}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! $templates->links() !!}
</div>
<form action="/templates/import" class="form" method="post" enctype="multipart/form-data">
<div class="modal fade" tabindex="-1" role="dialog" id="import">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Import dari CSV</h4>
      </div>
      <div class="modal-body">
      		{{ csrf_field() }}
      		{{ method_field('POST') }}
      		<div class="form-group">
      			<label>File CSV</label>
      			<input type="file" name="csv">
      			<p class="help-block">Format CSV : Nama, Email, Tag, Komentar</p>
      		</div>
      </div>
      <div class="modal-footer">
        <button type="submit" value="1" class="btn btn-primary">Submit CSV</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>
@include('merchant._footer')