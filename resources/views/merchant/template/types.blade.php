@include('merchant._header')
<div class="container mt15">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Drag And Drop <span class="label label-info ml15">Beta</span>
				</div>
				<div class="panel-body">
					<img src="/images/web/campaign-drag-drop.png" alt="Email Drag And Drop" class="img img-responsive">
				</div>
				<div class="panel-footer">
					<a href="/templates/dragndrop/create" class="btn btn-success btn-block">Buat Template Dengan Drag & Drop</a>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Editor
				</div>
				<div class="panel-body">
					<img src="/images/web/campaign-editor.png" alt="Email Drag And Drop" class="img img-responsive">
				</div>
				<div class="panel-footer">
					<a href="/templates/create" class="btn btn-success btn-block">Buat Template Dengan Editor</a>
				</div>
			</div>
		</div>
	</div>
</div>
@include('merchant._footer')