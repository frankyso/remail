@include('merchant._header')
<div class="container">
	<div class="row mb15">
		<div class="col-md-8">
			<form method="POST" action="/templates">
				{{ csrf_field() }}
				{{ method_field('POST') }}

				<h4>Buat Template</h4>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="">Nama Template</label>
					<input type="text" class="form-control" name="name" value="{{old('name')}}">
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
					<label for="">Subject</label>
					<input type="text" class="form-control" name="subject" value="{{old('subject')}}">
					{!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
					<label for="">Template</label>
					<textarea name="message" id="" cols="30" rows="20" data-type="textarea">{{old('template')}}</textarea>
					{!! $errors->first('message', '<p class="help-block">:message</p>') !!}
				</div>		
				<p class="help-block text-right">Remail Composer akan segera terbit!</p>		
				<div class="form-group">
					<input type="submit" class="btn btn-success">
				</div>
			</form>
		</div>
		<div class="col-md-4">
			<h4><strong>Shortcode</strong></h4>
			<p>{NAME} <br>Nama Penerima</p>
			<p>{UNSUBSCRIBE} : <br> Unsubscribe (Link sudah terkandung dalam teks)</p>
			<p>{UNSUBSCRIBE_URL} <br> https://remail.web.id/...</p>
		</div>
	</div>
</div>

<!-- Modal -->
<form method="POST" action="/templates/dragndrop">
{{ csrf_field() }}
{{ method_field('POST') }}
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Buat Template Drag & Drop</h4>
      </div>
      <div class="modal-body">
      	<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
			<label for="">Nama Template</label>
			<input type="text" class="form-control" name="name" value="{{old('name')}}">
			{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
		</div>
		<div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
			<label for="">Subject</label>
			<input type="text" class="form-control" name="subject" value="{{old('subject')}}">
			{!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
		</div>
      </div>
      <div class="modal-footer">
        <input type="submit" value="Lanjutkan" class="btn btn-primary">
      </div>
    </div>
  </div>
</div>
</form>
@include('merchant._footer')