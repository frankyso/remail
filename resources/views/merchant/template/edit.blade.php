@include('merchant._header')
<div class="container">
	<div class="row mb15">
		<div class="col-md-8">
			<form method="POST" action="/templates/{{$template->id}}">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}

				<h4>Ubah Template</h4>

				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="">Nama Template</label>
					<input type="text" class="form-control" name="name" value="{{old('name',$template->name)}}">
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
					<label for="">Subject</label>
					<input type="text" class="form-control" name="subject" value="{{old('subject',$template->subject)}}">
					{!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
					<label for="">Template</label>
					<textarea name="message" id="" cols="30" rows="20" data-type="textarea">{{old('template',$template->content_html)}}</textarea>
					{!! $errors->first('message', '<p class="help-block">:message</p>') !!}
				</div>				

				<div class="form-group">
					<input type="submit" class="btn btn-success">
					<a href="/templates/{{$template->id}}?_token={{csrf_token()}}&_method=DELETE" class="btn btn-default confirm-post">Hapus</a>
				</div>
			</form>
		</div>
		<div class="col-md-4">
			<h4><strong>Shortcode</strong></h4>
			<p>{NAME} <br>Nama Penerima</p>
			<p>{UNSUBSCRIBE} : <br> Unsubscribe (Link sudah terkandung dalam teks)</p>
			<p>{UNSUBSCRIBE_URL} <br> https://remail.web.id/...</p>
		</div>
	</div>
</div>
@include('merchant._footer')