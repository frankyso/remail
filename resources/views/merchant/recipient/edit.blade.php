@include('merchant._header')
<div class="container">
	<div class="row mb15">
		<div class="col-md-8 col-md-offset-2">
			<form method="POST" action="/recipients/{{$recipient->id}}">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}

				<h4>Ubah Penerima</h4>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="">Nama</label>
					<input type="text" class="form-control" name="name" value="{{old('name',$recipient->name)}}">
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="">Email</label>
					<input type="text" class="form-control" name="email" value="{{old('email',$recipient->email)}}">
					{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('comment') ? ' has-error' : '' }}">
					<label for="">Komentar</label>
					<input type="text" class="form-control" name="comment" value="{{old('comment',$recipient->comment)}}">
					{!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('tags') ? ' has-error' : '' }}">
					<label for="">Tag</label>
					<select class="form-control select2" multiple="multiple" name="tags[]" data-tags=true>
						@foreach(Auth::user()->tags as $tag)
							@if($recipient->tags()->where('recipient_tag_id','=',$tag->id)->count()>0)
								<option value="{{$tag->id}}" selected>{{$tag->name}}</option>
							@else
								<option value="{{$tag->id}}">{{$tag->name}}</option>
							@endif
						@endforeach
					</select>
					<p class="help-block"><a href="/tags">Kelola Tag</a></p>
					{!! $errors->first('tags', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
					<a href="/recipients/{{$recipient->id}}?_token={{csrf_token()}}&_method=DELETE" class="btn btn-default confirm-post">Hapus</a>
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')