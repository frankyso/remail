@include('merchant._header')
<div class="container mt15">
	<div class="row mb15">
		<div class="col-md-4">
			<a href="/recipients/create" class="btn btn-primary">Tambahkan Penerima Baru</a>
		</div>
		<div class="col-md-8 text-right">
			<form class="form-inline">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Ketikan Kata Kunci" value="{{$q}}" name="q">
				</div>
				<div class="form-group">
					<select name="tag" class="form-control">
						<option value="">Semua Tag</option>
						@foreach(Auth::user()->tags as $tag)
							<option @if($tagq==$tag->id) selected @endif value="{{$tag->id}}">{{$tag->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<select name="status" class="form-control">
						<option value="">Semua Status</option>
						<option value="UNVERIFIED" @if($status=='UNVERIFIED') selected @endif>Unverified</option>
						<option value="VALID" @if($status=='VALID') selected @endif>Valid</option>
						<option value="INVALID" @if($status=='INVALID') selected @endif>Invalid</option>
					</select>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
				</div>
				<div class="form-group">
					<div class="btn-group">
						<a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
				    	<ul class="dropdown-menu pull-right">
				        	<li><a href="/recipients/export">Export ke CSV</a></li>
				        	<li><a href="#" data-toggle="modal" data-target="#import">Import dari CSV</a></li>
				        	<li><a href="/tags">Daftar Tag</a></li>
				        </ul>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Nama</th>
					<th>Email</th>
					<th>Tag</th>
					<th>Komentar</th>
				</tr>
			</thead>
			<tbody>
				@foreach($recipients as $recipient)
				<tr class="clickable-row" data-href="/recipients/{{$recipient->id}}/edit">
					<td>{{$recipient->name}}</td>
					<td>{{$recipient->email}}</td>
					<td>
						@foreach($recipient->tags as $tag)
							{{@$tag->tag->name}}, 
						@endforeach
					</td>
					<td>{{$recipient->comment}} <div class="pull-right">{!!$recipient->status()!!}</div></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! $recipients->links() !!}
</div>
<form action="/recipients/import" class="form" method="post" enctype="multipart/form-data">
<div class="modal fade" tabindex="-1" role="dialog" id="import">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Import dari CSV</h4>
      </div>
      <div class="modal-body">
      		{{ csrf_field() }}
      		{{ method_field('POST') }}
      		<div class="form-group">
      			<label>File CSV</label>
      			<input type="file" name="csv">
      			<p class="help-block">Format CSV : Nama, Email, Tag, Komentar</p>
      		</div>
      </div>
      <div class="modal-footer">
        <button type="submit" value="1" class="btn btn-primary">Submit CSV</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</form>
@include('merchant._footer')