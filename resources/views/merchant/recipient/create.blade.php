@include('merchant._header')
<div class="container">
	<div class="row mb15">
		<div class="col-md-8 col-md-offset-2">
			<form method="POST" action="/recipients">
				{{ csrf_field() }}
				{{ method_field('POST') }}

				<h4>Tambah Penerima</h4>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="">Nama</label>
					<input type="text" class="form-control" name="name" value="{{old('name')}}">
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="">Email</label>
					<input type="text" class="form-control" name="email" value="{{old('email')}}">
					{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('comment') ? ' has-error' : '' }}">
					<label for="">Komentar</label>
					<input type="text" class="form-control" name="comment" value="{{old('comment')}}">
					{!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('tags') ? ' has-error' : '' }}">
					<label for="">Tag</label>
					<select class="form-control select2" multiple="multiple" name="tags[]" data-tags=true>
						@foreach(Auth::user()->tags as $tag)
							<option value="{{$tag->id}}">{{$tag->name}}</option>
						@endforeach
					</select>
					<p class="help-block"><a href="/tags">Kelola Tag</a></p>
					{!! $errors->first('tags', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')