@include('merchant._header')
<input type="hidden" id="id" value="{{$template->id}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<div class="liamdnd-container">
	<div class="preview-area">
	@if($template->content_html=="")
		<style>
			html,body
			{
				padding:0px;
				margin: 0px;
			}

			#document h1 , #document h2, #document h3, #document h4, #document table h1 , #document table h2, #document table h3, #document table h4
			{
				font-size: 26px;
				font-family: "Helvetica";
				color: #202020;
				font-style: normal;
				font-weight: bold;
				letter-spacing: normal;
				text-align: left;
			}

			#document table h3
			{
				font-size: 20px;
			}

			#document table h4
			{
				font-size: 18px;
			}

			#document table p
			{
				margin:10px 0;
				padding:0;
				line-height: 1.5;
			}

			#document table a
			{
				color:#202020;
				font-weight: normal;
				text-decoration: underline;				
			}

			#document
			{
				color:#202020;
				font-size: 14px;
				line-height: 1.5;
			}

			#preheader table p, #preheader
			{
				font-size: 12px;
			}

			#preheader table p
			{
				margin: 0px;
			}

			#preheader table a
			{
				color:#656565;
				font-weight: normal;
				text-decoration: underline;
			}

			.target-preheader-style, .target-header-style, .target-body-style, .target-footer-style
			{
				padding:9px 0px;
			}

			.target-footer-style
			{
				background-color: #FAFAFA;
			}
		</style>
		<table id="document" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:Arial; font-size:14px; color:#000; font-weight:normal;">
			<tr>
				<td align="center" class="target-preheader-style" style="font-size:12px; background-color:#f5f5f5;">
					<table border="0" width="600" cellpadding="0" cellspacing="0" id="preheader" class="target-preheader-align">
						<tr>
							<td class="drop-area" data-title="PreHeader">
								<table class="widget-block can-sort" width="100%" id="text-rtTj0">
									<tbody><tr>
										<td class="can-edit">			<table border="0" cellpadding="0" cellspacing="0" data-type="text" class="index" width="100%">
												<tbody><tr>
													<td><div class="target-text text-target-style">
														<p>Gunakan Bagian ini untuk memberikan preview tentang konten Anda</p></div>
														</td>
												</tr>
											</tbody></table>
										<div class="action-button last-remove">
												<span class="fa fa-bars action-sort"></span>
												<span class="fa fa-pencil-square-o action-edit"></span>
												<span class="fa fa-clone action-duplicate"></span>
												<span class="fa fa-trash-o action-delete"></span>
											</div>
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center"  class="target-header-style">
					<table border="0" width="600" cellpadding="0" cellspacing="0" id="header" style="padding-top:9px; padding-bottom:9px;" class="target-header-align">
						<tr>
							<td class="drop-area"  data-title="Header">
							<table class="widget-block can-sort" width="100%">
								<tbody><tr>
									<td class="can-edit">			<table border="0" cellpadding="0" cellspacing="0" data-type="image" class="index container-target-style" width="100%" style="background: grey; color: white; border:none;">
											<tbody><tr>
												<td>
													<img src="http://app.remail.web.id/images/web/eheader.png" alt="Image" class="image-target-style" width="100%">
												</td>
											</tr>
										</tbody></table>
									<div class="action-button last-remove">
											<span class="fa fa-bars action-sort"></span>
											<span class="fa fa-pencil-square-o action-edit"></span>
											<span class="fa fa-clone action-duplicate"></span>
											<span class="fa fa-trash-o action-delete"></span>
										</div>
									</td>
								</tr>
							</tbody></table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" class="target-body-style">
					<table border="0" width="600" cellpadding="0" cellspacing="0" id="body" style="padding-top:9px; padding-bottom:9px;"  class="target-body-align">
						<tr>
							<td class="drop-area"  data-title="Body">
							<table class="widget-block can-sort" width="100%" id="text-gbn8q">
								<tbody><tr>
									<td class="can-edit">			<table border="0" cellpadding="0" cellspacing="0" data-type="text" class="index" width="100%">
											<tbody><tr>
												<td><div class="target-text text-target-style">
													<h1>Waktunya Mendesain Email Anda.</h1><p>Anda saat ini telah menggunakan Drag N Drop Email Builder Remail. untuk menambahkan konten silahkan Drag Blok yang ada di sebelah kanan dan Lepaskan pada halaman ini.</p><p>Anda Siap? Ayo Mulai!</p></div>
													</td>
											</tr>
										</tbody></table>
									<div class="action-button last-remove">
											<span class="fa fa-bars action-sort"></span>
											<span class="fa fa-pencil-square-o action-edit"></span>
											<span class="fa fa-clone action-duplicate"></span>
											<span class="fa fa-trash-o action-delete"></span>
										</div>
									</td>
								</tr>
							</tbody></table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center"  class="target-footer-style">
					<table border="0" width="600" cellpadding="0" cellspacing="0" id="footer" style="padding-top:9px; padding-bottom:9px;"  data-title="Footer">
						<tr>
							<td class="drop-area"  data-title="Footer">
								<table class="widget-block can-sort" width="100%">
									<tbody><tr>
										<td class="can-edit">			<table border="0" cellpadding="0" cellspacing="0" data-type="footer" class="index" width="100%">
												<tbody><tr>
													<td>
														<div class="target-text text-target-style" style="font-size:11px; text-align:center;">
															<p>Copyright © {CURRENT_YEAR} {USER:COMPANY}, All rights reserved.</p>
															<p>Email ini dikirimkan kepada {RECIPIENT:EMAIL}, karena Anda berlangganan newsletter kami. Anda dapat mengubah pengaturan subscribe Anda, atau <a href="{UNSUBSCRIBE_URL}">Unsubscribe</a></p>
														</div>
													</td>
												</tr>
											</tbody></table>
										<div class="action-button last-remove">
												<span class="fa fa-bars action-sort"></span>
												<span class="fa fa-pencil-square-o action-edit"></span>
												<span class="fa fa-clone action-duplicate"></span>
												<span class="fa fa-trash-o action-delete"></span>
											</div>
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		@else
			{!!html_entity_decode($template->content_html)!!}
		@endif
	</div>
	<div class="widget-area pt15 pl15 pr15">
		<div class="widget-editor-document widget-editor">
			<ul class="nav nav-pills nav-justified" role="tablist">
				<li role="presentation" class="active"><a href="#template" aria-controls="profile" role="tab" data-toggle="tab">Info Template</a></li>
				<li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Konten</a></li>
				<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Desain</a></li>
			</ul>	
			<hr>		  
			<div class="tab-content pt15">
				<div role="tabpanel" class="tab-pane active" id="template">
					<div class="form-group">
						<label for="">Nama Template</label>
						<input type="text" class="form-control" value="{{$template->name}}" id="name_template">
					</div>
					<div class="form-group">
						<label for="">Subject Email</label>
						<input type="text" class="form-control" value="{{$template->subject}}" id="subject_email">
					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="home">
					<div class="row">
						<div class="col-md-4 mb15">
							<div class="widget-content-block" data-widget="text">
								<img src="/images/web/liam/text.png" alt="text" class="img" style="width:125px;">
							</div>
						</div>
						<div class="col-md-4 mb15">
							<div class="widget-content-block" data-widget="boxed-text">
								<img src="/images/web/liam/boxed-text.png" alt="text" class="img" style="width:125px;">
							</div>
						</div>
						<div class="col-md-4 mb15">
							<div class="widget-content-block" data-widget="divider">
								<img src="/images/web/liam/divider.png" alt="text" class="img" style="width:125px;">
							</div>
						</div>
						<div class="col-md-4 mb15">
							<div class="widget-content-block" data-widget="button">
								<img src="/images/web/liam/button.png" alt="text" class="img" style="width:125px;">
							</div>
						</div>
						<div class="col-md-4 mb15">
							<div class="widget-content-block" data-widget="image">
								<img src="/images/web/liam/image.png" alt="text" class="img" style="width:125px;">
							</div>
						</div>
						<div class="col-md-4 mb15">
							<div class="widget-content-block" data-widget="footer">
								<img src="/images/web/liam/footer.png" alt="text" class="img" style="width:125px;">
							</div>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="profile">
					@include('merchant.dragndrop.block._style_document')
				</div>
			</div>
		</div>

		<div class="widget-editor-text widget-editor">
			<h4 class="text-center mb20"><strong>TEKS</strong></h4>
			<ul class="nav nav-pills nav-justified" role="tablist">
				<li role="presentation" class="active"><a href="#widget-editor-text-home" aria-controls="widget-editor-text-home" role="tab" data-toggle="tab">Konten</a></li>
				<li role="presentation"><a href="#widget-editor-text-style" aria-controls="widget-editor-text-style" role="tab" data-toggle="tab">Gaya</a></li>
			</ul>	
			<hr>		  
			<div class="tab-content pt15">
				<div role="tabpanel" class="tab-pane active" id="widget-editor-text-home">
					<textarea name="" class="editor need-load-text" data-target='.target-text' id="" cols="30" rows="10"></textarea>
				</div>
				<div role="tabpanel" class="tab-pane" id="widget-editor-text-style">
					@include('merchant.dragndrop.block._style_text')
				</div>
			</div>
			<hr>
			<button class="btn btn-success save-widget btn-sm">Simpan</button>
		</div>

		<div class="widget-editor-boxed-text widget-editor">
			<h4 class="text-center mb20"><strong>TEKS</strong></h4>
			<ul class="nav nav-pills nav-justified" role="tablist">
				<li role="presentation" class="active"><a href="#widget-editor-boxed-text-home" aria-controls="widget-editor-boxed-text-home" role="tab" data-toggle="tab">Konten</a></li>
				<li role="presentation"><a href="#widget-editor-boxed-text-style" aria-controls="widget-editor-boxed-text-style" role="tab" data-toggle="tab">Gaya</a></li>
			</ul>	
			<hr>		  
			<div class="tab-content pt15">
				<div role="tabpanel" class="tab-pane active" id="widget-editor-boxed-text-home">
					<textarea name="" class="editor need-load-text" data-target='.target-text' id="" cols="30" rows="10"></textarea>
				</div>
				<div role="tabpanel" class="tab-pane" id="widget-editor-boxed-text-style">
					@include('merchant.dragndrop.block._style_text')
					<hr>
					@include('merchant.dragndrop.block._style_container')
				</div>
			</div>
			<hr>
			<button class="btn btn-success save-widget btn-sm">Simpan</button>
		</div>

		<div class="widget-editor-divider widget-editor">
			<h4 class="text-center mb20"><strong>DIVIDER</strong></h4>
			@include('merchant.dragndrop.block._style_divider')
			<hr>
			<button class="btn btn-success save-widget btn-sm">Simpan</button>
		</div>

		<div class="widget-editor-image widget-editor">
			<h4 class="text-center mb20"><strong>Image</strong></h4>
			@include('merchant.dragndrop.block._style_image')
			<hr>
			<button class="btn btn-success save-widget btn-sm">Simpan</button>
		</div>

		<div class="widget-editor-footer widget-editor">
			<h4 class="text-center mb20"><strong>Footer</strong></h4>
			@include('merchant.dragndrop.block._style_footer')
			<hr>
			<button class="btn btn-success save-widget btn-sm">Simpan</button>
		</div>

		<div class="widget-editor-button widget-editor">
			<h4 class="text-center mb20"><strong>Button</strong></h4>
			@include('merchant.dragndrop.block._style_button')
			<hr>
			<button class="btn btn-success save-widget btn-sm">Simpan</button>
		</div>
	</div>
</div>
<nav class="navbar navbar-default navbar-static-top mb0">
	<div class="container-fluid">
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li class=""><a id="sendPreview" href="#" data-toggle="modal" data-target="#sendPreviewModal">Kirim Preview</a></li>
				<li class=""><a id="save" href="#">Simpan &amp; Keluar <span class="fa fa-chevron-right"></span></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>

<div class="modal fade" tabindex="-1" role="dialog" id="sendPreviewModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kirim Preview</h4>
      </div>
      <div class="modal-body">
	  	<div class="form-group">
		  	<label for="">Email Anda</label>
		  	<input type="text" id="emailPreview" value="" class="form-control">
		</div>
      </div>
      <div class="modal-footer">        
        <button type="button" class="btn btn-primary" id="sendPreviewButton">Kirim Preview</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@include('merchant.dragndrop.block.text')
@include('merchant.dragndrop.block.button')
@include('merchant.dragndrop.block.boxed-text')
@include('merchant.dragndrop.block.divider')
@include('merchant.dragndrop.block.image')
@include('merchant.dragndrop.block.footer')
@include('merchant._footer')
<script src="/js/dragndrop.js"></script>