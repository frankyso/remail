<div class="hidden-block-container">
	<div class="widget-boxed-text-block">
		@include('merchant.dragndrop.block._block-heading')
			<table border="0" cellpadding="0" cellspacing="0" data-type="boxed-text" class="index container-target-style" width="100%" style="background: grey; color: white; border: 2px solid #000;">
				<tr>
					<td><p class="text-target-style target-text" style="margin:18px; text-align:center;">Ini adalah Block kotak text. Anda dapat menggunakannya untuk menarik perhatian atau mengisinya dengan pesan penting.</p></td>
				</tr>
			</table>
		@include('merchant.dragndrop.block._block-footer')
	</div>
</div>