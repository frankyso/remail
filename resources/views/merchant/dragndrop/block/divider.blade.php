<div class="hidden-block-container">
	<div class="widget-divider-block">
		@include('merchant.dragndrop.block._block-heading')
			<table border="0" cellpadding="0" cellspacing="0" data-type="divider" class="index" width="100%">
				<tr>
					<td style="padding:18px 0px;" class="divider-target-style">
						<table width="100%" class="divider-child-target-style" style="min-width: 100%; border-top-width: 1px; border-top-style: solid; border-top-color: rgb(175, 175, 175);">
							<tr>
								<td><span></span></td>
							</tr>
						</table>	
					</td>
				</tr>
			</table>
		@include('merchant.dragndrop.block._block-footer')
	</div>
</div>