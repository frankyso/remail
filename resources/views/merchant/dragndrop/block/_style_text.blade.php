<h4><strong>Gaya Teks</strong></h4>
<div class="form-group">
	<label for="">Warna Teks</label>
	<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target=".text-target-style">
</div>
<div class="form-group">
	<label for="">Font Family</label>
	<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-family" data-target=".text-target-style">
		<option value="Arial">Arial</option>
		<option value="Comic Sans MS">Comic Sans</option>
		<option value="Courier New">Courier New</option>
		<option value="Georgia">Georgia</option>
		<option value="Helvetica">Helvetica</option>
		<option value="Lucida Sans Unicode">Lucida</option>
		<option value="Tahoma">Tahoma</option>
		<option value="Times New Roman">Times New Roman</option>
		<option value="Trebuchet MS">Trebuchet MS</option>
		<option value="Verdana">Verdana</option>
	</select>
</div>
<div class="form-group">
	<label for="">Ukuran Font</label>
	<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-size" data-target=".text-target-style">
		<option value="9px">9px</option>
		<option value="10px">10px</option>
		<option value="11px">11px</option>
		<option value="12px">12px</option>
		<option value="13px">13px</option>
		<option value="14px">14px</option>
		
		@for ($i=16; $i < 74; $i+=2) 
			<option value="{{$i}}px">{{$i}}px</option>
		@endfor
	</select>
</div>

<div class="form-group">
	<label for="">Ketebalan Font</label>
	<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target=".text-target-style">
		<option value="400">Normal</option>
		<option value="bold">Bold</option>
	</select>
</div>
<div class="form-group">
	<label for="">Text Align</label>
	<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-align" data-target=".text-target-style">
		<option value="left">Rata Kiri</option>
		<option value="center">Rata Tengah</option>
		<option value="right">Rata Kanan</option>
		<option value="justify">Rata Seimbang</option>
	</select>
</div>