<h4><strong>Gaya Penampung</strong></h4>
<div class="form-group">
	<label for="">Latar Belakang</label>
	<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="background-color" data-target=".container-target-style">
</div>
<div class="form-group">
	<label for="">Bentuk Garis Batas</label>
	<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-style" data-target=".container-target-style">
		<option value="none">none</option>
		<option value="hidden">hidden</option>
		<option value="dotted">dotted</option>
		<option value="dashed">dashed</option>
		<option value="solid">solid</option>
		<option value="double">double</option>
		<option value="groove">groove</option>
		<option value="ridge">ridge</option>
		<option value="inset">inset</option>
		<option value="outset">outset</option>
	</select>
</div>
<div class="form-group">
	<label for="">Ketebalan Garis Batas</label>
	<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-width" data-target=".container-target-style">
</div>
<div class="form-group">
	<label for="">Warna Garis Batas</label>
	<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-color" data-target=".container-target-style">
</div>