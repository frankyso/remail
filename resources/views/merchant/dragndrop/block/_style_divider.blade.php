<div class="form-group">
	<label for="">Padding Top</label>
	<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="padding-top" data-target=".divider-target-style">
</div>

<div class="form-group">
	<label for="">Padding Bottom</label>
	<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="padding-bottom" data-target=".divider-target-style">
</div>

<div class="form-group">
	<label for="">Bentuk Garis Batas</label>
	<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-top-style" data-target=".divider-child-target-style">
		<option value="none">none</option>
		<option value="hidden">hidden</option>
		<option value="dotted">dotted</option>
		<option value="dashed">dashed</option>
		<option value="solid">solid</option>
		<option value="double">double</option>
		<option value="groove">groove</option>
		<option value="ridge">ridge</option>
		<option value="inset">inset</option>
		<option value="outset">outset</option>
	</select>
</div>
<div class="form-group">
	<label for="">Ketebalan Garis Batas</label>
	<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-top-width" data-target=".divider-child-target-style">
</div>
<div class="form-group">
	<label for="">Warna Garis Batas</label>
	<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-top-color" data-target=".divider-child-target-style">
</div>