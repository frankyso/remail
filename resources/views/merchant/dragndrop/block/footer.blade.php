<div class="hidden-block-container">
	<div class="widget-footer-block">
		@include('merchant.dragndrop.block._block-heading')
			<table border="0" cellpadding="0" cellspacing="0" data-type="footer" class="index" width="100%">
				<tr>
					<td>
						<div class="target-text text-target-style" style="font-size:11px; text-align:center;">
							<p>Copyright &copy; {CURRENT_YEAR} {USER:COMPANY}, All rights reserved.</p>
							<p>Email ini dikirimkan kepada {RECIPIENT:EMAIL}, karena Anda berlangganan newsletter kami. Anda dapat mengubah pengaturan subscribe Anda, atau <a href="{UNSUBSCRIBE_URL}">Unsubscribe</a></p>
						</div>
					</td>
				</tr>
			</table>
		@include('merchant.dragndrop.block._block-footer')
	</div>
</div>