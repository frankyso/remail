<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingOne">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					Halaman Utama
				</a>
			</h4>
		</div>
		<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body">
				<h3>Heading 1</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target="#document h1" data-action="append" value="#202020">
				</div>
				<div class="form-group">
					<label for="">Font Family</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-family" data-target="#document h1" data-action="append">
						<option value="Arial">Arial</option>
						<option value="Comic Sans MS">Comic Sans</option>
						<option value="Courier New">Courier New</option>
						<option value="Georgia">Georgia</option>
						<option value="Helvetica" selected>Helvetica</option>
						<option value="Lucida Sans Unicode">Lucida</option>
						<option value="Tahoma">Tahoma</option>
						<option value="Times New Roman">Times New Roman</option>
						<option value="Trebuchet MS">Trebuchet MS</option>
						<option value="Verdana">Verdana</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ukuran Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-size" data-target="#document h1" data-action="append">
						<option value="9px">9px</option>
						<option value="10px">10px</option>
						<option value="11px">11px</option>
						<option value="12px">12px</option>
						<option value="13px">13px</option>
						<option value="14px">14px</option>
						
						@for ($i=16; $i < 74; $i+=2) 
							<option value="{{$i}}px">{{$i}}px</option>
						@endfor
					</select>
				</div>

				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target="#document h1" data-action="append">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Text Align</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-align" data-target="#document h1" data-action="append">
						<option value="left">Rata Kiri</option>
						<option value="center">Rata Tengah</option>
						<option value="right">Rata Kanan</option>
						<option value="justify">Rata Seimbang</option>
					</select>
				</div>
				<h3>Heading 2</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target="#document h3" data-action="append" value="#202020">
				</div>
				<div class="form-group">
					<label for="">Font Family</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-family" data-target="#document h3" data-action="append">
						<option value="Arial">Arial</option>
						<option value="Comic Sans MS">Comic Sans</option>
						<option value="Courier New">Courier New</option>
						<option value="Georgia">Georgia</option>
						<option value="Helvetica" selected>Helvetica</option>
						<option value="Lucida Sans Unicode">Lucida</option>
						<option value="Tahoma">Tahoma</option>
						<option value="Times New Roman">Times New Roman</option>
						<option value="Trebuchet MS">Trebuchet MS</option>
						<option value="Verdana">Verdana</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ukuran Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-size" data-target="#document h3" data-action="append">
						<option value="9px">9px</option>
						<option value="10px">10px</option>
						<option value="11px">11px</option>
						<option value="12px">12px</option>
						<option value="13px">13px</option>
						<option value="14px">14px</option>
						
						@for ($i=16; $i < 74; $i+=2) 
							<option value="{{$i}}px">{{$i}}px</option>
						@endfor
					</select>
				</div>

				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target="#document h3" data-action="append">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Text Align</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-align" data-target="#document h3" data-action="append">
						<option value="left">Rata Kiri</option>
						<option value="center">Rata Tengah</option>
						<option value="right">Rata Kanan</option>
						<option value="justify">Rata Seimbang</option>
					</select>
				</div>
				<h3>Heading 3</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target="#document h3" data-action="append" value="#202020">
				</div>
				<div class="form-group">
					<label for="">Font Family</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-family" data-target="#document h3" data-action="append">
						<option value="Arial">Arial</option>
						<option value="Comic Sans MS">Comic Sans</option>
						<option value="Courier New">Courier New</option>
						<option value="Georgia">Georgia</option>
						<option value="Helvetica" selected>Helvetica</option>
						<option value="Lucida Sans Unicode">Lucida</option>
						<option value="Tahoma">Tahoma</option>
						<option value="Times New Roman">Times New Roman</option>
						<option value="Trebuchet MS">Trebuchet MS</option>
						<option value="Verdana">Verdana</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ukuran Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-size" data-target="#document h3" data-action="append">
						<option value="9px">9px</option>
						<option value="10px">10px</option>
						<option value="11px">11px</option>
						<option value="12px">12px</option>
						<option value="13px">13px</option>
						<option value="14px">14px</option>
						
						@for ($i=16; $i < 74; $i+=2) 
							<option value="{{$i}}px">{{$i}}px</option>
						@endfor
					</select>
				</div>

				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target="#document h3" data-action="append">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Text Align</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-align" data-target="#document h3" data-action="append">
						<option value="left">Rata Kiri</option>
						<option value="center">Rata Tengah</option>
						<option value="right">Rata Kanan</option>
						<option value="justify">Rata Seimbang</option>
					</select>
				</div>
				<h3>Heading 4</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target="#document h4" data-action="append" value="#202020">
				</div>
				<div class="form-group">
					<label for="">Font Family</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-family" data-target="#document h4" data-action="append">
						<option value="Arial">Arial</option>
						<option value="Comic Sans MS">Comic Sans</option>
						<option value="Courier New">Courier New</option>
						<option value="Georgia">Georgia</option>
						<option value="Helvetica" selected>Helvetica</option>
						<option value="Lucida Sans Unicode">Lucida</option>
						<option value="Tahoma">Tahoma</option>
						<option value="Times New Roman">Times New Roman</option>
						<option value="Trebuchet MS">Trebuchet MS</option>
						<option value="Verdana">Verdana</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ukuran Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-size" data-target="#document h4" data-action="append">
						<option value="9px">9px</option>
						<option value="10px">10px</option>
						<option value="11px">11px</option>
						<option value="12px">12px</option>
						<option value="13px">13px</option>
						<option value="14px">14px</option>
						
						@for ($i=16; $i < 74; $i+=2) 
							<option value="{{$i}}px">{{$i}}px</option>
						@endfor
					</select>
				</div>

				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target="#document h4" data-action="append">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Text Align</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-align" data-target="#document h4" data-action="append">
						<option value="left">Rata Kiri</option>
						<option value="center">Rata Tengah</option>
						<option value="right">Rata Kanan</option>
						<option value="justify">Rata Seimbang</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					PreHeader
				</a>
			</h4>
		</div>
		<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			<div class="panel-body">
				<h3>Gaya PreHeader</h3>
				<div class="form-group">
					<label for="">Latar Belakang</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="background-color" data-target=".target-preheader-style">
				</div>
				<div class="form-group">
					<label for="">Bentuk Garis Batas Atas</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-top-style" data-target=".target-preheader-style">
						<option value="none">none</option>
						<option value="hidden">hidden</option>
						<option value="dotted">dotted</option>
						<option value="dashed">dashed</option>
						<option value="solid">solid</option>
						<option value="double">double</option>
						<option value="groove">groove</option>
						<option value="ridge">ridge</option>
						<option value="inset">inset</option>
						<option value="outset">outset</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ketebalan Garis Batas Atas</label>
					<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-top-width" data-target=".target-preheader-style">
				</div>
				<div class="form-group">
					<label for="">Warna Garis Batas Atas</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-top-color" data-target=".target-preheader-style">
				</div>
				<div class="form-group">
					<label for="">Bentuk Garis Batas Bawah</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-bottom-style" data-target=".target-preheader-style">
						<option value="none">none</option>
						<option value="hidden">hidden</option>
						<option value="dotted">dotted</option>
						<option value="dashed">dashed</option>
						<option value="solid">solid</option>
						<option value="double">double</option>
						<option value="groove">groove</option>
						<option value="ridge">ridge</option>
						<option value="inset">inset</option>
						<option value="outset">outset</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ketebalan Garis Batas Bawah</label>
					<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-bottom-width" data-target=".target-preheader-style">
				</div>
				<div class="form-group">
					<label for="">Warna Garis Batas Bawah</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-bottom-color" data-target=".target-preheader-style">
				</div>
				<h3>Teks Preheader</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target=".target-preheader-style">
				</div>
				<div class="form-group">
					<label for="">Font Family</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-family" data-target=".target-preheader-style">
						<option value="Arial">Arial</option>
						<option value="Comic Sans MS">Comic Sans</option>
						<option value="Courier New">Courier New</option>
						<option value="Georgia">Georgia</option>
						<option value="Helvetica">Helvetica</option>
						<option value="Lucida Sans Unicode">Lucida</option>
						<option value="Tahoma">Tahoma</option>
						<option value="Times New Roman">Times New Roman</option>
						<option value="Trebuchet MS">Trebuchet MS</option>
						<option value="Verdana">Verdana</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ukuran Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-size" data-target=".target-preheader-style">
						<option value="9px">9px</option>
						<option value="10px">10px</option>
						<option value="11px">11px</option>
						<option value="12px">12px</option>
						<option value="13px">13px</option>
						<option value="14px">14px</option>
						
						@for ($i=16; $i < 74; $i+=2) 
							<option value="{{$i}}px">{{$i}}px</option>
						@endfor
					</select>
				</div>

				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target=".target-preheader-style">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Text Align</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-align" data-target=".target-preheader-align">
						<option value="left">Rata Kiri</option>
						<option value="center">Rata Tengah</option>
						<option value="right">Rata Kanan</option>
						<option value="justify">Rata Seimbang</option>
					</select>
				</div>
				<h3>Link Preheader</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target="#preheader a" data-action="append">
				</div>
				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target="#preheader a"  data-action="append">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Dekorasi Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-decoration" data-target="#preheader a"  data-action="append">
						<option value="underline">Garis Bawah</option>
						<option value="line-through">Coret</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThree">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
					Header
				</a>
			</h4>
		</div>
		<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			<div class="panel-body">
				<h3>Gaya Header</h3>
				<div class="form-group">
					<label for="">Latar Belakang</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="background-color" data-target=".target-header-style">
				</div>
				<div class="form-group">
					<label for="">Bentuk Garis Batas Atas</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-top-style" data-target=".target-header-style">
						<option value="none">none</option>
						<option value="hidden">hidden</option>
						<option value="dotted">dotted</option>
						<option value="dashed">dashed</option>
						<option value="solid">solid</option>
						<option value="double">double</option>
						<option value="groove">groove</option>
						<option value="ridge">ridge</option>
						<option value="inset">inset</option>
						<option value="outset">outset</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ketebalan Garis Batas Atas</label>
					<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-top-width" data-target=".target-header-style">
				</div>
				<div class="form-group">
					<label for="">Warna Garis Batas Atas</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-top-color" data-target=".target-header-style">
				</div>
				<div class="form-group">
					<label for="">Bentuk Garis Batas Bawah</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-bottom-style" data-target=".target-header-style">
						<option value="none">none</option>
						<option value="hidden">hidden</option>
						<option value="dotted">dotted</option>
						<option value="dashed">dashed</option>
						<option value="solid">solid</option>
						<option value="double">double</option>
						<option value="groove">groove</option>
						<option value="ridge">ridge</option>
						<option value="inset">inset</option>
						<option value="outset">outset</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ketebalan Garis Batas Bawah</label>
					<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-bottom-width" data-target=".target-header-style">
				</div>
				<div class="form-group">
					<label for="">Warna Garis Batas Bawah</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-bottom-color" data-target=".target-header-style">
				</div>
				<h3>Teks Header</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target=".target-header-style">
				</div>
				<div class="form-group">
					<label for="">Font Family</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-family" data-target=".target-header-style">
						<option value="Arial">Arial</option>
						<option value="Comic Sans MS">Comic Sans</option>
						<option value="Courier New">Courier New</option>
						<option value="Georgia">Georgia</option>
						<option value="Helvetica">Helvetica</option>
						<option value="Lucida Sans Unicode">Lucida</option>
						<option value="Tahoma">Tahoma</option>
						<option value="Times New Roman">Times New Roman</option>
						<option value="Trebuchet MS">Trebuchet MS</option>
						<option value="Verdana">Verdana</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ukuran Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-size" data-target=".target-header-style">
						<option value="9px">9px</option>
						<option value="10px">10px</option>
						<option value="11px">11px</option>
						<option value="12px">12px</option>
						<option value="13px">13px</option>
						<option value="14px">14px</option>
						
						@for ($i=16; $i < 74; $i+=2) 
							<option value="{{$i}}px">{{$i}}px</option>
						@endfor
					</select>
				</div>

				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target=".target-header-style">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Text Align</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-align" data-target=".target-header-align">
						<option value="left">Rata Kiri</option>
						<option value="center">Rata Tengah</option>
						<option value="right">Rata Kanan</option>
						<option value="justify">Rata Seimbang</option>
					</select>
				</div>
				<h3>Link Header</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target="#header a" data-action="append">
				</div>
				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target="#header a"  data-action="append">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Dekorasi Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-decoration" data-target="#header a"  data-action="append">
						<option value="underline">Garis Bawah</option>
						<option value="line-through">Coret</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFour">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
						Body
					</a>
				</h4>
			</div>
			<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
				<div class="panel-body">
					<h3>Gaya body</h3>
					<div class="form-group">
						<label for="">Latar Belakang</label>
						<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="background-color" data-target=".target-body-style">
					</div>
					<div class="form-group">
						<label for="">Bentuk Garis Batas Atas</label>
						<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-top-style" data-target=".target-body-style">
							<option value="none">none</option>
							<option value="hidden">hidden</option>
							<option value="dotted">dotted</option>
							<option value="dashed">dashed</option>
							<option value="solid">solid</option>
							<option value="double">double</option>
							<option value="groove">groove</option>
							<option value="ridge">ridge</option>
							<option value="inset">inset</option>
							<option value="outset">outset</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Ketebalan Garis Batas Atas</label>
						<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-top-width" data-target=".target-body-style">
					</div>
					<div class="form-group">
						<label for="">Warna Garis Batas Atas</label>
						<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-top-color" data-target=".target-body-style">
					</div>
					<div class="form-group">
						<label for="">Bentuk Garis Batas Bawah</label>
						<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-bottom-style" data-target=".target-body-style">
							<option value="none">none</option>
							<option value="hidden">hidden</option>
							<option value="dotted">dotted</option>
							<option value="dashed">dashed</option>
							<option value="solid">solid</option>
							<option value="double">double</option>
							<option value="groove">groove</option>
							<option value="ridge">ridge</option>
							<option value="inset">inset</option>
							<option value="outset">outset</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Ketebalan Garis Batas Bawah</label>
						<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-bottom-width" data-target=".target-body-style">
					</div>
					<div class="form-group">
						<label for="">Warna Garis Batas Bawah</label>
						<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-bottom-color" data-target=".target-body-style">
					</div>
					<h3>Teks Body</h3>
					<div class="form-group">
						<label for="">Warna Teks</label>
						<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target=".target-body-style">
					</div>
					<div class="form-group">
						<label for="">Font Family</label>
						<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-family" data-target=".target-body-style">
							<option value="Arial">Arial</option>
							<option value="Comic Sans MS">Comic Sans</option>
							<option value="Courier New">Courier New</option>
							<option value="Georgia">Georgia</option>
							<option value="Helvetica">Helvetica</option>
							<option value="Lucida Sans Unicode">Lucida</option>
							<option value="Tahoma">Tahoma</option>
							<option value="Times New Roman">Times New Roman</option>
							<option value="Trebuchet MS">Trebuchet MS</option>
							<option value="Verdana">Verdana</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Ukuran Font</label>
						<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-size" data-target=".target-body-style">
							<option value="9px">9px</option>
							<option value="10px">10px</option>
							<option value="11px">11px</option>
							<option value="12px">12px</option>
							<option value="13px">13px</option>
							<option value="14px">14px</option>
							
							@for ($i=16; $i < 74; $i+=2) 
								<option value="{{$i}}px">{{$i}}px</option>
							@endfor
						</select>
					</div>

					<div class="form-group">
						<label for="">Ketebalan Font</label>
						<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target=".target-body-style">
							<option value="400">Normal</option>
							<option value="bold">Bold</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Text Align</label>
						<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-align" data-target=".target-body-align">
							<option value="left">Rata Kiri</option>
							<option value="center">Rata Tengah</option>
							<option value="right">Rata Kanan</option>
							<option value="justify">Rata Seimbang</option>
						</select>
					</div>
					<h3>Link Body</h3>
					<div class="form-group">
						<label for="">Warna Teks</label>
						<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target="#body a" data-action="append">
					</div>
					<div class="form-group">
						<label for="">Ketebalan Font</label>
						<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target="#body a"  data-action="append">
							<option value="400">Normal</option>
							<option value="bold">Bold</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Dekorasi Font</label>
						<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-decoration" data-target="#body a"  data-action="append">
							<option value="underline">Garis Bawah</option>
							<option value="line-through">Coret</option>
						</select>
					</div>
				</div>
			</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingFive">
			<h4 class="panel-title">
				<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
					Footer
				</a>
			</h4>
		</div>
		<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
			<div class="panel-body">
				<h3>Gaya Footer</h3>
				<div class="form-group">
					<label for="">Latar Belakang</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="background-color" data-target=".target-footer-style">
				</div>
				<div class="form-group">
					<label for="">Bentuk Garis Batas Atas</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-top-style" data-target=".target-footer-style">
						<option value="none">none</option>
						<option value="hidden">hidden</option>
						<option value="dotted">dotted</option>
						<option value="dashed">dashed</option>
						<option value="solid">solid</option>
						<option value="double">double</option>
						<option value="groove">groove</option>
						<option value="ridge">ridge</option>
						<option value="inset">inset</option>
						<option value="outset">outset</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ketebalan Garis Batas Atas</label>
					<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-top-width" data-target=".target-footer-style">
				</div>
				<div class="form-group">
					<label for="">Warna Garis Batas Atas</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-top-color" data-target=".target-footer-style">
				</div>
				<div class="form-group">
					<label for="">Bentuk Garis Batas Bawah</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="border-bottom-style" data-target=".target-footer-style">
						<option value="none">none</option>
						<option value="hidden">hidden</option>
						<option value="dotted">dotted</option>
						<option value="dashed">dashed</option>
						<option value="solid">solid</option>
						<option value="double">double</option>
						<option value="groove">groove</option>
						<option value="ridge">ridge</option>
						<option value="inset">inset</option>
						<option value="outset">outset</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ketebalan Garis Batas Bawah</label>
					<input type="text" class="form-control need-load-style"  data-form-type="text" data-style-type="border-bottom-width" data-target=".target-footer-style">
				</div>
				<div class="form-group">
					<label for="">Warna Garis Batas Bawah</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="border-bottom-color" data-target=".target-footer-style">
				</div>
				<h3>Teks Footer</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target=".target-footer-style">
				</div>
				<div class="form-group">
					<label for="">Font Family</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-family" data-target=".target-footer-style">
						<option value="Arial">Arial</option>
						<option value="Comic Sans MS">Comic Sans</option>
						<option value="Courier New">Courier New</option>
						<option value="Georgia">Georgia</option>
						<option value="Helvetica">Helvetica</option>
						<option value="Lucida Sans Unicode">Lucida</option>
						<option value="Tahoma">Tahoma</option>
						<option value="Times New Roman">Times New Roman</option>
						<option value="Trebuchet MS">Trebuchet MS</option>
						<option value="Verdana">Verdana</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Ukuran Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-size" data-target=".target-footer-style">
						<option value="9px">9px</option>
						<option value="10px">10px</option>
						<option value="11px">11px</option>
						<option value="12px">12px</option>
						<option value="13px">13px</option>
						<option value="14px">14px</option>
						
						@for ($i=16; $i < 74; $i+=2) 
							<option value="{{$i}}px">{{$i}}px</option>
						@endfor
					</select>
				</div>

				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target=".target-footer-style">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Text Align</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-align" data-target=".target-footer-align">
						<option value="left">Rata Kiri</option>
						<option value="center">Rata Tengah</option>
						<option value="right">Rata Kanan</option>
						<option value="justify">Rata Seimbang</option>
					</select>
				</div>
				<h3>Link Footer</h3>
				<div class="form-group">
					<label for="">Warna Teks</label>
					<input type="text" class="form-control colorpicker need-load-style"  data-form-type="text" data-style-type="color" data-target="#footer a" data-action="append">
				</div>
				<div class="form-group">
					<label for="">Ketebalan Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="font-weight" data-target="#footer a"  data-action="append">
						<option value="400">Normal</option>
						<option value="bold">Bold</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Dekorasi Font</label>
					<select name="" id="" class="form-control need-load-style" data-form-type="select" data-style-type="text-decoration" data-target="#footer a"  data-action="append">
						<option value="underline">Garis Bawah</option>
						<option value="line-through">Coret</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>