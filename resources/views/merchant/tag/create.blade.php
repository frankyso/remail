@include('merchant._header')
<div class="container">
	<div class="row mb15">
		<div class="col-md-8 col-md-offset-2">
			<form method="POST" action="/tags">
				{{ csrf_field() }}
				{{ method_field('POST') }}

				<h4>Tambah Tag</h4>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="">Nama</label>
					<input type="text" class="form-control" name="name" value="{{old('name')}}">
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
					<label for="">Deskripsi</label>
					<input type="text" class="form-control" name="description" value="{{old('description')}}">
					{!! $errors->first('description', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')