@include('merchant._header')
<div class="container mt15">
	<div class="row mb15">
		<div class="col-md-6">
			<a href="/tags/create" class="btn btn-primary">Tambahkan Tag Baru</a>
		</div>
		<div class="col-md-6 text-right">
			<form class="form-inline">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Ketikan Kata Kunci" value="{{$q}}" name="q">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
				</div>
			</form>
		</div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Nama</th>
					<th>Deskripsi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($tags as $tag)
				<tr class="clickable-row" data-href="/tags/{{$tag->id}}/edit">
					<td>{{$tag->name}}</td>
					<td>{{$tag->name}}</td>
					<td>{{$tag->description}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! $tags->links() !!}
</div>
@include('merchant._footer')