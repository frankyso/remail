<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Remail App</title>

    <!-- Bootstrap -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="dashboard-background">

  <!-- Modal -->
  <form method="POST" action="/overview">
  {{ csrf_field() }}
  {{ method_field('POST') }}
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Kritik &amp; Saran</h4>
        </div>
        <div class="modal-body">
          <textarea name="saran" id="" cols="30" rows="10" class="form-control"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </div>
      </div>
    </div>
  </div>
  </form>

  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">
          <img src="/images/web/logo-big-white.png" alt="" class="img" height="26">
        </a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="{{ Request::is("overview*") ? 'active' : '' }}"><a href="/overview">Overview</a></li>
          <li class="{{ Request::is("campaigns*") ? 'active' : '' }}"><a href="/campaigns">Kampanye</a></li>
          <li class="{{ Request::is("templates*") ? 'active' : '' }}"><a href="/templates">Template</a></li>
          <!-- <li class="{{ Request::is("recipients*") ? 'active' : '' }}"><a href="/recipients">Penerima</a></li> -->
          <li class="{{ Request::is("lists*") ? 'active' : '' }}"><a href="/lists">Daftar Penerima</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}} <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/settings/profile">Profil</a></li>
              <li><a href="/settings/domains">Domain</a></li>
              <li><a href="/settings/billing">Billing</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="{{\OAuth::consumer('Google', url('settings/billing/invite'))->getAuthorizationUri()}}">Undang Teman</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#" data-toggle="modal" data-target="#myModal">Kritik &amp; Saran</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="/auth/logout">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>