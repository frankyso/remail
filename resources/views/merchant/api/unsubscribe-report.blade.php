<!DOCTYPE html>
<html>
    <head>
        <title>Unsubscribe</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:600" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #091217;
                display: table;
                font-weight: 600;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <h1>Terimakasih!</h1>
            </div>
        </div>
    </body>
</html>
