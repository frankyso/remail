@include('merchant._header')
<div class="container">
	<div class="row mb15">
		<div class="col-md-3">
			@include('merchant.setting._sidebar')
		</div>
		<div class="col-md-9">
			<form method="POST" action="/settings/email">
				{{ csrf_field() }}
				{{ method_field('POST') }}

				<div>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">SMTP untuk Pengiriman</a></li>
						<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">IMAP / POP3 Untuk Bounce</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active pt15" id="home">
							<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="">Nama Koneksi</label>
								<input type="text" class="form-control" name="name" value="{{old('name')}}">
								{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
							</div>

							<div class="form-group {{ $errors->has('host') ? ' has-error' : '' }}">
								<label for="">Alamat SMTP</label>
								<input type="text" class="form-control" name="host" value="{{old('host')}}">
								{!! $errors->first('host', '<p class="help-block">:message</p>') !!}
							</div>

							<hr>

							<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
								<label for="">SMTP Username</label>
								<input type="text" class="form-control" name="username" value="{{old('username')}}">
								<p class="help-block">Email Bounce akan kembali kesini</p>
								{!! $errors->first('username', '<p class="help-block">:message</p>') !!}
							</div>

							<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
								<label for="">SMTP Password</label>
								<input type="text" class="form-control" name="password" value="{{old('password')}}">
								{!! $errors->first('password', '<p class="help-block">:message</p>') !!}
							</div>
							
							<hr>
							
							<!-- <div class="form-group {{ $errors->has('sender') ? ' has-error' : '' }}">
								<label for="">Email Pengirim</label>
								<input type="text" class="form-control" name="sender" value="{{old('sender')}}">
								{!! $errors->first('sender', '<p class="help-block">:message</p>') !!}
							</div> -->

							<div class="form-group {{ $errors->has('reply_to_email') ? ' has-error' : '' }}">
								<label for="">Balas ke [Email]</label>
								<input type="text" class="form-control" name="reply_to_email" value="{{old('reply_to_email')}}">
								{!! $errors->first('reply_to_email', '<p class="help-block">:message</p>') !!}
							</div>

							<div class="form-group {{ $errors->has('reply_to_name') ? ' has-error' : '' }}">
								<label for="">Balas ke [Nama]</label>
								<input type="text" class="form-control" name="reply_to_name" value="{{old('reply_to_name')}}">
								{!! $errors->first('reply_to_name', '<p class="help-block">:message</p>') !!}
							</div>

							<hr>

							<div class="form-group {{ $errors->has('secure') ? ' has-error' : '' }}">
								<label for="">Enkripsi</label>
								<div class="radio">
								  <label>
								    <input type="radio" name="secure" value="tls" checked>
								    <strong>tls</strong> sebagai enkripsi default
								  </label>
								</div>
								<div class="radio">
								  <label>
								    <input type="radio" name="secure" value="ssl">
								    <strong>ssl</strong> dapat digunakan tetapi tidak dianjurkan
								  </label>
								</div>
							</div>

							<div class="form-group {{ $errors->has('port') ? ' has-error' : '' }}">
								<label for="">Port</label>
								<input type="text" class="form-control" name="port" value="{{old('port')}}">
								{!! $errors->first('port', '<p class="help-block">:message</p>') !!}
							</div>

							<div class="form-group">
								<input type="submit" class="btn btn-success">
							</div>
						</div>
						<div role="tabpanel" class="tab-pane pt15" id="profile">
							<div class="form-group {{ $errors->has('bounce_host') ? ' has-error' : '' }}">
								<label for="">Email Server</label>
								<input type="text" class="form-control" name="bounce_host" value="{{old('bounce_host')}}">
								{!! $errors->first('bounce_host', '<p class="help-block">:message</p>') !!}
							</div>
							<div class="form-group {{ $errors->has('bounce_port') ? ' has-error' : '' }}">
								<label for="">Port</label>
								<input type="text" class="form-control" name="bounce_port" value="{{old('bounce_port')}}">
								{!! $errors->first('bounce_port', '<p class="help-block">:message</p>') !!}
							</div>

							<div class="form-group {{ $errors->has('bounce_service') ? ' has-error' : '' }}">
								<label for="">Service</label>
								<div class="radio">
								  <label>
								    <input type="radio" name="bounce_service" value="imap" checked>
								    <strong>imap</strong>
								  </label>
								</div>
								<div class="radio">
								  <label>
								    <input type="radio" name="bounce_service" value="pop3">
								    <strong>pop3</strong>
								  </label>
								</div>
							</div>

							<div class="form-group {{ $errors->has('bounce_option') ? ' has-error' : '' }}">
								<label for="">Bounce Option</label>
								<input type="text" class="form-control" name="bounce_option" value="{{old('bounce_option')}}">
								{!! $errors->first('bounce_option', '<p class="help-block">:message</p>') !!}
							</div>

							<div class="form-group {{ $errors->has('bounce_mailbox') ? ' has-error' : '' }}">
								<label for="">Folder</label>
								<input type="text" class="form-control" name="bounce_mailbox" value="{{old('bounce_mailbox')}}" placeholder="Default : 'Inbox'">
								{!! $errors->first('bounce_mailbox', '<p class="help-block">:message</p>') !!}
							</div>

							<div class="form-group">
								<input type="submit" class="btn btn-success">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')