@include('merchant._header')
<div class="container" data-spy="scroll" data-target=".nav-pills">
     <div class="col-md-3">
          @include('merchant.setting._sidebar')
     </div>
     <div class="col-md-9">
	     <div class="row mb15">
	     	<div class="col-md-6">
	     		<a href="/settings/email/create" class="btn btn-primary">Tambah SMTP Baru</a>
	     	</div>
	     	<div class="col-md-6 text-right">
	     		<form class="form-inline">
	     			<div class="form-group">
	     				<input type="text" class="form-control" placeholder="Ketikan Kata Kunci" value="{{$q}}" name="q">
	     			</div>
	     			<div class="form-group">
	     				<input type="submit" class="btn btn-success">
	     			</div>
	     		</form>
	     	</div>
	     </div>
	     <div class="panel panel-default">
	     	<table class="table table-striped table-bordered">
	     		<thead>
	     			<tr>
	     				<th>Nama Koneksi</th>
	     				<th>Alamat SMTP</th>
	     				<th>SMTP username</th>
	     				<th>SMTP Konfirgurasi Email</th>
	     			</tr>
	     		</thead>
	     		<tbody>
	     			@foreach($smtp as $smtpv)
	     			<tr class="clickable-row" data-href="/settings/email/{{.$smtpv->id}}/edit">
	     				<td>{{$smtpv->name}}</td>
	     				<td>{{$smtpv->host}}</td>
	     				<td>{{$smtpv->username}}</td>
	     				<td>{{$smtpv->smtp_secure}}</td>
	     			</tr>
	     			@endforeach
	     		</tbody>
	     	</table>
	     </div>
	     {!! $smtp->links() !!}
     </div>
</div>
@include('merchant._footer')