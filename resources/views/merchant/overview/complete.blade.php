@include('merchant._header')
	<div class="container mt30">
		<div class="row text-center">
			<div class="col-md-4 mb30">
				<div class="panel @if(!$email) panel-primary @else panel-default @endif">
					<div class="panel-heading">
						Konfirmasikan Email Anda
					</div>
					<div class="panel-body">
						<p>Lakukan konfirmasi email Anda terlebih dahulu, jika Anda belum menerima Email dari kami silahkan tekan tombol dibawah ini</p>
					</div>
					<div class="panel-footer">
						<a href="/auth/resend-confirmation?_token={{csrf_token()}}&_method=POST" class="btn @if(!$email) btn-success @else btn-default @endif btn-block confirm-post">Kirim ulang Email Konfirmasi</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb30">
				<div class="panel @if(!$balance) panel-primary @else panel-default @endif">
					<div class="panel-heading">
						Topup Saldo Anda
					</div>
					<div class="panel-body">
						<p>Lakukan topup terlebih dahulu untuk menyewa server email kami. <br><br></p>
					</div>
					<div class="panel-footer">
						<a href="/settings/billing" class="btn @if(!$balance) btn-success @else btn-default @endif btn-block">Topup Saldo</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb30">
				<div class="panel @if(!$domain) panel-primary @else panel-default @endif">
					<div class="panel-heading">
						Tambahkan Domain Anda
					</div>
					<div class="panel-body">
						<p>Siapkan domain pertama Anda, untuk melakukan pengiriman email ya! <br><br></p>
					</div>
					<div class="panel-footer">
						<a href="/settings/domains/create" class="btn @if(!$domain) btn-success @else btn-default @endif btn-block">Tambah Domain</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@include('merchant._footer')