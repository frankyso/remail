@include('merchant._header')
<div class="container mt30">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-9">
					<div id="analityc"></div>
				</div>
				<div class="col-md-3">
					<h4>Efisiensi Kampanye</h4>
					<hr>
					@foreach($efficiencys as $efficiency)
						<div class="form-group">
							<label>{{$efficiency->name}}</label>
							<div class="progress">
							  <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: {{($efficiency->spread()->clicked()->count() / $efficiency->spread()->count())*100}}%">{{($efficiency->spread()->clicked()->count() / $efficiency->spread()->count())*100}}%</div>
							</div>
						</div>
					@endforeach

					<a href="/campaigns" class="btn btn-block btn-sm btn-primary">
						Lihat Lebih Detail
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="panel status panel-primary">
		        <div class="panel-heading">
		            <h1 class="panel-title text-center">{{$widget['email']}}</h1>
		        </div>
		        <div class="panel-body text-center">                        
		            <strong>Email Database</strong>
		        </div>
		    </div>
		</div>
		<div class="col-md-4">
			<div class="panel status panel-primary">
		        <div class="panel-heading">
		            <h1 class="panel-title text-center">{{$widget['template']}}</h1>
		        </div>
		        <div class="panel-body text-center">                        
		            <strong>Template</strong>
		        </div>
		    </div>
		</div>
		<div class="col-md-4">
			<div class="panel status panel-primary">
		        <div class="panel-heading">
		            <h1 class="panel-title text-center">{{$widget['waiting']}}</h1>
		        </div>
		        <div class="panel-body text-center">                        
		            <strong>Campaign Menunggu</strong>
		        </div>
		    </div>
		</div>
	</div>
</div>
@include('merchant._footer')

<script>
	window.analityc.title 		= 	"Statistik 2016";
	window.analityc.subtitle 	=	"Email Campaign Anda Tahun Ini";
	window.analityc.series 		= 	[{
							            name: 'Disiapkan',
							            data: [
							                @foreach($analytics['prepared'] as $analityc)
							                    [Date.UTC({{$analityc->created_at->format('Y')}}, {{$analityc->created_at->format('m')}}, {{$analityc->created_at->format('d')}}), {{$analityc->number}}],
							                @endforeach
							            ]
							        },
							        {
							            name: 'Terkirim',
							            data: [
							                @foreach($analytics['sent'] as $analityc)
							                    [Date.UTC({{$analityc->created_at->format('Y')}}, {{$analityc->created_at->format('m')}}, {{$analityc->created_at->format('d')}}), {{$analityc->number}}],
							                @endforeach
							            ]
							        }
							        ]
</script>