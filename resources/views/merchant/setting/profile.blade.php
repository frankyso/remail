@include('merchant._header')
<div class="container" data-spy="scroll" data-target=".nav-pills">
     <div class="col-md-3">
          @include('merchant.setting._sidebar')
     </div>
     <div class="col-md-9">
          <form method="POST" action="/settings/profile/1" enctype="multipart/form-data">
               {{ csrf_field() }}
               {{ method_field('PATCH') }}
               <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="name" value="{{$user->name}}">
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
               </div>
               <div class="form-group">
                    <label>Email</label>
                    <div class="form-control">{{$user->email}}</div>
               </div>
               <hr>
               <h4>Ganti Password</h4>
               <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" >
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
               </div>

               <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label>Konfirmasi Password</label>
                    <input type="password" class="form-control" name="password_confirmation" >
                    {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
               </div>
               
               <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Simpan">
               </div>
          </form>
     </div>
</div>
@include('merchant._footer')