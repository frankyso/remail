@include('merchant._header')
<div class="container" data-spy="scroll" data-target=".nav-pills">
     <div class="row mb15">
          <div class="col-md-3">
               @include('merchant.setting._sidebar')
          </div>
          <div class="col-md-9">
               <div class="row">
                    <div class="col-md-6">
                         <h4>Saldo Anda</h4>
                         <div class="pull-left mr15">
                              <h3 class="font-weight-bold text-warning mb5 mt0">Rp. {{number_format(Auth::user()->balance)}}</h3>
                         </div>
                         <div class="pull-left">
                              <a href="#" class="btn btn-sm btn-success" data-toggle="collapse" data-target="#add-balance">Tambahkan Saldo</a>
                         </div>
                    </div>
                    <div class="col-md-6">
                         <h4>100 Email Gratis</h4>
                         <p>Undang teman Anda untuk menggunakan Remail, dapatkan email 100 email gratis untuk setiap teman Anda yang melakukan pendaftaran</p>
                         <div class="clearfix"></div>
                         <a href="{{\OAuth::consumer('Google', url('settings/billing/invite'))->getAuthorizationUri()}}" class="btn btn-sm btn-success">Undang Teman</a>
                    </div>
               </div>
               <div class="mt25"></div>

               @if(strtolower(Request::get('status'))=='pending')
                    <div class="alert alert-info">
                         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <strong>Pending!</strong> We have received your trasaction information, please check your email for about payment information
                    </div>
               @endif

               @if(strtolower(Request::get('status'))=='berhasil')
                    <div class="alert alert-success">
                         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <strong>Success!</strong> Balance Topup success, have a nice day!
                    </div>
               @endif
               
               <div class="panel panel-primary collapse @if($errors->has('balance')) in @endif" id="add-balance">
                    <div class="panel-heading">
                         <h3 class="panel-title">Tambah Saldo (Prepaid)</h3>
                    </div>
                    <div class="panel-body">
                         <div class="row">
                              <div class="col-md-6">
                                   <p>Ini adalah pembayaran awal. Pembayaran mungkin membutuhkan waktu sampai 5 menit untuk dapat diselesaikan.</p>
                                   <p class="help-block"><small class="mr5">Powered By</small><img src="/images/web/ipaymu.png" alt="Logo iPaymu" width="30%"></p>
                              </div>
                              <div class="col-md-6">
                                   <form action="/setting/billing/addbalance" class="form-inline pull-right" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('POST') }}
                                        <div class="form-group">
                                             <select name="ammount" class="form-control">
                                                  <option value="50000">50.000</option>
                                                  <option value="100000">100.000</option>
                                                  <option value="300000">300.000</option>
                                                  <option value="500000">500.000</option>
                                                  <option value="1000000">1.000.000</option>
                                                  <option value="2500000">2.500.000</option>
                                                  <option value="5000000">5.000.000</option>
                                             </select>
                                        </div>
                                        <div class="form-group">
                                             <input type="submit" class="btn btn-default" value="Submit">
                                        </div>
                                   </form>
                              </div>
                         </div>
                    </div>
               </div>
               
               <hr>
               <h4>Riwayat</h4>
               <div class="panel panel-default">
                    <table class="table table-striped table-bordered">
                         <thead>
                              <tr>
                                   <th>Date</th>
                                   <th>Description</th>
                                   <th>Ammount</th>
                              </tr>
                         </thead>
                         <tbody>
                              @foreach(Auth::user()->transactions()->orderBy('created_at','desc')->orderBy('id','desc')->get() as $transaction)
                              <tr>
                                   <td>{{$transaction->created_at->format("d M Y")}}</td>
                                   <td>{{$transaction->getDescription()}}</td>
                                   <td>{!!$transaction->getAmmount()!!} <div class="pull-right">{!!$transaction->getExtra()!!}</div></td>
                              </tr>
                              @endforeach
                         </tbody>
                    </table>
               </div>
          </div>
     </div>
</div>
@include('merchant._footer')