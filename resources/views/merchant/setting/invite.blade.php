@include('merchant._header')
<div class="container mb30 mt15">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <form action="" method="post">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <input type="submit" value="Kirim Undangan" class="btn btn-success">
                    <hr>
                    <table class="table table-striped table-bordered">
                      <thead>
                        <th style="width:20px;"><input type="checkbox" id="selectAll" class="mr15"> Email</th>
                      </thead>
                      <tbody>
                         @foreach($emails as $email)
                         <tr>
                              <td><input type="checkbox" name="emails[]" value="{{$email}}" class="mr15">{{str_limit($email,40)}}</td>
                         </tr>
                         @endforeach
                      </tbody>
                    </table>
               </form>
          </div>
     </div>
</div>
@include('merchant._footer')