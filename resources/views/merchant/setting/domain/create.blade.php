@include('merchant._header')
<div class="container mt15">
	<div class="row">
		<div class="col-md-3">
			@include('merchant.setting._sidebar')
		</div>
		<div class="col-md-9">
			<form method="POST" action="/settings/domains">
				{{ csrf_field() }}
				{{ method_field('POST') }}

				<h4>Tambah Domain</h4>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="">Domain</label>
					<input type="text" class="form-control" name="name" value="{{old('name')}}">
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success">
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')