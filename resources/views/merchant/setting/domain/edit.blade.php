
@include('merchant._header')
<div class="container mt15">
	<div class="row">
		<div class="col-md-3">
			@include('merchant.setting._sidebar')
		</div>
		<div class="col-md-9">
			<h4><strong>Domain : {{strtoupper(old('name',$domain->name))}}</strong> 

			<a href="/settings/domains/{{$domain->id}}?_token={{csrf_token()}}&_method=DELETE" class="btn btn-default pull-right btn-sm confirm-post">Hapus</a>
			</h4>
			{!!$domain->status()!!}
			
			<hr>
			@if(count($dns) > 0)
			<div id="Suggest" class="collapse in">
				<h4><strong>Informasi Pengaturan DNS</strong></h4>
				<p>Ubah atau ikuti pengaturan dibawah ini untuk melakukan integrasi</p>
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<tr>
							<th>Type</th>
							<th>Name</th>
							<th>Value</th>
						</tr>
						@foreach($dns as $dnss)
						<tr>
							<td>{{$dnss->record_type}}</td>
							<td>{{$dnss->name}}</td>
							<td><textarea class="form-control" name="" id="" cols="30" rows="3" readonly="">{{$dnss->value}}</textarea></td>
						</tr>
						@endforeach
					</table>
				</div>
				Bingung cara mengubah DNS? <a target="_blank" href="https://kb.qwords.com/2012/04/bagaimana-cara-merubah-dns-zone-mx-cname-dan-a-record/">Klik Disini</a>

				<div class="alert alert-info mb15 mt15">
					Jika Anda telah mengikuti semua petunjuk diatas, silahkan menunggu email konfirmasi bahwa domain telah diverifikasi. proses ini akan membutuhkan waktu kurang dari 1 jam.
				</div>

				<p>Anda tidak sabar menunggu? <a href="/settings/domains/{{$domain->id}}/edit?check=true" class="btn btn-primary btn-sm ml5">Cek Pengaturan DNS</a></p>
			</div>
			@endif			

			<form method="POST" action="/settings/domains/{{$domain->id}}" class="collapse" id="Extend">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<h4><strong>Perpanjang / Tambah Server</strong></h4>
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label for="">Sewa Selama</label>
							<select name="months" id="" class="form-control">
								<option value="1">1 Bulan</option>
								<option value="3">3 Bulan</option>
								<option value="6">6 Bulan</option>
								<option value="12">1 Tahun</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Jumlah Server</label>
							<select name="droplets" id="" class="form-control">
								@for($i=1; $i < 11; $i++)
								<option @if($i==$domain->droplets()->count()) selected @endif value="{{$i}}">{{$i}} Server</option>
								@endfor
							</select>
						</div>
						<div class="form-group">
							<a href="#" class="btn btn-default" data-toggle="collapse" data-target="#Suggest, #Extend">Batal</a>
							<input type="submit" value="submit" class="btn btn-success">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@include('merchant._footer')