@include('merchant._header')
<div class="container mt15">
	<div class="row">
		<div class="col-md-3">
			@include('merchant.setting._sidebar')
		</div>
		<div class="col-md-9">
			<div class="row mb15">
				<div class="col-md-6">
					<a href="/settings/domains/create" class="btn btn-primary">Tambahkan Domain Baru</a>
				</div>
				<div class="col-md-6 text-right">
					<form class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Ketikan Kata Kunci" value="{{$q}}" name="q">
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>
			<div class="panel panel-default">
				<table class="table table-striped table-bordered" width="100%">
					<thead>
						<tr>
							<th>Domain</th>
						</tr>
					</thead>
					<tbody>
						@foreach($domains as $domain)
						<tr class="clickable-row" data-href="/settings/domains/{{$domain->id}}/edit">
							<td>{{$domain->name}} <span class="pull-right">{!!$domain->status()!!}</span></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			{!! $domains->links() !!}
		</div>
	</div>
</div>
@include('merchant._footer')