<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title>Oops! Ada Kesalahan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!-- Libs CSS -->
    <link type="text/css" media="all" href="/images/web/error/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Template CSS -->
    <link type="text/css" media="all" href="/images/web/error/css/style.css" rel="stylesheet" />
    <!-- Responsive CSS -->
    <link type="text/css" media="all" href="/images/web/error/css/respons.css" rel="stylesheet" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="144x144" href="/images/web/error/img/favicons/favicon144x144.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/images/web/error/img/favicons/favicon114x114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/images/web/error/img/favicons/favicon72x72.png" />
    <link rel="apple-touch-icon" href="/images/web/error/img/favicons/favicon57x57.png" />
    <link rel="shortcut icon" href="/images/web/error/img/favicons/favicon.png" />
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>

</head>
<body>

    <!-- Load page -->
    <div class="animationload">
        <div class="loader">
        </div>
    </div>
    <!-- End load page -->


    <!-- Content Wrapper -->
    <div id="wrapper">
        <div class="container">
            <div class="col-xs-12 col-sm-7 col-lg-7">
                <!-- Info -->
                <div class="info">
                    <h1>Aww!</h1>
                    <h2>Ada Kesalahan</h2>
                    <p>Halaman yang ingin Anda akses tidak ditemukan, atau telah dipindahkan</p>
                    <a href="/" class="btn">Home</a>
                    <a href="http://www.remail.web.id/hubungi-kami/" class="btn btn-light-blue" target="_blank">Hubungi Kami</a>
                </div>
                <!-- end Info -->
            </div>

            <div class="col-xs-12 col-sm-5 col-lg-5 text-center">
                <!-- Guardian -->
                <div class="guardian">
                    <img src="/images/web/error/img/guardian.gif" alt="Guardian" />
                </div>
                <!-- end Guardian -->
            </div>

        </div>
        <!-- end container -->
    </div>
    <!-- end Content Wrapper -->


    <!-- Scripts -->
    <script src="/images/web/error/js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="/images/web/error/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/images/web/error/js/modernizr.custom.js" type="text/javascript"></script>
    <script src="/images/web/error/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="/images/web/error/js/scripts.js" type="text/javascript"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</body>
</html>