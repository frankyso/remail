@include('auth._header')
<div class="container container-table">
    <div class="row vertical-center-row">
        <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="text-center col-md-4 col-md-offset-4">
            	<img src="/images/web/logo-big-white.png" alt="" class="img mb40" width="200">
            	<h3 class="text-left mb20 text-white font-weight-light"><strong>Halo.</strong> Silahkan Login</h3>
            	<div class="form-group @if($errors->has('email')) has-error @endif">
            		<input type="text" class="form-control" placeholder="Email" name="email">
            	</div>
            	<div class="form-group">
            		<input type="password" class="form-control" placeholder="Password" name="password">
            	</div>
            	<div class="form-group">
            		<input type="submit" value="Login" class="btn btn-success btn-block">
            	</div>
            	<div class="form-group">
            		<a class="pull-left text-white" href="/auth/forgot"><small>Lupa Password?</small></a>
            		<a href="/auth/register" class="pull-right text-white"><small>Saya tidak punya akun, Register!</small></a>
            	</div>
            </div>
        </form>
    </div>
</div>
@include('auth._footer')