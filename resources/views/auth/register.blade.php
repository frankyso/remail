@include('auth._header')
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="container container-table">
    <div class="row vertical-center-row">
        <form action="/auth/register" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="text-center col-md-6 col-md-offset-3">
            	<img src="/images/web/logo-big-white.png" alt="" class="img mb40" width="200">
            	<h3 class="text-left mb20 text-white font-weight-light"><strong>Tehe!</strong> Buat Akun saya</h3>
            	<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input type="text" class="form-control" placeholder="Fullname" name="name" value="{{old('name')}}">
                    {!! $errors->first('name', '<p class="help-block text-left">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            		<input type="text" class="form-control" placeholder="Email" name="email" value="{{old('email', $email)}}">
                    {!! $errors->first('email', '<p class="help-block text-left">:message</p>') !!}
            	</div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            {!! $errors->first('password', '<p class="help-block text-left">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('confirmation') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password Confirmation" name="confirmation">
                            {!! $errors->first('confirmation', '<p class="help-block text-left">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Recaptcha::render() !!}
                </div>
                <p class="text-white text-left"><small>Dengan melakukan register Anda telah setuju dengan <a href="http://www.remail.web.id/syarat-ketentuan/" target="_blank" class="text-white"><u>Syarat &amp; Ketentuan</u></small></a></p>
                @if($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <p>{{$errors->first()}}</p>
                </div>
                @endif
            	<div class="form-group">
            		<input type="submit" value="Register" class="btn btn-success btn-block">
            	</div>
            	<div class="form-group">
            		<a class="pull-left text-white" href="/auth/login"><small>Saya punya akun, Kembali Login</small></a>
            	</div>
            </div>
        </form>
    </div>
</div>
@include('auth._footer')