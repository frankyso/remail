@include('auth._header')
<div class="container container-table">
    <div class="row vertical-center-row">
        <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="text-center col-md-4 col-md-offset-4">
                
            	<img src="/images/web/logo-big-white.png" alt="" class="img mb40" width="200">
            	<h3 class="text-left mb20 text-white font-weight-light"><strong>Aduh!</strong> Lupa password!</h3>
            	<div class="form-group">
            		<input type="text" class="form-control" placeholder="Enter your email" name="email">
            	</div>
            	<div class="form-group">
            		<input type="submit" value="Login" class="btn btn-success btn-block">
            	</div>
            	<div class="form-group">
            		<a class="pull-left text-white" href="/auth/login"><small>Aku ingat!, Kembali</small></a>
            	</div>
                </form>
            </div>
        </form>
    </div>
</div>
@include('auth._footer')