@include('auth._header')
<div class="container container-table">
    <div class="row vertical-center-row">
        <form action="" method="POST">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="text-center col-md-6 col-md-offset-3">
            	<img src="/images/web/logo-big-white.png" alt="" class="img mb40" width="200">
            	<h3 class="text-left mb20 text-white font-weight-light"><strong>Ea!</strong> Reset Your Password</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            {!! $errors->first('password', '<p class="help-block text-left">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password Confirmation" name="password_confirmation">
                            {!! $errors->first('password_confirmation', '<p class="help-block text-left">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <input type="hidden" name="token" value="{{$token}}">
                <input type="hidden" name="email" value="{{$email}}">
            	<div class="form-group">
            		<input type="submit" value="Reset" class="btn btn-success btn-block">
            	</div>
            	<div class="form-group">
            		<a class="pull-left text-white" href="/auth/login"><small>I have an account, back to Login page</small></a>
            	</div>
            </div>
        </form>
    </div>
</div>
@include('auth._footer')