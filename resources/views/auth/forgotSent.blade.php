@include('auth._header')
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="container container-table">
    <div class="row vertical-center-row">
        <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="col-md-6 col-md-offset-3">
            	<img src="/images/web/logo-big-white.png" alt="" class="img mb40" width="200">
            	<h3 class="text-left mb20 text-white font-weight-light"><strong>Yes!</strong> Link Password Terkirim!</h3>
            	<div class="alert alert-success">
            	  Kami telah mengirimkan link reset password ke email Anda!
            	</div>
            	<div class="form-group">
            		<a class="pull-left text-white" href="/auth/login"><small>Kembali kehalaman login</small></a>
            	</div>
            </div>
        </form>
    </div>
</div>
@include('auth._footer')