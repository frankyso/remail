<p>Terimakasih telah menggunakan Remail sebagai layanan email marketing Anda.</p>
<p>Kami informasikan kepada Anda bahwa pengiriman Campaign {{$campaign}} telah kami hentikan, dikarenakan beberapa hal.</p>
<ol>
	<li>Anda belum mengatur DNS Anda</li>
	<li>Cek Saldo Anda</li>
</ol>