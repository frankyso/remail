<p>Terimakasih telah menggunakan Remail sebagai layanan email marketing Anda.</p><p><br></p><p>kami informasikan bahwa dedicated IP yang kami berikan kepada domain Anda telah kami cabut, dikarenakan domain yang belum terbayarkan.</p><p><br></p>

<table>
	<tbody>
		<tr>
			<td>Domain</td>
			<td>{{$domain}}</td>
		</tr>
		<tr>
			<td>IP</td>
			<td>{{$droplet}}</td>
		</tr>
	</tbody>
</table>

<p><br></p>
<p>Anda dapat menggunakan domain ini lagi setelah melakukan pembayaran, dan kami akan memberikan Dedicated IP baru kepada Anda.</p><p><br></p><p>Note : Anda harus melakukan re-integrasi terhadap IP baru teresebut.</p>