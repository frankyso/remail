<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'recipients_tag';
    public $timestamps = false;

    public function recipients()
    {
    	return $this->hasMany('App\RecipientTag','recipient_tag_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User','user_id');
    }
}
