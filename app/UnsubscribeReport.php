<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnsubscribeReport extends Model
{
    protected $table = 'unsubscribe_report';
}

