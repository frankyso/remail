<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ValidateEmailStatus extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $recipient_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($recipient_id)
    {
        $this->recipient_id  =   $recipient_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $recipient  =   \App\Recipient::find($this->recipient_id);
        if(!$recipient)
        {
            return true;
        }
        
        try {
            $response = \cURL::get('http://techstroke.com/email-availability-checker/?email='.$recipient->email);
            $result = $response->body;
        } catch (\Exception $e) {
            $result = "Already Exists";
        }
        if (str_contains($result,"Already Exists")) {
            $recipient->status  =   "VALID";
            $recipient->save();
            var_dump($recipient->email." ".$recipient->status);
        }           
        if (str_contains($result,"No such Email address exists")) {
            $recipient->status  =   "INVALID";
            $recipient->save();
            var_dump($recipient->email." ".$recipient->status);
            $recipient->delete();
        }
    }
}
