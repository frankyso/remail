<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendingEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $domain;
    protected $data;
    protected $campaign_id;
    protected $recipient_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($domain, $data, $campaign_id, $recipient_id)
    {
        $this->domain           =   $domain;
        $this->data             =   $data;
        $this->campaign_id      =   $campaign_id;
        $this->recipient_id     =   $recipient_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /*Setting The Right one and dont make duplicate*/
        var_dump("Procecing ".$this->campaign_id."_".$this->recipient_id);

        if (\App::environment('local')) {
            echo "EMAIL SENT";
            exit;
        }

        $campaignSpread     =   \App\CampaignSpread::where('campaign_id','=',$this->campaign_id)->where('recipient_id','=',$this->recipient_id)->first();
        if (!$campaignSpread) {
            return true;
        }

        // Check if has sent
        if ($campaignSpread->status == "MAILGUN_QUEUE") {
            return true;
        }

        try {
            $request = \cURL::newRequest('post', 'https://api.mailgun.net/v3/'.$this->domain.'/messages', $this->data)->setUser('api')->setPass(config('services.mailgun.secret'));

            $response = $request->send();
            $response = json_decode($response->body);

            \App\CampaignSpread::where('campaign_id','=',$this->campaign_id)
                        ->where('recipient_id','=',$this->recipient_id)
                        ->update(['status' => 'MAILGUN_QUEUE','mailgun_id' => $response->id,'sent_at'=>date("Y-m-d H:i:s")]);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        /*Mengecek apakah ini adalah data yang terakhir untuk dikirimkan*/
        $campaign   =   \App\Campaign::find($this->campaign_id);
        if ($campaign!=null) {
            $jumlah     =   $campaign->spread()->queue()->count();
            if ($jumlah==0) {
                $campaign->status   =   "SENT";
                $campaign->save();
            }
        }
    }
}
