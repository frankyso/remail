<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Bus\SelfHandling;

class CampaignRecipientAdd extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $campaign_id;
    protected $list;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campaign_id, $list)
    {
        $this->campaign_id  =   $campaign_id;
        $this->list         =   $list;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Artisan::call('campaign:recipientAdd', [
            'id_campaign' => $this->campaign_id, 'list' => $this->list
        ]);
        // $id_campaign    = $this->campaign_id;
        // $tags           = $this->tags;
        // $campaign       =   \App\Campaign::find($id_campaign);

        // /*Looping to Get Users*/
        // $recipient = $campaign->user->recipients()->whereHas('tags', function($query) use ($tags){
        //     $query->where(function($queryInside) use ($tags){
        //         foreach ((array) $tags as $key => $value) {
        //             $queryInside->orWhere('recipient_tag_id','=',$value);
        //         }
        //     });
        //     $query->where('recipient_tag_id','!=',0);
        // });

        // $recipient->chunk(1000, function ($recipient_array) use ($campaign) {
        //     $spread = array();
        //     foreach ($recipient_array as $value) {
        //         $spread[]   =   ['campaign_id'=>$campaign->id,
        //                         'recipient_id'=>$value->id,
        //                         'status'=>'QUEUE',
        //                         'created_at'=> \Carbon\Carbon::now(),
        //                         'updated_at'=>\Carbon\Carbon::now()
        //                         ];
        //     }
        //     \App\CampaignSpread::insert($spread);
        // });
    }
}
