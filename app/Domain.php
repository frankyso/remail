<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class Domain extends Model
{
    protected $table    = 'domains';
    protected $dates    = ['expire_at'];
    protected $keyPath  = "key/domain";

    public static function boot()
    {
        parent::boot();

        static::creating(function($domain)
        {
            $request = \cURL::newRequest('post', 'https://api.mailgun.net/v3/domains', ['name' => $domain->name,'wildcard'=>'true'])
                ->setUser('api')->setPass(config('services.mailgun.secret'));

            $response = $request->send();
            $result   = json_decode($response->body, true);
            if (!isset($result['domain'])) {
                return false;
            }

            \cURL::newRequest('post', 'https://api.mailgun.net/v3/domains/'.$domain->name.'/webhooks',['url'=>'https://app.remail.web.id/api/v1/webhook/'.$domain->name.'/bounced','id'=>'bounce'])
                ->setUser('api')->setPass(config('services.mailgun.secret'))->send();

            \cURL::newRequest('post', 'https://api.mailgun.net/v3/domains/'.$domain->name.'/webhooks',['url'=>'https://app.remail.web.id/api/v1/webhook/'.$domain->name.'/delivered','id'=>'deliver'])
                ->setUser('api')->setPass(config('services.mailgun.secret'))->send();

            \cURL::newRequest('post', 'https://api.mailgun.net/v3/domains/'.$domain->name.'/webhooks',['url'=>'https://app.remail.web.id/api/v1/webhook/'.$domain->name.'/dropped','id'=>'drop'])
                ->setUser('api')->setPass(config('services.mailgun.secret'))->send();
                
        });

        static::deleted(function($domain)
        {
            $request = \cURL::newRequest('delete', 'https://api.mailgun.net/v3/domains/'.$domain->name)
                ->setUser('api')->setPass(config('services.mailgun.secret'));

            $response = $request->send();
        });
    }

    public function scopeActive($query)
    {
    	$query->where('validated','=', 1);
    }

    public function active()
    {
        if ($this->validated==1) {
            return true;
        }

        return false;
    }

    public function status()
    {
        if ($this->active()) {
            return "<span class='label label-success'>Verified</span>";
        }

        return "<span class='label label-default'>Unverified</span>";
    }

    public function droplets()
    {
    	return $this->hasMany('App\DropletDomain','domain_id');
    }

    public function campaigns()
    {
        return $this->hasMany('App\Campaign','domain_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function attachDroplets($droplet_ammount = 0)
    {
        $droplets   =   \App\Droplet::whereHas('domains',function($query){},0)->online()->orderBy('updated_at','ASC');
        if ($droplet_ammount > $this->droplets()->count()) {
            $droplets_needed    =   $droplet_ammount   -   $this->droplets()->count();
            $droplet    =   $droplets->take($droplets_needed);
            foreach ($droplet->get() as $key => $value) {
                /*Mengintegrasikan ke Domain*/
                $dropletDomain  =   new \App\DropletDomain;
                $dropletDomain->domain_id   =   $this->id;
                $dropletDomain->droplet_id  =   $value->id;
                $dropletDomain->save();

                /*Access to domain to make nameserver*/
                config(['remote.connections.production'=>['host'=>$value->public_ip,
                                                    'username'=>Crypt::decrypt($value->username),
                                                    'password'=>Crypt::decrypt($value->password)]]);

                /*Renaming Hostname*/
                // $this->name
                try {
                    /*Better Check if it's has private key*/
                    if (!$this->privateKey()) {
                        /*Generate Key*/
                        \SSH::run([
                            "rm -f /root/mail.*",
                            "rm -f /etc/postfix/dkim.key",
                            "cd /root/",
                            "opendkim-genkey -t -s mail -d ".$this->name,
                            "cp mail.private /etc/postfix/dkim.key"
                        ]);

                        sleep(1);

                        $publicKeyParsing   =   \SSH::getString('/root/mail.txt');
                        $publicKeyParsing   =   explode("p=", $publicKeyParsing);
                        $publicKeyParsing   =   substr($publicKeyParsing[1], 0, strrpos($publicKeyParsing[1], '"'));

                        $this->publicKey("v=DKIM1; g=*; k=rsa; p=".$publicKeyParsing);
                        $this->privateKey(\SSH::getString('/root/mail.private'));
                    }
                    else
                    {
                        \SSH::putString('/etc/postfix/dkim.key', $this->privateKey());
                        \SSH::run([
                            "chmod 600 /etc/postfix/dkim.key"
                        ]);
                    }

                    \SSH::run([
                        "sed -i -e 's/".$value->name."/"."remail.".$this->name."/g' /etc/hosts",
                        "sed -i -e 's/".$value->name."/"."remail.".$this->name."/g' /etc/hostname",
                        "sed -i -e 's/".$value->name."/"."remail.".$this->name."/g' /etc/postfix/main.cf",
                        "reboot"
                    ]);

                    $value->status  =   "RESTART";
                    $value->save();
                } catch (\Exception $e) {
                    \Bugsnag::notifyException($e);
                }
            }
        }
        elseif($droplet_ammount < $this->droplets()->count())
        {
            $droplets_removed   =   $this->droplets()->count() - $droplet_ammount;
            $droplet            =   $this->droplets()->take($droplets_removed);
            foreach ($droplet->get() as $key => $value) {
                /*Access to domain to make nameserver*/
                config(['remote.connections.production'=>['host'=>$value->droplet->public_ip,
                                                    'username'=>Crypt::decrypt($value->droplet->username),
                                                    'password'=>Crypt::decrypt($value->droplet->password)]]);


                /*Renaming Hostname*/
                // $this->name
                try {
                    \SSH::run([
                        "sed -i -e 's/"."remail.".$this->name."/".$value->droplet->name."/g' /etc/hosts",
                        "sed -i -e 's/"."remail.".$this->name."/".$value->droplet->name."/g' /etc/hostname",
                        "sed -i -e 's/"."remail.".$this->name."/".$value->droplet->name."/g' /etc/postfix/main.cf",
                        "rm -f /root/mail.*",
                        "rm -f /etc/postfix/dkim.key",
                        "reboot"
                    ]);
                } catch (\Exception $e) {
                    \Bugsnag::notifyException($e);
                }

                $dropletModel = $value->droplet;
                $dropletModel->status   =   "RESTART";
                $dropletModel->save();

                $value->delete();
            }
        }
    }

    public function privateKey($key=null)
    {
        if ($key) {
            \Storage::disk('local')->put($this->keyPath.'/'.$this->user->id."_".$this->name.'/private', $key);
        }

        $exists = \Storage::disk('local')->exists($this->keyPath.'/'.$this->user->id."_".$this->name.'/private');
        if ($exists) {
            return \Storage::disk('local')->get($this->keyPath.'/'.$this->user->id."_".$this->name.'/private');
        }

        \Slack::to('#notif-droplet')->send("Droplet ".$this->user->id."_".$this->name." : Tidak memiliki Key Request Gagal");

        return false;
    }

    public function publicKey($key=null)
    {
        if ($key) {
            \Storage::disk('local')->put($this->keyPath.'/'.$this->user->id."_".$this->name.'/public', $key);
        }

        $exists = \Storage::disk('local')->exists($this->keyPath.'/'.$this->user->id."_".$this->name.'/public');
        if ($exists) {
            return \Storage::disk('local')->get($this->keyPath.'/'.$this->user->id."_".$this->name.'/public');
        }

        \Slack::to('#notif-droplet')->send("Droplet ".$this->user->id."_".$this->name." : Tidak memiliki Key Request Gagal");

        return false;
    }

    public function checkDNS($sendEmail = false)
    {   
        $request = \cURL::newRequest('put', 'https://api.mailgun.net/v3/domains/'.$this->name.'/verify')
            ->setUser('api')->setPass(config('services.mailgun.secret'));
        $mg    =   $request->send();
        // dd($mg);

        /*Melakukan pengecekan pada A Record First*/
        $result     =   array();

        /*Searching for SPF*/
        $request = \cURL::newRequest('get', 'https://api.mailgun.net/v3/domains/'.$this->name)
                ->setUser('api')->setPass(config('services.mailgun.secret'));

        $response = json_decode($request->send()->body);
        // if ($response->sending_dns_records[0]->valid != "valid") {
        //     $response->sending_dns_records[0]->value    =   str_replace("mailgun.org", "config.remail.web.id", $response->sending_dns_records[0]->value);
        //     $result[]   =   $response->sending_dns_records[0];
        // }

        // if ($response->sending_dns_records[1]->valid != "valid") {
        //     $result[]   =   $response->sending_dns_records[1];
        // }
        
        
        $spfRecords     =   dns_get_record($response->sending_dns_records[0]->name, DNS_TXT);

        if (count($spfRecords) > 0) {
            foreach ($spfRecords as $key => $spfRecord) {
                $response->sending_dns_records[0]->value    =   str_replace("mailgun.org", "config.remail.web.id", $response->sending_dns_records[0]->value);
                if ($spfRecord['txt']  != "v=spf1 include:config.remail.web.id ~all") {
                    $result['spf']   =   $response->sending_dns_records[0];
                }
                else
                {
                    unset($result['spf']);
                    break;
                }
            }
        }
        else
        {
            $result['spf']   =   $response->sending_dns_records[0];
        }

        $dkimRecords     =   dns_get_record($response->sending_dns_records[1]->name, DNS_TXT);
        if (count($dkimRecords) > 0) {
            if ($dkimRecords[0]['txt']  != $response->sending_dns_records[1]->value) {
                $result['dkim']   =   $response->sending_dns_records[1];
            }
        }
        else
        {
            $result['dkim']   =   $response->sending_dns_records[1];
        }

        if($this->validated==0 && count($result) == 0)
        {
            $this->validated = 1;
            $this->save();

            if ($sendEmail == true) {
                $data['domain']     =    $this->name;
                $email  =  $this->user->email;
                $name   =  $this->user->name;
                \Mail::send('email.domainValidated', $data, function ($m) use ($email, $name) {
                    $m->to($email, $name)->subject('Domain Telah Aktif');
                });
            }
        }
        elseif($this->validated==1 && count($result) > 0)
        {
            $this->validated = 0;
            $this->save();

            if ($sendEmail == true) {
                $data['domain']     =    $this->name;
                $email  =  $this->user->email;
                $name   =  $this->user->name;
                \Mail::send('email.domainUnvalidated', $data, function ($m) use ($email, $name) {
                    $m->to($email, $name)->subject('Pengaturan Domain Salah');
                });
            }
        }

        return $result;
    }

    private function search_in_array($array, $keyword, $wildcard=false)
    {
        $result = "";
        foreach ($array as $key => $value) {
            if ($wildcard) {
                if (str_contains($value, $keyword)) {
                    $result .= " ".$value;
                };
            }
            else
            {
                if (str_is($keyword,$value)) {
                    $result .= " ".$value;
                };
            }
        }

        return $result;
    }
}
