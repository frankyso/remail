<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DropletDomain extends Model
{
    protected $table = 'droplets_domain';

    public function droplet()
    {
    	return $this->belongsTo('App\Droplet','droplet_id');
    }

    public function domain()
    {
    	return $this->belongsTo('App\Domain','domain_id');
    }
}
