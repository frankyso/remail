<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Phoenix\EloquentMeta\MetaTrait;

class Transaction extends Model
{
	use MetaTrait;
    protected $table = 'transactions';
    protected $meta_model = 'App\TransactionMeta';

    public function user()
    {
    	return $this->belongsTo('App\User','user_id');
    }

    public function getDescription()
    {
    	if ($this->type=="TOPUP") {
    		return "Balance topup";
    	}

        if ($this->type=="REFUND") {
            return "Refund balance";
        }

        if ($this->type=="PLAN_CHANGE") {
            return "Plan billing ".number_format($this->getMeta('session',0))." sessions for ".number_format($this->getMeta('contract',0))." month";
        }

        if ($this->type=="DOMAIN") {
            return "Sewa server untuk domain ".$this->getMeta('domain_name');
        }

        if ($this->type=="AFFILIATE") {
            return "Reward Affiliasi";
        }

        return $this->getMeta('description');
    }

    public function getAmmount($html = true, $currency = "Rp.")
    {
    	if ($html==true) {
    		if ($this->ammount < 0) {
    			return "<span class='text-danger'>-".$currency.number_format(abs($this->ammount))."</span>";
    		}
    		else
    		{
				return "<span class='text-success'>".$currency.number_format($this->ammount)."</span>";
    		}
    	}

    	return $this->ammount;
    }

    public function getExtra()
    {
    	$html = "";
    	if ($this->status=="DRAFT") {
    		$html 	.= "<span class='label label-default' data-toggle='tooltip' data-placement='top' title='You have not complete the payment information, this information will be deleted soon.'>DRAFT</span>";
    	}
    	if ($this->status=="PENDING") {
    		$html 	.= "<span class='label label-info' data-toggle='tooltip' data-placement='top' title='This transaction status is pending, please complete Your payment information or this transaction info will be deleted in 3 days'>PENDING</span>";
    	}
    	if ($this->status=="SUCCESS") {
    		$html 	.= "<span class='label label-success' data-toggle='tooltip' data-placement='top'>SUCCESS</span>";
    	}


    	return $html;
    }
}
