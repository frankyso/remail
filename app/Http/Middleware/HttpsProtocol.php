<?php 
namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

class HttpsProtocol {

    public function handle($request, Closure $next)
    {
    	if ($request->server('HTTP_X_FORWARDED_PROTO') == 'http' && env('APP_ENV') != 'local' && !$request->is('api/v1/payment'))
		{
   			return redirect()->secure($request->getRequestUri());
		}

		return $next($request); 
    }
}
?>