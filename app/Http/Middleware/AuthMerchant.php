<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthMerchant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            if (Auth::user()->role!='MERCHANT') {
                return redirect('auth/login');
            }
        }
        elseif (Auth::check()) {
            if (Auth::user()->role!='RESELLER') {
                return redirect('auth/login');
            }
        }
        else
        {
            return redirect('auth/login');
        }

        return $next($request);
    }
}
