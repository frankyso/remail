<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('overview');
});

Route::group(['middleware' => 'api'], function () {
	Route::post('api/v1/payment', 'Api\V1\ApiController@payment');
});

Route::group(['middleware' => 'web'], function () {
	Route::get('auth/login', 'Auth\AuthController@getLogin');
	Route::get('auth/register', 'Auth\AuthController@getRegister');
	Route::get('auth/forgot/sent', 'Auth\AuthController@getForgetPasswordSent');
	Route::get('auth/forgot', 'Auth\AuthController@getForgetPassword');
	Route::get('auth/reset/{token}', 'Auth\AuthController@getResetPassword');
	Route::post('auth/reset/{token}', 'Auth\AuthController@postResetPassword');
	Route::post('auth/resend-confirmation', 'Auth\AuthController@postResendConfirmation');
	Route::get('auth/logout', function(){Auth::logout(); return redirect('auth/login');});
	Route::get('auth/activate/{id}', 'Auth\AuthController@getActivateAccount');
	Route::post('auth/login', 'Auth\AuthController@postLogin');
	Route::post('auth/register', 'Auth\AuthController@postRegister');
	Route::post('auth/forgot', 'Auth\AuthController@postForgetPassword');
	
	Route::get('api/v1/opened/{id}', 'Api\V1\ApiController@opened');
	Route::get('api/v1/clicked/{id}', 'Api\V1\ApiController@clicked');
	Route::get('api/v1/unsubscribe/{id}', 'Api\V1\ApiController@unsubscribe');
	Route::get('api/v1/unsubscribe-report/{id}', 'Api\V1\ApiController@unsubscribeReport');

	// RESTFUL API
	Route::post('api/v1/recipient', 'Api\V1\ApiController@recipient');
	Route::post('api/v1/recipient/create', 'Api\V1\ApiController@recipientCreate');
	Route::post('api/v1/recipient/update', 'Api\V1\ApiController@recipientUpdate');
	Route::post('api/v1/recipient/delete', 'Api\V1\ApiController@recipientDelete');
	Route::get('api/v1/recipient/list', 'Api\V1\ApiController@recipientList');

	// Mailgun Webhook
	Route::post('api/v1/webhook/{domain}/delivered', 'Api\V1\ApiController@webhookDelivered');
	Route::post('api/v1/webhook/{domain}/dropped', 'Api\V1\ApiController@webhookDropped');
	Route::post('api/v1/webhook/{domain}/bounced', 'Api\V1\ApiController@webhookBounced');


	/*Publishers Route*/
	Route::group(['namespace'=>'Merchant'], function () {
		/* Authentication Routing */

		Route::group(['middleware' => ['merchant']], function () {
			Route::resource('/overview', 'OverviewController');
			Route::resource('/campaigns', 'CampaignController');
			Route::get('/templates/types', 'TemplateController@types');
			Route::resource('/templates/dragndrop', 'DragnDropController');
			Route::post('/templates/dragndrop/autosave', 'DragnDropController@autosave');
			Route::post('/templates/dragndrop/sendpreview', 'DragnDropController@sendpreview');
			Route::resource('/templates', 'TemplateController');
			
			Route::resource('/lists', 'ListController');
			Route::group(['prefix' => 'lists/{list_id}/'], function () {
				Route::get('import/excel/{filename}', 'ListController@getExcel');
				Route::post('import/excel', 'ListController@postExcel');
				Route::post('import/excel/{filename}/post', 'ListController@postExcelFinal');

				Route::get('recipients/export', 'ListRecipientController@export');
				Route::resource('recipients', 'ListRecipientController');
				Route::resource('integrate', 'ListIntegrationController');
				Route::resource('setting', 'ListSettingController');
			    // Route::get('detail', function ($accountId)    {
			    //     // Matches The "/accounts/{account_id}/detail" URL
			    // });
			});

			Route::post('/recipients/import', 'RecipientController@import');
			Route::get('/recipients/export', 'RecipientController@export');
			Route::resource('/recipients', 'RecipientController');
			Route::resource('/tags', 'TagController');
			Route::resource('/settings/profile', 'Setting\ProfileController');
			Route::resource('/settings/email', 'Setting\EmailController');
			Route::resource('/settings/domains', 'Setting\DomainController');
			Route::get('/settings/billing/invite', 'Setting\BillingController@invite');
			Route::post('/settings/billing/invite', 'Setting\BillingController@postInvite');
			Route::resource('/settings/billing', 'Setting\BillingController');
			Route::post('/setting/billing/addbalance', 'Setting\BillingController@addBalance');

			/*Campaigns Routes*/
			// Route::get('/campaign/{id}/export', 'CampaignController@export');
			// Route::resource('/campaign', 'CampaignController');
			// Route::get('/campaign/local/create', 'CampaignController@CreateLocalCampaign');
			// Route::get('/campaign/media/create', 'CampaignController@CreateMediaCampaign');
			// Route::resource('/campaign/local/facebook', 'Campaign\Local\FacebookController');
			// Route::resource('/campaign/local/twitter', 'Campaign\Local\TwitterController');
			// Route::resource('/campaign/local/clicktowebsite', 'Campaign\Local\ClickToWebsiteController');
			// Route::resource('/campaign/local/survey', 'Campaign\Local\SurveyController');
			// Route::resource('/campaign/local/email', 'Campaign\Local\EmailController');
			// Route::resource('/campaign/media/facebook', 'Campaign\Media\FacebookController');
			// Route::resource('/campaign/media/twitter', 'Campaign\Media\TwitterController');
			// Route::resource('/campaign/media/clicktowebsite', 'Campaign\Media\ClickToWebsiteController');
			
			// Route::get('/customer/export', 'CustomerController@export');
			// Route::resource('/customer', 'CustomerController');
			// Route::get('/hotspot/{id}/export', 'HotspotController@export');
			// Route::resource('/hotspot', 'HotspotController');

			// Route::get('/setting', function(){return redirect('/setting/brand');});
			// Route::resource('/setting/brand', 'Setting\BrandController');
			// Route::resource('/setting/authentication', 'Setting\AuthtenticationController');
			// Route::resource('/setting/media', 'Setting\MediaController');

			// Route::post('/setting/profile/resendemail', 'Setting\ProfileController@resendEmailConfirmation');
			// Route::resource('/setting/profile', 'Setting\ProfileController');
			
			// Route::post('/setting/billing/addbalance', 'Setting\BillingController@addBalance');
			// Route::post('/setting/billing/changeplan', 'Setting\BillingController@changePlan');
			// Route::resource('/setting/billing', 'Setting\BillingController');
	    });
	});
});
