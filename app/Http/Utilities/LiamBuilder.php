<?php 
	namespace App\Http\Utilities;
	use Crypt;

	class LiamBuilder
	{
		private $raw_template;
		function __construct($template)
		{
			$this->raw_template 	=	$template;
		}

		public function parse($receiver = null)
		{
			$DOM_HTML			= 	new \Htmldom($this->raw_template);

			/*Remove DragnDrop Helper*/
			foreach ($DOM_HTML->find('.action-button') as $key => $value) {
			    $value->outertext    =   "";
			}

			if($receiver==null)
			{
				return "<html><body>".urldecode($DOM_HTML->save())."</body></html>";
			}

			/*Clicked*/
			foreach ($DOM_HTML->find('a') as $key => $value) {				
			    $value->href    =   url('api/v1/clicked/'.$receiver->campaign_id."_".$receiver->recipient_id).'?redirect='.urlencode($value->href);
			}
			
			$content_html = urldecode($DOM_HTML->save());
			// dd($DOM_HTML->save());

			/*Unsubscribed*/
			$content_html = str_replace("{NAME}", $receiver->recipient->name, $content_html);
			$content_html = str_replace("{UNSUBSCRIBE}", "<a href=\"{UNSUBSCRIBE_URL}\">Unsubscribe</a>", $content_html);
			$content_html = str_replace("{UNSUBSCRIBE_URL}", url('api/v1/unsubscribe/'.Crypt::encrypt($receiver->recipient_id)), $content_html);


			$content_html = str_replace("{CURRENT_YEAR}", date('Y'), $content_html);
			$content_html = str_replace("{USER:COMPANY}", $receiver->recipient->user->company_name, $content_html);
			$content_html = str_replace("{RECIPIENT:EMAIL}", $receiver->recipient->email, $content_html);

			// echo $content_html;
			// dd();

			return "<html><body>$content_html<img src='".url('api/v1/opened/'.$receiver->campaign_id."_".$receiver->recipient_id)."' alt='' width='0' height='0'></body></html>";
		}
	}
?>