<?php

namespace App\Http\Controllers\Merchant\Setting;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Domain;
use App\Droplet;
use App\DropletDomain;
use Auth;
use Validator;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;


class DomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $domains  =   Auth::user()->domains();

        if ($request->get('q')) {
            $domains->where('name','like','%'.$request->get('q').'%');
        }

        return view('merchant.setting.domain.index')->with('domains',$domains->paginate(15))->with('q',$request->get('q',''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('merchant.setting.domain.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules  =   ['name'      => 'required|unique:domains,name'];

        $messages = [
            'name.unique' => 'Domain pernah ditambahkan sebelumnya oleh pengguna lain, saran kami gunakan subdomain ex. remail.'.$request->get('name','domainanda.com'),
        ];


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $domain                  = new Domain;
        $domain->name            = $request->get('name'); 
        $domain->name            = str_replace("https", "", $domain->name);
        $domain->name            = str_replace("http", "", $domain->name);
        $domain->name            = str_replace("www.", "", $domain->name);
        $domain->name            = str_replace(":", "", $domain->name);
        $domain->name            = str_replace("/", "", $domain->name);
        $domain->user_id         = Auth::user()->id;
        if (!$domain->save()) {
            return redirect()->back()
                    ->withErrors(['name'=>'Domain pernah ditambahkan sebelumnya oleh pengguna lain, saran kami gunakan subdomain ex. remail.'.$request->get('name','domainanda.com')])
                    ->withInput($request->except('password'));
        }

        // $domain->attachDroplets(1);
        $domain->checkDNS();
        return redirect('settings/domains/'.$domain->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $domain  =   Auth::user()->domains()->find($id);

        return view('merchant.setting.domain.edit')->with('domain',$domain)->with('dns',$domain->checkDNS(false, $request->get('check', false)));
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $domain  =   Auth::user()->domains()->find($id);

        /*Check duitnya cukup tidak perpanjang paket selama sebulan*/
        // $balance    =   Auth::user()->balance;
        // $price      =   config('remail.price');
        // $months     =   $request->get('months',0);
        // $dropletsnumber   =   $request->get('droplets',0);
        // $user       =   Auth::user();
        // if ($balance < ($price * $dropletsnumber * $months)) {
        //     return redirect('settings/billing/');
        // }

        // /*Cek dulu dropletnya cocok apa tidak*/
        // if ($dropletsnumber == $domain->droplets()->count()) {
        //     if ($domain->expire_at >= \Carbon\Carbon::now()) {
        //         $domain->expire_at  =   $domain->expire_at->addMonths($months);
        //     }
        //     else
        //     {
        //         $domain->expire_at  =   \Carbon\Carbon::now()->addMonths($months);
        //     }
        // }
        // else
        // {
        //     $domain->expire_at  =   \Carbon\Carbon::now()->addMonths($months);
        // }

        // $domain->save();
        // $user->balance   -=   (config('remail.price') * $dropletsnumber * $months);
        // $user->save();

        /*Add Into Transaction*/
        // $transaction = new \App\Transaction;
        // $transaction->type      =   "DOMAIN";
        // $transaction->ammount   =   -config('remail.price') * $dropletsnumber * $months;
        // $transaction->status    =   "SUCCESS";
        // $transaction->user_id   =   $user->id;
        // $transaction->save();

        // \Auth::user()->parentReward();

        // $transaction->updateMeta('domain_name',$domain->name);
        // $transaction->updateMeta('droplets',$dropletsnumber);

        // $domain->attachDroplets(1);

        /*Saatnya mencarikan server yang kosong untuk pembeli yang tampan*/
        // if ($domain->droplets()->count() != $dropletsnumber) {
        //     $droplets   =   Droplet::whereHas('domains',function($query){},0)->orderBy('updated_at','ASC');
        //     if ($droplets->count()==0) {
        //         /*Lapor Kumendan*/
        //         \Slack::to('#notif-droplet')->send("Upgrade akun tapi kekurangan Droplet silahkan di cek untuk domain_id ".$domain->id);
        //         return redirect('settings/domains/');
        //     }
        //     elseif ($droplets->count()<=10) {
        //         \Slack::to('#notif-droplet')->send("Droplet yang tersedia tinggal ".($droplets->count()-1).", please tambahkan lagi");
        //     }

        //     if ($dropletsnumber > $domain->droplets()->count()) {
        //         $droplets_needed    =   $dropletsnumber   -   $domain->droplets()->count();
        //         $droplet    =   $droplets->take($droplets_needed);
        //         foreach ($droplet->get() as $key => $value) {
        //             /*Mengintegrasikan ke Domain*/
        //             $dropletDomain  =   new DropletDomain;
        //             $dropletDomain->domain_id   =   $domain->id;
        //             $dropletDomain->droplet_id  =   $value->id;
        //             $dropletDomain->save();

        //             /*Access to domain to make nameserver*/
        //             config(['remote.connections.production'=>['host'=>$value->public_ip,
        //                                                 'username'=>Crypt::decrypt($value->username),
        //                                                 'password'=>Crypt::decrypt($value->password)]]);

        //             /*Renaming Hostname*/
        //             // $domain->name
        //             try {
        //                 /*Better Check if it's has private key*/
        //                 if (!$domain->privateKey()) {
        //                     /*Generate Key*/
        //                     \SSH::run([
        //                         "rm -f /root/mail.*",
        //                         "rm -f /etc/postfix/dkim.key",
        //                         "cd /root/",
        //                         "opendkim-genkey -t -s mail -d ".$domain->name,
        //                         "cp mail.private /etc/postfix/dkim.key"
        //                     ]);

        //                     $publicKeyParsing   =   \SSH::getString('/root/mail.txt');
        //                     $publicKeyParsing   =   explode("p=", $publicKeyParsing);
        //                     $publicKeyParsing   =   substr($publicKeyParsing[1], 0, strrpos($publicKeyParsing[1], '"'));

        //                     $domain->publicKey("v=DKIM1; g=*; k=rsa; p=".$publicKeyParsing);
        //                     $domain->privateKey(\SSH::getString('/root/mail.private'));
        //                 }
        //                 else
        //                 {
        //                     \SSH::putString('/etc/postfix/dkim.key', $domain->privateKey());
        //                     \SSH::run([
        //                         "chmod 600 /etc/postfix/dkim.key"
        //                     ]);
        //                 }

        //                 \SSH::run([
        //                     "sed -i -e 's/".$value->name."/"."remail.".$domain->name."/g' /etc/hosts",
        //                     "sed -i -e 's/".$value->name."/"."remail.".$domain->name."/g' /etc/hostname",
        //                     "sed -i -e 's/".$value->name."/"."remail.".$domain->name."/g' /etc/postfix/main.cf",
        //                     "reboot"
        //                 ]);
        //             } catch (\Exception $e) {
        //                 \Slack::to('#notif-droplet')->send("Droplet ".$value->name." : Tidak dapat dihubungi. ".$e->getMessage());
        //             }
        //         }
        //     }
        //     elseif($dropletsnumber < $domain->droplets()->count())
        //     {
        //         $droplets_removed   =   $domain->droplets()->count() - $dropletsnumber;
        //         $droplet            =   $domain->droplets()->take($droplets_removed);
        //         foreach ($droplet->get() as $key => $value) {
        //             /*Access to domain to make nameserver*/
        //             config(['remote.connections.production'=>['host'=>$value->droplet->public_ip,
        //                                                 'username'=>Crypt::decrypt($value->droplet->username),
        //                                                 'password'=>Crypt::decrypt($value->droplet->password)]]);


        //             /*Renaming Hostname*/
        //             // $domain->name
        //             try {
        //                 \SSH::run([
        //                     "sed -i -e 's/"."remail.".$domain->name."/".$value->droplet->name."/g' /etc/hosts",
        //                     "sed -i -e 's/"."remail.".$domain->name."/".$value->droplet->name."/g' /etc/hostname",
        //                     "sed -i -e 's/"."remail.".$domain->name."/".$value->droplet->name."/g' /etc/postfix/main.cf",
        //                     "rm -f /root/mail.*",
        //                     "rm -f /etc/postfix/dkim.key",
        //                     "reboot"
        //                 ]);
        //             } catch (\Exception $e) {
        //                 \Slack::to('#notif-droplet')->send("Droplet ".$value->droplet->name." : Tidak dapat dihubungi");
        //             }

        //             $value->delete();
        //             \Slack::to('#notif-droplet')->send("Droplet ".$value->name." Dikembalikan ke Kolam droplet");
        //         }
        //     }
        // }

        $domain->checkDNS();
        return redirect('settings/domains/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $domain  =   Auth::user()->domains()->find($id);

        // \Storage::disk('local')->delete('key/domain/'.$domain->name.'/private');
        // \Storage::disk('local')->delete('key/domain/'.$domain->name.'/public');

        // Reverse Hostname
        foreach ($domain->droplets()->get() as $key => $droplet) {

            /*Access to domain to make nameserver*/
            config(['remote.connections.production'=>['host'=>$droplet->droplet->public_ip,
                                                'username'=>Crypt::decrypt($droplet->droplet->username),
                                                'password'=>Crypt::decrypt($droplet->droplet->password)]]);


            /*Renaming Hostname*/
            // $domain->name
            try {
                \SSH::run([
                    "sed -i -e 's/"."remail.".$domain->name."/".$droplet->droplet->name."/g' /etc/hosts",
                    "sed -i -e 's/"."remail.".$domain->name."/".$droplet->droplet->name."/g' /etc/hostname",
                    "sed -i -e 's/"."remail.".$domain->name."/".$droplet->droplet->name."/g' /etc/postfix/main.cf",
                    "rm -f /root/mail.*",
                    "rm -f /etc/postfix/dkim.key",
                    "reboot"
                ]);
            } catch (\Exception $e) {
                \Slack::to('#notif-droplet')->send("Droplet ".$droplet->droplet->name." : Tidak dapat dihubungi");
            }

            $droplet->delete();
            \Slack::to('#notif-droplet')->send("Droplet ".$droplet->droplet->name." Dikembalikan ke Kolam droplet");
        }

        $domain->delete();
        return redirect('settings/domains/');
    }
}
