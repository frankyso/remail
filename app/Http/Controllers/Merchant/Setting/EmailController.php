<?php

namespace App\Http\Controllers\Merchant\Setting;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Smtp;
use Validator;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $smtp  =   Auth::user()->smtp();

        if ($request->get('q')) {
            $smtp->where('name','like','%'.$request->get('q').'%');
        }
        return view('merchant.smtp.index')->with('smtp',$smtp->paginate(15))->with('q',$request->get('q',''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('merchant.smtp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules  =   ['name'              => 'required',
                    'host'              => 'required',
                    'username'          => 'required',
                    'password'          => 'required',
                    'reply_to_email'    => 'required',
                    'reply_to_name'     => 'required',
                    'secure'            => 'required',
                    'port'              => 'required',
                    'bounce_host'       => '',
                    'bounce_port'       => '',
                    'bounce_service'    => '',
                    'bounce_option'     => '',
                    'bounce_mailbox'    => ''];


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $smtp                   = new Smtp;
        $smtp->name             = $request->get('name','');
        $smtp->host             = $request->get('host','');
        $smtp->username         = $request->get('username','');
        $smtp->password         = $request->get('password','');
        $smtp->reply_to_mail    = $request->get('reply_to_email','');
        $smtp->reply_to_name    = $request->get('reply_to_name','');
        $smtp->smtp_secure      = $request->get('secure','tls');
        $smtp->smtp_port        = $request->get('port','');
        $smtp->bounce_host      = $request->get('bounce_host','');
        $smtp->bounce_port      = $request->get('bounce_port','');
        $smtp->bounce_service   = $request->get('bounce_service','imap');
        $smtp->bounce_option    = $request->get('bounce_option','');
        $smtp->bounce_mailbox   = $request->get('bounce_mailbox','');
        $smtp->user_id          = Auth::user()->id;
        $smtp->save();


        return redirect('settings/email');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $smtp  =   Auth::user()->smtp()->find($id);
        return view('merchant.smtp.edit')->with('smtp',$smtp);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules  =   ['name'              => 'required',
                    'host'              => 'required',
                    'username'          => 'required',
                    'password'          => 'required',
                    'reply_to_email'    => 'required',
                    'reply_to_name'     => 'required',
                    'secure'            => 'required',
                    'port'              => 'required',
                    'bounce_host'       => '',
                    'bounce_port'       => '',
                    'bounce_service'    => '',
                    'bounce_option'     => '',
                    'bounce_mailbox'    => ''];


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $smtp                   =   Auth::user()->smtp()->find($id);
        $smtp->name             = $request->get('name','');
        $smtp->host             = $request->get('host','');
        $smtp->username         = $request->get('username','');
        $smtp->password         = $request->get('password','');
        $smtp->reply_to_mail    = $request->get('reply_to_email','');
        $smtp->reply_to_name    = $request->get('reply_to_name','');
        $smtp->smtp_secure      = $request->get('secure','tls');
        $smtp->smtp_port        = $request->get('port','');
        $smtp->bounce_host      = $request->get('bounce_host','');
        $smtp->bounce_port      = $request->get('bounce_port','');
        $smtp->bounce_service   = $request->get('bounce_service','imap');
        $smtp->bounce_option    = $request->get('bounce_option','');
        $smtp->bounce_mailbox   = $request->get('bounce_mailbox','');
        $smtp->user_id          = Auth::user()->id;
        $smtp->save();


        return redirect('settings/email');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
