<?php

namespace App\Http\Controllers\Merchant\Setting;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('merchant.setting.billing');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addBalance(Request $request)
    {
        $ipaymu             =   \App\Http\Utilities\iPaymu::api(config('app.ipaymu_Key'));
        $ipaymu->addProduct('Topup Balance Remail', $request->get('ammount',0), 1);
        $ipaymu->paymentPage(config('app.url').'/settings/billing?', config('app.url').'/api/v1/payment', config('app.url').'/settings/billing');

        $transaction            =   new \App\Transaction;
        $transaction->type      =   "TOPUP";
        $transaction->ammount   =   $request->get('ammount',0);
        $transaction->user_id   =   \Auth::user()->id;
        $transaction->status    =   "DRAFT";
        $transaction->save();

        $transaction->updateMeta('ipaymu_sesid', $ipaymu->payment->sessionID);

        \Slack::to('#notif-transaction')->send("Hihi ada yang mau topup, dapet duit lagi ".number_format($transaction->ammount)." dari ".Auth::user()->name);
        
        return redirect($ipaymu->payment->url);
    }

    public function invite(Request $request)
    {
        $code = $request->get('code');
        $googleService = \OAuth::consumer('Google');
        if (is_null($code)) {
            return redirect('settings/billing');
        }

        try {
            $token = $googleService->requestAccessToken($code);
            $result = json_decode($googleService->request('https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=400'), true);
        } catch (\Exception $e) {
            return redirect('settings/billing');
        }

        $emails = [];
        foreach ($result['feed']['entry'] as $contact) {
            if (isset($contact['gd$email'])) {
                $emails[] = $contact['gd$email'][0]['address'];
            }
        }
        
        return view('merchant.setting.invite')->with('emails',$emails);
    }

    public function postInvite(Request $request)
    {
        foreach ($request->get('emails') as $key => $value) {
            $data['url']   =    url('auth/register')."?id=".Auth::user()->id."&email=".$value;
            $data['inviter']            =    \Auth::user()->name;
            \Mail::send('email.invite', $data, function ($m) use ($value) {
                $m->to($value)->subject('Undangan dari '.\Auth::user()->name);
                $m->from('no-reply@remail.web.id',\Auth::user()->name);
            });
        }

        \Toast::success(count($request->get('emails')).' orang telah diundang', '');
        return redirect('settings/billing');
    }
}
