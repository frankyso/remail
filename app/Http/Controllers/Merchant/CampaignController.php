<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Campaign;
use App\Jobs\CampaignRecipientAdd;
use App\Jobs\CampaignSetupRecipientQueue;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campaigns  =   Auth::user()->campaigns();

        if ($request->get('q')) {
            $campaigns->where('name','like','%'.$request->get('q').'%');
        }
        return view('merchant.campaign.index')->with('campaigns',$campaigns->orderBy('created_at','DESC')->paginate(8))->with('q',$request->get('q',''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('merchant.campaign.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes  =   [
                    'name'          => 'Nama Kampanye',
                    'template'      => 'Template',
                    'domain'        => 'Domain',
                    'list'          => 'Daftar Penerima',
                    'sender_email'  => 'Email Pengirim',
                    'sender_name'   => 'Nama Pengirim',
                    ];

        $rules  =   [
                    'name'          => 'required',
                    'template'      => 'required',
                    'domain'        => 'required',
                    'list'          => 'required',
                    'sender_email'  => 'required',
                    'sender_name'   => 'required',
                    ];


        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $domain                    = Auth::user()->domains()->findOrFail($request->get('domain',''));

        $campaign                  = new Campaign;
        $campaign->name            = $request->get('name'); 
        $campaign->template_id     = $request->get('template'); 
        $campaign->domain_id       = $request->get('domain','');
        $campaign->status          = "WAITING";
        $campaign->sender_email    = $request->get('sender_email','')."@".\App\Http\Utilities\RemailHelper::normalize_url($domain->name);
        $campaign->sender_name     = $request->get('sender_name','');
        $campaign->user_id         = Auth::user()->id;
        $campaign->save();

        // Mengumpulkan Jumlah penerima
        $list   =   $request->get('list');
        $recipient = $campaign->user->recipients()->whereHas('listRecipient', function($query) use ($list){
            $query->where('list_id','=',$list);
        })->where(function($query){
            /*Hanya yang UNVERIFIED & VERIFIED*/
            $query->where('status','=','VALID');
            $query->orWhere('status','=','UNVERIFIED');
        });
        
        // Menyimpan jumlah yang menerima email
        $campaign->recipients_count   =   $recipient->count();
        $campaign->save();
        
        // Memasukan kedalam Antrian
        $this->dispatch(new CampaignRecipientAdd($campaign->id, $request->get('list')));

        return redirect('campaigns');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaign  =   Auth::user()->campaigns()->find($id);
        return view('merchant.campaign.show')->with('campaign',$campaign);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules  =   [
                    'status'      => 'required',
                    ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $campaign  =   Auth::user()->campaigns()->find($id);
        $campaign->status   =   $request->get('status','WAITING');
        if ($campaign->status   ==   "SENDING") {
            if (($campaign->recipients_count * config('remail.price')) >= Auth::user()->balance) {
                \Toast::error('Saldo Anda tidak Mencukupi', '');
                return redirect()->back();
            }
        }

        $transaction = new \App\Transaction;
        $transaction->type      =   "EMAIL";
        $transaction->ammount   =   -config('remail.price') * $campaign->spread()->queue()->count();
        $transaction->status    =   "SUCCESS";
        $transaction->user_id   =   Auth::user()->id;
        $transaction->save();

        $transaction->updateMeta('description','Biaya Pengiriman '.$campaign->spread()->queue()->count()." email - Campaign ".$campaign->name);
        $campaign->save();

        $user   =   Auth::user();
        $user->balance  =  $user->balance - (config('remail.price') * $campaign->spread()->queue()->count());
        $user->save();

        /*Masukan ke Antrian*/
        $this->dispatch(new CampaignSetupRecipientQueue($campaign->id));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign  =   Auth::user()->campaigns()->find($id);
        $campaign->delete();
        return redirect('campaigns');
    }
}
