<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Recipient;
use App\Tag;
use App\RecipientTag;

class RecipientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recipients  =   Auth::user()->recipients();

        if ($request->get('q')) {
            $recipients->where(function($query) use ($request){
                $query->orWhere('name','like','%'.$request->get('q').'%');
                $query->orWhere('email','like','%'.$request->get('q').'%');
            });
        }

        if ($request->get('tag')) {
            $recipients->whereHas('tags',function($tagQuery) use ($request){
                $tagQuery->where('recipient_tag_id','=',$request->get('tag'));
            });
        }

        if ($request->get('status')) {
            $recipients->where('status','=',$request->get('status'));
        }


        return view('merchant.recipient.index')->with('recipients',$recipients->paginate(30))->with('q',$request->get('q',''))
        ->with('status',$request->get('status',''))
        ->with('tagq',$request->get('tag',''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('merchant.recipient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules  =   [
                    'name'         => 'required',
                    'email'        => 'required',
                    'comment'  => '',
                    'tags'         => 'required'];


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $recipient                  = new Recipient;
        $recipient->name            = $request->get('name'); 
        $recipient->email           = $request->get('email','');
        $recipient->comment         = $request->get('comment','');
        $recipient->user_id         = Auth::user()->id;
        $recipient->save();


        /*Pengaturan Tags*/
        if (is_array($request->get('tags')) && count($request->get('tags'))>0) {
            foreach ($request->get('tags') as $key => $tag_id) {
                if (!is_numeric($tag_id)) {
                    $tag                    = new Tag;
                    $tag->name              = $tag_id; 
                    $tag->description       = '';
                    $tag->user_id           = Auth::user()->id;
                    $tag->save();

                    $tag_id     =   $tag->id;
                }

                $recipientTag   =   new RecipientTag;
                $recipientTag->recipient_id         =   $recipient->id;
                $recipientTag->recipient_tag_id     =   $tag_id;
                $recipientTag->save();
            }
        }

        \Toast::success('Penerima baru berhasil ditambahkan', '');
        return redirect('recipients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipient  =   Auth::user()->recipients()->find($id);
        return view('merchant.recipient.edit')->with('recipient',$recipient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules  =   [
                    'name'         => 'required',
                    'email'        => 'required',
                    'comment'  => '',
                    'tags'         => ''];


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $recipient                  =   Auth::user()->recipients()->find($id);
        $recipient->name            = $request->get('name'); 
        $recipient->email           = $request->get('email','');
        $recipient->comment         = $request->get('comment','');
        $recipient->user_id         = Auth::user()->id;
        $recipient->save();


        /*Pengaturan Tags*/
        $recipient->tags()->delete();
        foreach ($request->get('tags') as $key => $tag_id) {
            if (!is_numeric($tag_id)) {
                $tag                    = new Tag;
                $tag->name              = $tag_id; 
                $tag->description       = '';
                $tag->user_id           = Auth::user()->id;
                $tag->save();

                $tag_id     =   $tag->id;
            }

            $recipientTag   =   new RecipientTag;
            $recipientTag->recipient_id         =   $recipient->id;
            $recipientTag->recipient_tag_id     =   $tag_id;
            $recipientTag->save();
        }

        \Toast::success('Penerima berhasil diubah', '');
        return redirect('recipients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recipient  =   Auth::user()->recipients()->find($id);
        $recipient->tags()->delete();
        $recipient->delete();
        return redirect('recipients');
    }

    public function import(Request $request)
    {
        if ($request->hasFile('csv')) {
            $file            = $request->file('csv');
            \Excel::load($file, function($reader) {
                foreach ($reader->get() as $key => $row) {
                    /*Looping For Saving*/
                    $recipient                  = new Recipient;
                    $recipient->name            = (string) $row->nama; 
                    $recipient->email           = (string) str_replace(" ", "", $row->email);
                    $recipient->comment         = (string) $row->komentar;
                    $recipient->user_id         = Auth::user()->id;
                    $recipient->save();

                    foreach (explode(',',$row->tag) as $key => $tag_id) {
                        /*Finding inside my list*/
                        if ($tag_id==null) {
                            continue;
                        }
                        $tagObject  = Auth::user()->tags()->where('name','=',$tag_id);
                        if ($tagObject->count()>0) {
                            $tag_id    =   $tagObject->first()->id;
                        }
                        else
                        {
                            $tag                    = new Tag;
                            $tag->name              = $tag_id; 
                            $tag->description       = '';
                            $tag->user_id           = Auth::user()->id;
                            $tag->save();

                            $tag_id     =   $tag->id;
                        }

                        $recipientTag   =   new RecipientTag;
                        $recipientTag->recipient_id         =   $recipient->id;
                        $recipientTag->recipient_tag_id     =   $tag_id;
                        $recipientTag->save();
                    }

                }
            });
        }
        return redirect('recipients');
    }

    public function export()
    {
        \Excel::create('Recipients', function($excel) {
            $excel->sheet('Sheet 1', function($sheet) {
                $sheet->appendRow(array(
                    'nama', 'email','komentar','tag'
                ));

            foreach (Auth::user()->recipients as $key => $recipient) {
                $tag_string =  '';
                foreach ($recipient->tags as $key => $tag) {
                    $tag_string .= $tag->tag->name.",";
                }

                $tag_string    =   rtrim($tag_string, ",");

                $sheet->appendRow(array(
                    $recipient->name, $recipient->email,$recipient->comment, $tag_string
                ));
            }
            });
        })->download('csv');
    }
}
