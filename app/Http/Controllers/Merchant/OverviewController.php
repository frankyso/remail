<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class OverviewController extends Controller
{
    public function index()
    {

        /*Check domain jika ada yang belm aktif*/
        if (Auth::user()->domains()->where('validated','=','0')->count() > 0) {
            \Toast::error('Ada domain yang belum diaktifkan / bermasalah, silahkan cek pada pengaturan Domain', '');
        }

    	// Pengecekan terhadap kelengkapan data
    	$data['email']		=	(Auth::user()->verified == 1)?true:false;
    	$data['balance']	=	(Auth::user()->transactions()->where('status','=','SUCCESS')->count() > 0)?true:false;
    	$data['domain']		=	(Auth::user()->domains()->count() > 0)?true:false;
    	if ($data['email'] == false || $data['balance'] == false || $data['domain']==false) {
    		return view('merchant.overview.complete',$data);
    	}

        // Mengumpulkan data analytics
        // Prepared Campaign
        $data['analytics']['prepared']  =   Auth::user()->campaigns()
                                            ->where(\DB::raw('date(created_at)'),'>=',date("Y-01-01"))
                                            ->where(\DB::raw('date(created_at)'),'<=',date("Y-12-31"))
                                            ->select('created_at')
                                            ->selectRaw('count(id) number')->groupBy(\DB::raw('date(created_at)'))->get();

        $data['analytics']['sent']  =   Auth::user()->campaigns()->sent()
                                            ->where(\DB::raw('date(created_at)'),'>=',date("Y-01-01"))
                                            ->where(\DB::raw('date(created_at)'),'<=',date("Y-12-31"))
                                            ->select('created_at')
                                            ->selectRaw('count(id) number')->groupBy(\DB::raw('date(created_at)'))->get();


        $data['widget']['email']    =   number_format(Auth::user()->recipients()->count());
        $data['widget']['template'] =   number_format(Auth::user()->templates()->count());
        $data['widget']['waiting']  =   number_format(Auth::user()->campaigns()->waiting()->count());

        $data['efficiencys']         =   Auth::user()->campaigns()->sent()->take(4)->get();
    	return view('merchant.overview.index',$data);
    }

    public function store(Request $request)
    {
        if ($request->has('saran')) {
            \Slack::to('#notif-suggest')->send($request->get('saran',''));
            \Toast::success('Saran Anda telah kami terima','');
        }
        return redirect()->back();
    }
}
