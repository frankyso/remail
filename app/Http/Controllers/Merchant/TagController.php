<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Tag;
use Validator;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tags  =   Auth::user()->tags();

        if ($request->get('q')) {
            $tags->where('name','like','%'.$request->get('q').'%');
        }

        return view('merchant.tag.index')->with('tags',$tags->paginate(15))->with('q',$request->get('q',''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('merchant.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules  =   ['name'         => 'required'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $tag                    = new Tag;
        $tag->name              = $request->get('name'); 
        $tag->description       = $request->get('description','');
        $tag->user_id           = Auth::user()->id;
        $tag->save();
        \Toast::success('Tag baru berhasil ditambahkan', '');
        return redirect('tags');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag  =   Auth::user()->tags()->find($id);
        return view('merchant.tag.edit')->with('tag',$tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules  =   ['name'         => 'required'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $tag                    = Auth::user()->tags()->find($id);
        $tag->name              = $request->get('name'); 
        $tag->description       = $request->get('description','');
        $tag->save();
        \Toast::success('Tag baru berhasil diupdate', '');
        return redirect('tags');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag  =   Auth::user()->tags()->find($id);
        $tag->recipients()->delete();
        $tag->delete();
        \Toast::success('Tag terhapus', '');
        return redirect('tags');
    }
}
