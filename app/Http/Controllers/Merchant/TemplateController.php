<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Template;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $templates  =   Auth::user()->templates();

        if ($request->get('q')) {
            $templates->where('name','like','%'.$request->get('q').'%');
        }
        return view('merchant.template.index')->with('templates',$templates->paginate(15))->with('q',$request->get('q',''));
    }

    public function types()
    {
        return view('merchant.template.types');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('merchant.template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules  =   [
                    'name'      => 'required',
                    'subject'   => 'required',
                    'message'   => 'required'];


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        if (!str_contains($request->get('message'), '{UNSUBSCRIBE_URL}') && !str_contains($request->get('message'), '{UNSUBSCRIBE}')) {
            return redirect()->back()
                    ->withErrors(['message'=>'Template harus mengandung Link Unsubscribe'])
                    ->withInput($request->except('password'));
        }

        $template                  = new Template;
        $template->name            = $request->get('name'); 
        $template->subject         = $request->get('subject','');
        $template->content_html    = $request->get('message','');
        $template->user_id         = Auth::user()->id;
        $template->save();

        \Toast::success('Template baru berhasil ditambahkan', '');
        return redirect('templates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template  =   Auth::user()->templates()->find($id);
        return view('merchant.template.edit')->with('template',$template);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules  =   [
                    'name'      => 'required',
                    'subject'   => 'required',
                    'message'   => 'required'];


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        if (!str_contains($request->get('message'), '{UNSUBSCRIBE_URL}') && !str_contains($request->get('message'), '{UNSUBSCRIBE}')) {
            return redirect()->back()
                    ->withErrors(['message'=>'Template harus mengandung Link Unsubscribe'])
                    ->withInput($request->except('password'));
        }

        $template                  =   Auth::user()->templates()->find($id);
        $template->name            = $request->get('name'); 
        $template->subject         = $request->get('subject','');
        $template->content_html    = $request->get('message','');
        $template->user_id         = Auth::user()->id;
        $template->save();

        \Toast::success('Tag baru berhasil ditambahkan', '');
        return redirect('templates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $template                  =   Auth::user()->templates()->find($id);
        $template->delete();
        return redirect('templates');
    }
}
