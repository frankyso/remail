<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\ListRecipient;
use App\Jobs\ValidateEmailStatus;
use App\Recipient;

class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lists  =   Auth::user()->lists();
        
        if ($request->get('q')) {
            $recipients->where(function($query) use ($request){
                $query->orWhere('name','like','%'.$request->get('q').'%');
            });
        }

        return view('merchant.list.index')->with('lists',$lists->paginate(30))->with('q',$request->get('q',''))
        ->with('status',$request->get('status',''))
        ->with('tagq',$request->get('tag',''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('merchant.list.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes  =   ['name'         =>  'Nama Daftar Penerima',
                    'listed_reason' =>  'Alasan Terdaftar',
                    'company'       =>  'Nama Perusahaan',
                    'address'       =>  'Alamat',
                    'city'          =>  'Kota',
                    'phone'         =>  'No Telpon',
                    'postal_code'   =>  'Kode Pos',
                    'website'       =>  'Website'];

        $rules  =   ['name'         =>  'required',
                    'listed_reason' =>  'required',
                    'company'       =>  'required',
                    'address'       =>  'required',
                    'phone'         =>  'required',
                    'city'          =>  'required',
                    'postal_code'   =>  '',
                    'website'       =>  'required|url'];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $list                   =   new ListRecipient;
        $list->user_id          =   Auth::user()->id;
        $list->name             =   $request->get('name');
        $list->listed_reason    =   $request->get('listed_reason');
        $list->company          =   $request->get('company');
        $list->contact_name     =   Auth::user()->name;
        $list->email_address    =   Auth::user()->email;
        $list->phone            =   $request->get('phone');
        $list->address          =   $request->get('address');
        $list->city             =   $request->get('city');
        $list->zip_code         =   $request->get('postal_code');
        $list->website          =   $request->get('website');
        $list->save();

        \Toast::success('Daftar Penerima baru berhasil ditambahkan', '');
        return redirect('lists/'.$list->id.'/edit/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list  =   Auth::user()->lists()->find($id);
        return view('merchant.list.edit')->with('list',$list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes  =   ['name'         =>  'Nama Daftar Penerima',
                    'listed_reason' =>  'Alasan Terdaftar',
                    'company'       =>  'Nama Perusahaan',
                    'address'       =>  'Alamat',
                    'city'          =>  'Kota',
                    'phone'         =>  'No Telpon',
                    'postal_code'   =>  'Kode Pos',
                    'website'       =>  'Website'];

        $rules  =   ['name'         =>  'required',
                    'listed_reason' =>  'required',
                    'company'       =>  'required',
                    'address'       =>  'required',
                    'phone'         =>  'required',
                    'city'          =>  'required',
                    'postal_code'   =>  '',
                    'website'       =>  'required|url'];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $list                   =   Auth::user()->lists()->find($id);
        $list->name             =   $request->get('name');
        $list->listed_reason    =   $request->get('listed_reason');
        $list->company          =   $request->get('company');
        $list->phone            =   $request->get('phone');
        $list->address          =   $request->get('address');
        $list->city             =   $request->get('city');
        $list->zip_code         =   $request->get('postal_code');
        $list->website          =   $request->get('website');
        $list->save();

        \Toast::success('Daftar Penerima berhasil diubah', '');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $list = Auth::user()->lists()->find($id);
        if(!$list)
        {
            return redirect()->back();
        }
        $list->recipients()->delete();
        $list->delete();

        return redirect('lists');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getExcel($list_id, $filename)
    {
        $headerRow  =   array();
        \Excel::selectSheetsByIndex(0)->load(public_path().'/tmp/import-contact/'.$filename, function($reader) use (&$headerRow) {
            $reader->noHeading();
            foreach ($reader->get()[0] as $key => $value) {
                $headerRow[]    =   ['key'=>$key, 'value'=>$value];
            }
        });

        return view('merchant.list.import-excel')->with('list_id',$list_id)->with('filename',$filename)->with('headerRows',$headerRow);
    }

    /**
     * Exporting Time
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postExcel(Request $request, $list_id)
    {
        if ($request->hasFile('excel')) {
            $file            = $request->file('excel');
            if (!in_array($file->guessExtension(), ['xls','xlsx','csv','txt'])) {
                \Toast::error('File yang di upload harus bertipe xls, xlsx, csv', '');
                return redirect()->back();
            }
            
            $destinationPath = public_path().'/tmp/import-contact/';
            $filename        = str_random(15) . '_' . $file->getClientOriginalName();
            $uploadSuccess   = $file->move($destinationPath, $filename);

            return redirect('/lists/'.$list_id.'/import/excel/'.$filename);
        }
    }

    public function postExcelFinal(Request $request, $list_id, $filename)
    {
        $attributes  =   ['name'         =>  'Nama Penerima',
                            'email'      =>  'Email Penerima',
                            'comment'    =>  'Komentar Anda'];

        $rules  =   [
                    'name'         => '',
                    'email'        => 'required',
                    'comment'  => ''];


        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $list   =   Auth::user()->lists()->find($list_id);
        if (!$list) {
            return redirect('lists');
        }

        $object     =   $this;
        \Excel::selectSheetsByIndex(0)->load(public_path().'/tmp/import-contact/'.$filename, function($reader) use ($request, $list_id, $object) {
            $reader->noHeading();
            foreach ($reader->get() as $key => $value) {
                $name       =   (is_numeric($request->get('name')))?$value[$request->get('name')]:"";
                $email      =   (is_numeric($request->get('email')))?$value[$request->get('email')]:"";
                $comment    =   (is_numeric($request->get('comment')))?$value[$request->get('comment')]:"";

                $recipient                  = new Recipient;
                $recipient->name            = (string) $name; 
                $recipient->email           = (string) str_replace(" ", "", $email);
                $recipient->comment         = (string) $comment;
                $recipient->user_id         = Auth::user()->id;
                $recipient->list_id         = $list_id;
                $recipient->save();

                $object->dispatch(new ValidateEmailStatus($recipient->id));
            }
        });
        return redirect('lists/'.$list_id.'/recipients');
    }
}
