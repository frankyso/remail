<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Template;
use Auth;
use App\Http\Utilities\LiamBuilder;

class DragnDropController extends Controller
{
    public function index()
    {
    	return view('merchant.dragndrop.create');
    }

    public function create()
    {
        $template                  = new Template;
        $template->name            = "Untitled"; 
        $template->type            = "DRAGNDROP";
        $template->user_id         = Auth::user()->id;
        $template->save();

        \Toast::success('Template Drag & Drop berhasil ditambahkan', '');
        \Toast::success('Template akan disimpan setiap 20 detik!', '');
        \Toast::success('Selamat mendesain!', '');
        return redirect('templates/dragndrop/'.$template->id.'/edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
    	$rules  =   [
    	            'name'      => 'required',
    	            'subject'   => 'required'];


    	$validator = Validator::make($request->all(), $rules);
    	if ($validator->fails()) {
    	    return redirect()->back()
    	            ->withErrors($validator)
    	            ->withInput($request->except('password'));
    	}

    	$template                  = new Template;
    	$template->name            = $request->get('name'); 
    	$template->subject         = $request->get('subject','');
    	$template->type            = "DRAGNDROP";
    	$template->user_id         = Auth::user()->id;
    	$template->save();

    	\Toast::success('Template baru berhasil ditambahkan, Berikutnya Template Anda akan otomatis diupdate setiap 20 detik', '');
    	return redirect('templates/dragndrop/'.$template->id.'/edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template  =   Auth::user()->templates()->find($id);
        return view('merchant.dragndrop.edit')->with('template',$template);
    }

    public function autosave(Request $request)
    {
    	$rules  =   [
    	            'id'      => 'required',
    	            'template_html'   => 'required',
                    ];


    	$validator = Validator::make($request->all(), $rules);
    	if ($validator->fails()) {
    	    exit;
    	}

        if (!str_contains($request->get('template_html'), 'UNSUBSCRIBE_URL') && !str_contains($request->get('template_html'), 'UNSUBSCRIBE'))
        {
            return response()->json(['icon' => 'warning', 'text' => 'Template harus memiliki Link Unsubscribe']);
        }

        if (!$request->has('subject'))
        {
            return response()->json(['icon' => 'warning', 'text' => 'Email harus memiliki subject']);
        }

        if (!$request->has('name'))
        {
            return response()->json(['icon' => 'warning', 'text' => 'Email harus memiliki Nama']);
        }

    	$template  =   Auth::user()->templates()->find($request->get('id'));
        $template->content_html = urldecode(html_entity_decode($request->get('template_html')));
        /*Remove Semua yang terkait dengan drag size pada halamann dragndrop*/
        $DOM_HTML           =   new \Htmldom($template->content_html);
        foreach ($DOM_HTML->find('.ui-resizable-handle') as $key => $value) {
            $value->outertext    =   "";
        }
        $template->content_html  =  $DOM_HTML->save();
        $template->subject       =  $request->get('subject');
        $template->name          =  $request->get('name');
    	$template->save();

        return response()->json(['icon' => 'success', 'text' => 'Template Tersimpan']);
    }

    public function sendpreview(Request $request)
    {
        $rules  =   [
    	            'id'      => 'required',
                    'template_html'=> 'required',
    	            'email'   => 'required|email',
                    ];


    	$validator = Validator::make($request->all(), $rules);
    	if ($validator->fails()) {
            return response()->json(['icon' => 'warning', 'text' => 'Masukan email yang valid']);
    	    exit;
    	}

        try {
            $email_html = new LiamBuilder($request->get('template_html'));
            $email_html = $email_html->parse();

            $params = ['from'     =>  "Email Preview <email-preview@remail.web.id>",
            'to'        =>  $request->get('email'), 
            'subject'   =>  "Email Testing : ".Auth::user()->name, 
            'html'      =>  $email_html ];
            $request = \cURL::newRequest('post', 'https://api.mailgun.net/v3/remail.web.id/messages', $params)->setUser('api')->setPass(config('services.mailgun.secret'));

            $response = $request->send();
            $response = json_decode($response->body);

            return response()->json(['icon' => 'success', 'text' => 'Silahkan Cek Email Anda']);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
