<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Recipient;
use Illuminate\Validation\Factory;
use App\Jobs\ValidateEmailStatus;

class ListRecipientController extends Controller
{
    public function index(Request $request, $list_id)
    {
    	$list 	=	Auth::user()->lists()->find($list_id);
        $listRecipient 	=	$list->recipients();
    	if ($request->get('q')) {
    	    $listRecipient->where(function($query) use ($request){
    	        $query->orWhere('name','like','%'.$request->get('q').'%');
    	    });
    	}

    	return view('merchant.list.recipients')->with('recipients',$listRecipient->paginate(30))->with('q',$request->get('q',''))->with('list_id',$list_id)->with('list',$list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $list_id)
    {
        return view('merchant.list.recipient.create')->with('list_id',$list_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $list_id)
    {
        $attributes  =   ['name'         =>  'Nama Penerima',
                            'email'      =>  'Email Penerima',
                            'comment'    =>  'Komentar Anda'];

        $rules  =   [
                    'name'         => 'required',
                    'email'        => 'required',
                    'comment'  => ''];


        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $list   =   Auth::user()->lists()->find($list_id);
        if (!$list) {
            return redirect('lists');
        }

        $exists     =   $list->recipients()->where('email','=',$request->get('email'))->count();
        if ($exists) {
            return redirect()->back()->withErrors(['email'=>'Email sebelumnya telah terdaftar pada daftar ini'])->withInput($request->all());
        }

        $recipient                  = new Recipient;
        $recipient->name            = $request->get('name'); 
        $recipient->email           = $request->get('email','');
        $recipient->comment         = $request->get('comment','');
        $recipient->list_id         = $list_id;
        $recipient->user_id         = Auth::user()->id;
        $recipient->save();

        $this->dispatch(new ValidateEmailStatus($recipient->id));

        \Toast::success('Penerima baru berhasil ditambahkan', '');
        return redirect('lists/'.$list_id.'/recipients');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $list_id, $id)
    {
        $recipient  =   Auth::user()->recipients()->find($id);
        return view('merchant.list.recipient.edit')->with('list_id',$list_id)->with('recipient',$recipient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $list_id, $id)
    {
        $attributes  =   ['name'         =>  'Nama Penerima',
                            'email'      =>  'Email Penerima',
                            'comment'    =>  'Komentar Anda'];

        $rules  =   [
                    'name'         => 'required',
                    'email'        => 'required',
                    'comment'  => ''];


        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        $list   =   Auth::user()->lists()->find($list_id);
        if (!$list) {
            return redirect('lists');
        }

        $recipient                  = Auth::user()->lists()->find($list_id)->recipients()->find($id);
        $recipient->name            = $request->get('name'); 
        $recipient->email           = $request->get('email','');
        $recipient->comment         = $request->get('comment','');
        $recipient->list_id         = $list_id;
        $recipient->save();

        $this->dispatch(new ValidateEmailStatus($recipient->id));

        \Toast::success('Penerima berhasil diubah', '');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($list_id, $id)
    {
        $recipient  =  Auth::user()->lists()->find($list_id)->recipients()->find($id);;
        $recipient->delete();

        \Toast::success('Penerima berhasil dihapus', '');
        return redirect('lists/'.$list_id.'/recipients');
    }

    public function export($list_id)
    {

        \Excel::create('Recipients', function($excel) use ($list_id) {
            $excel->sheet('Sheet 1', function($sheet) use ($list_id) {
                $sheet->appendRow(array(
                    'nama', 'email','komentar'
                ));

            foreach (Auth::user()->lists()->find($list_id)->recipients as $key => $recipient) {
                $sheet->appendRow(array(
                    $recipient->name, $recipient->email,$recipient->comment
                ));
            }
            });
        })->download('csv');
    }
}
