<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Encryption\DecryptException;
use Crypt;
use App\Jobs\ValidateEmailStatus;

class ApiController extends Controller
{
    public function opened(Request $request, $id)
    {
    	$id = explode("_",$id);
    	\App\CampaignSpread::where('campaign_id','=',$id[0])
    	            ->where('recipient_id','=',$id[1])
    	            ->update(['status' => 'OPENED']);

    	            return "UPDATED";
    }

    public function clicked(Request $request, $id)
    {
        $id = explode("_",$id);
        \App\CampaignSpread::where('campaign_id','=',$id[0])
                    ->where('recipient_id','=',$id[1])
                    ->update(['status' => 'CLICKED']);

        return redirect(urldecode($request->get('redirect')));
    }

    public function unsubscribe(Request $request, $id)
    {        
        $id = Crypt::decrypt($id);        
        $recipient  =   \App\Recipient::find($id);
        if ($recipient) {
            $recipient->status="UNSUBSCRIBED";
            $recipient->save();
            return view('merchant.api.unsubscribe')->with('recipient',$recipient);
        }
    }

    public function unsubscribeReport(Request $request, $id)
    {        
        $id = Crypt::decrypt($id);        
        $recipient  =   \App\Recipient::find($id);
        if ($recipient) {
            $unsubscribe_report     = new \App\UnsubscribeReport;
            $unsubscribe_report->user_id        = $recipient->user->id;
            $unsubscribe_report->recipient_id   = $recipient->id;
            $unsubscribe_report->save();
            return view('merchant.api.unsubscribe-report')->with('recipient',$recipient);
        }
    }

    public function payment(Request $request)
    {
    	$TransactionMeta = \App\TransactionMeta::where('value','=',$request->get('sid'));
    	if ($TransactionMeta->count()==0) {
    		return response()->json(['status' => 'fail', 'message' => 'Couldn\'t find session ID']);
    	}

    	$Transaction = $TransactionMeta->first()->metable;
    	if ($Transaction->status!="DRAFT") {
    		return response()->json(['status' => 'fail', 'message' => 'Cannot update transaction, transaction have been update before']);
    	}
    	
    	if ($request->get('status')=="berhasil") {
    		$Transaction->status =	"SUCCESS";
    		$Transaction->user->addBalance($Transaction->ammount);
            \Slack::to('#notif-transaction')->send("Ada Topup Lunas");
    	}
    	elseif ($request->get('status')=="pending") {
    		$Transaction->status =	"PENDING";
    	}

    	$Transaction->updateMeta('ipaymu_id',$request->get('trx_id'));

    	$Transaction->save();
    }

    public function recipientCreate(Request $request)
    {
        $user = \App\User::verified()->where('api','=',$request->get('key'));
        if ($user->count()!=1) {
            return response()->json(['status' => 'ERROR', 'message' => 'API KEY TIDAK VALID']);
        }

        $user = $user->first();
        $rules  =   [
                    'name'         => 'required',
                    'email'        => 'required|email',
                    'comment'  => '',
                    'list'         => ''];


        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'message' => 'Field name atau email kosong.']);
        }

        $recipient                  = new \App\Recipient;
        $recipient->name            = $request->get('name'); 
        $recipient->email           = $request->get('email');
        $recipient->comment         = $request->get('comment','');
        $recipient->list_id         = $request->get('list','');
        $recipient->user_id         = $user->id;
        $recipient->save();
        $this->dispatch(new ValidateEmailStatus($recipient->id));
        // foreach ($request->get('tags',['Merchant']) as $key => $tag_id) {
        //     if (!is_numeric($tag_id)) {
        //         $tag                    = new \App\Tag;
        //         $tag->name              = $tag_id; 
        //         $tag->description       = '';
        //         $tag->user_id           = $user->id;
        //         $tag->save();

        //         $tag_id     =   $tag->id;
        //     }

        //     $recipientTag   =   new \App\RecipientTag;
        //     $recipientTag->recipient_id         =   $recipient->id;
        //     $recipientTag->recipient_tag_id     =   $tag_id;
        //     $recipientTag->save();
        // }

        return response()->json(['status' => 'SUCCESS', 'message' => 'Recipient berhasil disimpan.']);
    }

    public function recipientUpdate(Request $request)
    {
        $user = \App\User::verified()->where('api','=',$request->get('key'));
        if ($user->count()!=1) {
            return response()->json(['status' => 'ERROR', 'message' => 'API KEY TIDAK VALID']);
        }

        $rules  =   ['id'         => 'required'];


        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'message' => 'ID harus terisi']);
        }

        $user       =   $user->first();

        $recipient                  = $user->recipients()->find($request->get('id'));
        
        if (!$recipient) {
            return response()->json(['status' => 'ERROR', 'message' => 'Penerima tidak tersedia']);
        }

        if ($request->get('name')) {
            $recipient->name            = $request->get('name'); 
        }
        
        if ($request->get('email')) {
            $recipient->email           = $request->get('email');
        }

        if ($request->get('comment','')) {
            $recipient->comment         = $request->get('comment','');
        }

        if ($request->get('list','')) {
            $recipient->list_id         = $request->get('list','');
        }
        
        $recipient->save();
        $this->dispatch(new ValidateEmailStatus($recipient->id));

        // if ($request->get('tags')) {
        //     $recipient->tags()->delete();
        //     foreach ($request->get('tags',['Merchant']) as $key => $tag_id) {
        //         if (!is_numeric($tag_id)) {
        //             $tag                    = new \App\Tag;
        //             $tag->name              = $tag_id; 
        //             $tag->description       = '';
        //             $tag->user_id           = $user->id;
        //             $tag->save();

        //             $tag_id     =   $tag->id;
        //         }

        //         $recipientTag   =   new \App\RecipientTag;
        //         $recipientTag->recipient_id         =   $recipient->id;
        //         $recipientTag->recipient_tag_id     =   $tag_id;
        //         $recipientTag->save();
        //     }
        // }

        return response()->json(['status' => 'SUCCESS', 'message' => 'Recipient berhasil diupdate.']);
    }

    public function recipientDelete(Request $request)
    {
        $user = \App\User::verified()->where('api','=',$request->get('key'));
        if ($user->count()!=1) {
            return response()->json(['status' => 'ERROR', 'message' => 'API KEY TIDAK VALID']);
        }

        $rules  =   ['id'         => 'required'];

        $validator = \Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'message' => 'ID harus terisi']);
        }

        $user       =   $user->first();
        $recipient                  = $user->recipients()->find($request->get('id'));
        if (!$recipient) {
            return response()->json(['status' => 'ERROR', 'message' => 'Penerima tidak tersedia']);
        }

        $recipient->delete();
        return response()->json(['status' => 'SUCCESS', 'message' => 'Recipient berhasil dihapus.']);
    }

    public function recipientList(Request $request)
    {
        $user = \App\User::verified()->where('api','=',$request->get('key'));
        if ($user->count()!=1) {
            return response()->json(['status' => 'ERROR', 'message' => 'API KEY TIDAK VALID']);
        }

        $user       =   $user->first();
        $recipients     =   $user->recipients();
        $recipients->take($request->get('item',20));
        $recipients->skip(($request->get('page',1)-1) * $request->get('item',20));

        /*Build Data*/

        foreach ($recipients->get() as $key => $value) {
            $tags   =   array();
            foreach ($value->tags()->get() as $key => $tag) {
                $tags[]     =   $tag->tag->name;
            }

            $data[]     =   ["name"     =>  $value->name,
                            "email"     =>  $value->email,
                            "comment"   =>  $value->comment,
                            "tags"      =>  $tags,
                            ];
        }

        return response()->json(['status' => 'SUCCESS', 'data' => ['recipients'=>$data,'item'=>$request->get('item',20),'page'=>$request->get('page',1)]]);
    }

    public function recipient(Request $request)
    {
        // Method CREATE, PATCH, DESTROY
        // Mencari api key
        $user = \App\User::verified()->where('api','=',$request->get('key'));
        if ($user->count()!=1) {
            return response()->json(['status' => 'ERROR', 'message' => 'API KEY TIDAK VALID']);
        }

        $user = $user->first();

        $rules  =   [
                    'name'         => 'required',
                    'email'        => 'required',
                    'comment'  => '',
                    'tags'         => ''];


        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'message' => 'Field name atau email kosong.']);
        }

        switch ($request->get('method')) {
            case 'CREATE':
                $recipient                  = new \App\Recipient;
                $recipient->name            = $request->get('name'); 
                $recipient->email           = $request->get('email','');
                $recipient->comment         = $request->get('comment','');
                $recipient->user_id         = $user->id;
                $recipient->save();
                foreach (explode(",",$request->get('tag')) as $key => $tag_id) {
                    if (!is_numeric($tag_id)) {
                        $tag                    = new \App\Tag;
                        $tag->name              = $tag_id; 
                        $tag->description       = '';
                        $tag->user_id           = $user->id;
                        $tag->save();

                        $tag_id     =   $tag->id;
                    }

                    $recipientTag   =   new \App\RecipientTag;
                    $recipientTag->recipient_id         =   $recipient->id;
                    $recipientTag->recipient_tag_id     =   $tag_id;
                    $recipientTag->save();
                }

                return response()->json(['status' => 'SUCCESS', 'message' => 'Recipient berhasil disimpan.']);
                break;

            case 'PATCH':
                
                break;

            case 'DESTROY':
                
                break;

            case 'LIST':
                dd($user->recipients()->get());
                break;
        }
    }

    public function webhookDelivered(Request $request, $domain)
    {
        // Check Domain Valid
        $domainModel     =   \App\Domain::where('name','=',$domain)->first();
        if (!$domainModel) {
            exit('{"message":"Domain not found"}');
        }

        $campaignSpread     =   \App\CampaignSpread::where('mailgun_id','=',$request->get('Message-Id'))->first();
        if (!$campaignSpread) {
            exit('{"message":"Message ID Not Found"}');
        }

        \App\CampaignSpread::where('mailgun_id','=',$request->get('Message-Id'))->update(['status'=>'SENT']);    

        $recipient          =   $campaignSpread->recipient;
        $recipient->status  =   "VALID";
        $recipient->save();

        exit('{"message":"Post received. Thanks!"}');
    }

    public function webhookDropped(Request $request, $domain)
    {
        // Check Domain Valid
        $domainModel     =   \App\Domain::where('name','=',$domain)->first();
        if (!$domainModel) {
            exit('{"message":"Domain not found"}');
        }

        $campaignSpread     =   \App\CampaignSpread::where('mailgun_id','=',$request->get('Message-Id'))->first();
        if (!$campaignSpread) {
            exit('{"message":"Message ID Not Found"}');
        }

        \App\CampaignSpread::where('mailgun_id','=',$request->get('Message-Id'))->update(['status'=>'BOUNCED']);

        exit('{"message":"Post received. Thanks!"}');
    }

    public function webhookBounced(Request $request, $domain)
    {
        // Check Domain Valid
        $domainModel     =   \App\Domain::where('name','=',$domain)->first();
        if (!$domainModel) {
            exit('{"message":"Domain not found"}');
        }

        $campaignSpread     =   \App\CampaignSpread::where('mailgun_id','=',$request->get('Message-Id'))->first();
        if (!$campaignSpread) {
            exit('{"message":"Message ID Not Found"}');
        }

        \App\CampaignSpread::where('mailgun_id','=',$request->get('Message-Id'))->update(['status'=>'BOUNCED']);    

        $recipient          =   $campaignSpread->recipient;
        $recipient->status  =   "INVALID";
        $recipient->save();

        exit('{"message":"Post received. Thanks!"}');
    }
}
