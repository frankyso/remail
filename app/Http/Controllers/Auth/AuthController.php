<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Contracts\Encryption\DecryptException;

use Auth;
use Crypt;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    public function getLogin()
    {

        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $rules      = [
                        'email'          => 'required',
                        'password'         => 'required',
                    ];
        $validator  = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }

        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            if (Auth::user()->role == config('wifimu.role.administrator')) {
                return redirect(url(config('wifimu.prefix_url.administrator')."/overview"));
            }

            return redirect('overview');
        }

        return redirect()->back();
    }

    public function getRegister(Request $request)
    {
        session()->put('parent_id', $request->get('id'));
        return view('auth.register')->with('email', $request->get('email'));
    }

    public function postRegister(Request $request)
    {
        $rules      = [
                        'name'          => 'required',
                        'email'         => 'required|unique:users,email',
                        'password'      => 'required',
                        'confirmation'  => 'required|same:password',
                        'g-recaptcha-response' => 'required|recaptcha'
                    ];

        $validator  = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($request->except('password'));
        }
        
        $user = new User;
        $user->name     =   $request->get('name');
        $user->email    =   $request->get('email');
        $user->password =   bcrypt($request->get('password'));
        $user->role     =   'MERCHANT';
        $user->verified =   0;
if(session()->get('parent_id', 0))
{
$user->parent_id=   session()->get('parent_id', 0);
}
        $user->api      =   str_random(40);
        $user->save();

        $user->sendEmailConfirmation();

        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            return redirect('overview');
        }

        return redirect()->back();
    }

    public function postResendConfirmation()
    {
        Auth::user()->sendEmailConfirmation();
        \Toast::success('Email konfirmasi telah dikirimkan, silahkan cek Email Anda.', '');
        return redirect()->back();
    }

    public function getForgetPassword()
    {
        return view('auth.forgot');
    }

    public function getForgetPasswordSent()
    {
        return view('auth.forgotSent');
    }

    public function postForgetPassword(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = \Password::sendResetLink($request->only('email'), function (Message $message) {
           $message->subject("Your Password Reset Link");
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect('auth/forgot/sent');

            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }

    public function getResetPassword(Request $request, $token=null)
    {
        if (is_null($token)) {
           throw new NotFoundHttpException;
        }

        return view('auth.reset')->with('token', $token)->with('email', $request->get('email'));
    }

    public function postResetPassword(Request $request)
    {
        $this->validate($request, [
           'token' => 'required',
           'email' => 'required|email',
           'password' => 'required|confirmed',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
            Auth::login($user);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return redirect('/');

            default:
                return redirect()->back()
                           ->withInput($request->only('email'))
                           ->withErrors(['email' => trans($response)]);
       }
    }

    public function getActivateAccount($key)
    {
        $id     =   Crypt::decrypt($key);
        $user   =   \App\User::findOrFail($id);
        $user->verified     =   1;
        // $user->expire_at   =   \Carbon\Carbon::now()->addDays(7);
        $user->save();
        
        $user->parentReward();

        \Slack::to('#notif-user')->send("Ada user baru register, please make sure kolam droplet masih mencukupi untuk pengguna kita.");

        return redirect('/');
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
