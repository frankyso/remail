<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipientTag extends Model
{
    protected $table = 'recipients_tag_relation';
    public $timestamps = false;

    public function tag()
    {
        return $this->belongsTo('App\Tag','recipient_tag_id');
    }

    public function recipient()
    {
        return $this->belongsTo('App\Recipient','recipient_id');
    }
}
