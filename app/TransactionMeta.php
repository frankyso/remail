<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionMeta extends \Phoenix\EloquentMeta\Meta
{
    protected $table = 'transactions_meta';
}
