<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListRecipient extends Model
{
    protected $table = 'lists';

    public function recipients()
    {
    	return $this->hasMany('App\Recipient','list_id');
    }
}
