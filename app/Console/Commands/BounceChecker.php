<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class BounceChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:bounceChecker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengecek Bounce';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*Memulai Masuk kedalam Email*/
        $this->info("Connecting to IMAP");
        $imap = imap_open("{mail.remail.web.id:143/imap/novalidate-cert}INBOX", "bounce@remail.web.id", "2iuINwIUaAXh");
        /* grab emails */
        $emails = imap_search($imap,'ALL');
        /* if emails are returned, cycle through each... */
        if($emails) {
            /* for every email... */
            foreach($emails as $email_number) {
                $matches = array();
                $message = imap_fetchbody($imap,$email_number,2);
                $pattern = '/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b/i';
                preg_match_all($pattern, $message, $matches);
                foreach ($matches[0] as $key => $value) {
                    $this->info($value);
                    /*Langsung Update KeDatabase*/
                    $recipients = \App\Recipient::where('email','=',$value);
                    foreach ($recipients->get() as $key => $recipient) {
                        if ($recipient) {
                            $recipient->status  =   "INVALID";
                            $recipient->save();

                            \App\CampaignSpread::where('recipient_id','=',$recipient->id)->update(['status' => 'BOUNCED']);
                        }
                    }

                    imap_delete($imap, $email_number);
                }
            }
        }
        imap_expunge($imap);
        imap_close($imap);
    }
}


