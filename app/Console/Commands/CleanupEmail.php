<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class CleanupEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recipient:cleanUp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengcleanup Email yang ada Spasi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*Memulai Masuk kedalam Email*/
        \App\Recipient::where('email','like','% %')->chunk(200, function ($recipients) {
            foreach ($recipients as $recipient) {
                $recipient->email   =   preg_replace('/\s+/', '', $recipient->email);
                $recipient->save();
                $this->info($recipient->email);
            }
        });
    }
}


