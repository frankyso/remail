<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class PullGit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pullgit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull All Git';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $droplets = \App\Droplet::where('id','!=','');
        $bar = $this->output->createProgressBar($droplets->count());
        foreach ($droplets->get() as $droplet) {
            /*Access to domain to make nameserver*/
            config(['remote.connections.production'=>['host'=>$droplet->public_ip,
                                                'username'=>Crypt::decrypt($droplet->username),
                                                'password'=>Crypt::decrypt($droplet->password)]]);

            /*Renaming Hostname*/
            // $domain->name
            try {
                \SSH::run([
                    "cd /home/remail-client/",
                    "git pull https://frankyso:rain180511@bitbucket.org/frankyso/remail-client.git",
                    "composer update",
                ]);
            } catch (\Exception $e) {
                \Slack::to('#notif-droplet')->send("Saat Upgrade, Droplet ".$droplet->name." : Tidak dapat dihubungi");
                $this->line("Saat Upgrade, Droplet ".$droplet->name." : Tidak dapat dihubungi");
            }


            
            $bar->advance();
        }
        $bar->finish();
    }
}
