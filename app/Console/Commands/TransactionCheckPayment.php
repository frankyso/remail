<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TransactionCheckPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:topup:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check payment transaction from iPaymu';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        /*Deleting Status Draft*/
        $transactions = \App\Transaction::where('type','=','TOPUP')->where('status','=','DRAFT');
        foreach ($transactions->get() as $transaction) {
            $difference_days = \Carbon\Carbon::createFromTimeStamp(strtotime($transaction->created_at))->diffInDays(\Carbon\Carbon::now(), false);
            if ($difference_days > 3) {
                $this->info("Transaction Deleted ID : {$transaction->id}");
                $transaction->delete();
            }
        }


        $this->line('Checkin Users who has pending Payment');
        $transactions = \App\Transaction::where('type','=','TOPUP')->where('status','=','PENDING');
        $bar = $this->output->createProgressBar($transactions->count());
        $iPaymu = \App\Http\Utilities\iPaymu::api(config('wifimu.ipaymu_key'));
        foreach ($transactions->get() as $transaction) {
            $ipaymu_trasaction = $iPaymu->transaction($transaction->getMeta('ipaymu_id',0));
            if ($ipaymu_trasaction->Status==0) {
                $difference_days = \Carbon\Carbon::createFromTimeStamp(strtotime($transaction->created_at))->diffInDays(\Carbon\Carbon::now(), false);
                if ($difference_days > 3) {
                    $this->info("Transaction Deleted ID : {$transaction->id}");
                    $transaction->delete();
                }
                else
                {
                    $this->info("Transaction Still Pending ID : {$transaction->id}");
                }
            }

            if ($ipaymu_trasaction->Status==1) {
                $transaction->status =  "SUCCESS";
                $transaction->user->addBalance($transaction->ammount);
                $transaction->save();
                $this->info("Transaction Success ID : {$transaction->id}");
            }

            $bar->advance();
        }

        $bar->finish();
    }
}
