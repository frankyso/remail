<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class DomainTester extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domain:tester';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Untuk mengingatkan pengguna yang dropletnya akan mati setiap 3 hari sekali';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Mencari pengguna yang akan expired seminggu lagi dan telah berakhir setelah seminggu');
        $domains    =   \App\Domain::get();
        foreach ($domains as $key => $domain) {
            $domain->checkDNS(true);
        }
    }
}
