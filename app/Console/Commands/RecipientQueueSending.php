<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Http\Utilities\LiamBuilder;
use App\Jobs\SendingEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class RecipientQueueSending extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:RecipientQueueSending {id_campaign}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Menambahkan semua pengguna ke Queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $campaigns       = \App\Campaign::find($this->argument('id_campaign'));
        $campaigns->spread()->queue()->chunk(200, function ($spreads) {
            foreach ($spreads as $spread) {
                var_dump($spread->recipient);
                if(!$spread->recipient)
                {
                    \App\CampaignSpread::where('campaign_id','=',$spread->campaign_id)
                        ->where('recipient_id','=',$spread->recipient_id)
                        ->delete();
                        continue;
                }
                
                $domain_name        =   $spread->campaign->domain->name;
                $email_to           =   $spread->recipient->email;
                $email_from_email   =   $spread->campaign->sender_email;
                $email_from_name    =   $spread->campaign->sender_name;
                $email_subject      =   $spread->campaign->template->subject;
                $email_html         =   new LiamBuilder($spread->campaign->template->content_html);
                $email_html         =   $email_html->parse($spread);

                $this->dispatch(new SendingEmail($domain_name,
                                                ['from'     =>  "$email_from_name <$email_from_email>", 
                                                'to'        =>  "$email_to", 
                                                'subject'   =>  $email_subject, 
                                                'html'      =>  $email_html ], 
                                                $spread->campaign_id, 
                                                $spread->recipient_id));

                /*Dont Add Anymore if on mailgun Queue*/
                
            }
        });
    }
}
