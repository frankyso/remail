<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\ValidateEmailStatus;

class NewMigrate extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate DKIM untuk current user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (\App\Recipient::where('status','=','UNVERIFIED')->get() as $key => $value) {
            var_dump($value->id);
            $this->dispatch(new ValidateEmailStatus($value->id));
        }
    }
}
