<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class AddCampaignRecipients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:recipientAdd {id_campaign} {list}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Background Process untuk campaign recipient';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $id_campaign    = $this->argument('id_campaign');
        $list           = $this->argument('list');
        $campaign       =   \App\Campaign::find($id_campaign);

        if ($campaign==null) {
            exit;
        }

        /*Looping to Get Users*/
        $recipient = $campaign->user->recipients()->whereHas('listRecipient', function($query) use ($list){
            $query->where('list_id','=',$list);
        })->where(function($query){
            /*Hanya yang UNVERIFIED & VERIFIED*/
            $query->where('status','=','VALID');
            $query->orWhere('status','=','UNVERIFIED');
        });

        $this->info('Starting Chunking File');
        $recipient->chunk(1000, function ($recipient_array) use ($campaign) {
            $this->info('Loopoing Inside a Thousand');
            
            $spread = array();
            foreach ($recipient_array as $value) {
                $spread[]   =   ['campaign_id'=>$campaign->id,
                                'recipient_id'=>$value->id,
                                'status'=>'QUEUE',
                                'created_at'=> \Carbon\Carbon::now(),
                                'updated_at'=>\Carbon\Carbon::now()
                                ];
            }
            \App\CampaignSpread::insert($spread);
        });
    }
}


