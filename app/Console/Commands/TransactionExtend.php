<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TransactionExtend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:extend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extending Package if user has expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Get user who has expired and have money');
        $users   =   \App\User::where('balance','>=',config('remail.price'))
                                ->where('expire_at','<',\Carbon\Carbon::now());

        foreach ($users as $key => $user) {
            /*Extend Plan*/
            $user->expire_at    =   \Carbon\Carbon::createFromTimeStamp(strtotime($user->expire_at))->addMonths(1);
            $user->balance      =   $user->expire_at - config('remail.price');
            $user->save();
        }

        /*Add Into Transaction*/
        $transaction = new \App\Transaction;
        $transaction->type      =   "PLAN";
        $transaction->ammount   =   -config('remail.price');
        $transaction->status    =   "SUCCESS";
        $transaction->user_id   =   $user->id;
        $transaction->save();
    }
}
