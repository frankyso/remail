<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Utilities\LiamBuilder;

class SpreadEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spread:sendEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending Email Throught SMTP Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Mulai menulis lagi script pengiriman email yang menggunakan mailgun
        // Mencari Campaign Spread dengan status Sending
        $campaign_spread_model      =   \App\CampaignSpread::whereHas('campaign',function($campaign){
                                            $campaign->sending();
                                            $campaign->whereHas('domain',function($domain){
                                                $domain->where('validated','=','1');
                                            });
                                            $campaign->whereHas('user',function($user){
                                                $user->where('balance','>=', config('remail.price'));
                                            });
                                        })->queue();

        foreach ($campaign_spread_model->get() as $key => $campaign_spread_value) {

            if ($campaign_spread_value->recipient->status=="INVALID") {
                \App\CampaignSpread::where('campaign_id','=',$campaign_spread_value->campaign_id)
                            ->where('recipient_id','=',$campaign_spread_value->recipient_id)
                            ->update(['status' => 'BOUNCED','sent_at'=>date("Y-m-d H:i:s")]);
                continue;
            }
            elseif ($campaign_spread_value->recipient->status=="UNVERIFIED") {
                try {
                    $response = \cURL::get('http://techstroke.com/email-availability-checker/?email='.$campaign_spread_value->recipient->email);
                    $result = $response->body;
                } catch (\Exception $e) {
                    $result = "Already Exists";
                }
                if (str_contains($result,"Already Exists")) {
                    \App\CampaignSpread::where('campaign_id','=',$campaign_spread_value->campaign_id)
                                                ->where('recipient_id','=',$campaign_spread_value->recipient_id)
                                                ->update(['status' => 'VALID','sent_at'=>date("Y-m-d H:i:s")]);
                    $recipient  =   $campaign_spread_value->recipient;
                    $recipient->status  =   "VALID";
                    $recipient->save();
                }           
                if (str_contains($result,"No such Email address exists")) {
                    \App\CampaignSpread::where('campaign_id','=',$campaign_spread_value->campaign_id)
                                                ->where('recipient_id','=',$campaign_spread_value->recipient_id)
                                                ->update(['status' => 'BOUNCED','sent_at'=>date("Y-m-d H:i:s")]);
                    $recipient  =   $campaign_spread_value->recipient;
                    $recipient->status  =   "INVALID";
                    $recipient->save();
                }
            }

            $domain_name        =   $campaign_spread_value->campaign->domain->name;
            $email_to           =   $campaign_spread_value->recipient->email;
            $email_from_email   =   $campaign_spread_value->campaign->sender_email;
            $email_from_name    =   $campaign_spread_value->campaign->sender_name;
            $email_subject      =   $campaign_spread_value->campaign->template->subject;
            $email_html         =   new LiamBuilder($campaign_spread_value->campaign->template->content_html);
            $email_html         =   $email_html->parse($campaign_spread_value);

            // Mengirim Email Ke Mailgun
            $request = \cURL::newRequest('post', 'https://api.mailgun.net/v3/'.$domain_name.'/messages', 
                ['from'     =>  "$email_from_name <$email_from_email>",
                'to'        =>  "$email_to",
                'subject'   =>  $email_subject,
                'html'      =>  $email_html
                ])
                ->setUser('api')->setPass(config('services.mailgun.secret'));

            $response = $request->send();
            $response = json_decode($response->body);

            \App\CampaignSpread::where('campaign_id','=',$campaign_spread_value->campaign_id)
                        ->where('recipient_id','=',$campaign_spread_value->recipient_id)
                        ->update(['status' => 'MAILGUN_QUEUE','mailgun_id' => $response->id,'sent_at'=>date("Y-m-d H:i:s")]);

            $user   =   $campaign_spread_value->campaign->user;
            $user->balance  -= config('remail.price');
            $user->save();

            $user->parentReward(true, config('remail.price'));



            // Cek semua campaign yang telah selesai dikirimkan
            $campaign   =   $campaign_spread_value->campaign;
            
            if ($campaign->spread()->queue()->count() == 0) {
                $campaign->status   =   'SENT';
                $campaign->save();
            };
        }
    }
}
