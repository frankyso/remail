<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class GenerateDKIM extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dkim';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate DKIM untuk current user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $domains    =   \App\Domain::whereHas('droplets',function(){});
        foreach ($domains->get() as $key => $domain) {
            foreach ($domain->droplets()->get() as $key => $value) {
                config(['remote.connections.production'=>['host'=>$value->public_ip,
                                                    'username'=>Crypt::decrypt($value->username),
                                                    'password'=>Crypt::decrypt($value->password)]]);

                /*Renaming Hostname*/
                // $domain->name
                try {
                    /*Better Check if it's has private key*/
                    if (!$domain->privateKey()) {
                        /*Generate Key*/
                        \SSH::run([
                            "rm -f /root/mail.*",
                            "rm -f /etc/postfix/dkim.key",
                            "cd /root/",
                            "opendkim-genkey -t -s mail -d ".$domain->name,
                            "cp mail.private /etc/postfix/dkim.key"
                        ]);

                        $publicKeyParsing   =   \SSH::getString('/root/mail.txt');
                        $publicKeyParsing   =   explode("p=", $publicKeyParsing);
                        $publicKeyParsing   =   substr($publicKeyParsing[1], 0, strrpos($publicKeyParsing[1], '"'));

                        $domain->publicKey("v=DKIM1; g=*; k=rsa; p=".$publicKeyParsing);
                        $domain->privateKey(\SSH::getString('/root/mail.private'));

                        $this->info("Generate New Key");
                    }
                    else
                    {
                        \SSH::putString('/etc/postfix/dkim.key', $domain->privateKey());
                        \SSH::run([
                            "chmod 600 /etc/postfix/dkim.key"
                        ]);

                        $this->info("Set Key to current domain");
                    }
                } catch (\Exception $e) {
                    \Slack::to('#notif-droplet')->send("Droplet ".$value->name." : Tidak dapat dihubungi. ".$e->getMessage());
                }
            }
        }
    }
}
