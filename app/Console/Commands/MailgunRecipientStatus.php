<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MailgunRecipientStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mailgun:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate DKIM untuk current user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $campaign_spread_model      =   \App\CampaignSpread::mailgun();

        /*Ganti nama file lama remail dengan yang baru*/
        foreach ($campaign_spread_model->get() as $key => $value) {
            $url    =   \cURL::buildUrl('https://api.mailgun.net/v3/'.$value->campaign->domain->name.'/events', 
                ['limit'         => '5',
                'message-id'     =>  '"'.$this->removeBracket($value->mailgun_id).'"',
                'event'=>'rejected OR failed OR delivered OR opened OR clicked unsubscribed OR complained']);

            $request = \cURL::newRequest('GET', $url)
                ->setUser('api')->setPass(config('services.mailgun.secret'));
            $result     =   json_decode($request->send()->body);
            
            try {
            	$event     =   $result->items[0]->event;
            } catch (\Exception $e) {
            	continue;
            }

            switch ($event) {
                case 'delivered':
                    \App\CampaignSpread::where('campaign_id','=',$value->campaign_id)
                                ->where('recipient_id','=',$value->recipient_id)
                                ->update(['status' => 'SENT','sent_at'=>date("Y-m-d H:i:s")]);
                    break;
                case 'failed':
                {
                    if ($result->items[0]->severity=="temporary") {
                        continue;
                    }

                    \App\CampaignSpread::where('campaign_id','=',$value->campaign_id)
                                                    ->where('recipient_id','=',$value->recipient_id)
                                                    ->update(['status' => 'BOUNCED','sent_at'=>date("Y-m-d H:i:s")]);
                    $recipient          =   $value->recipient;
                    $recipient->status  =   "INVALID";
                    $recipient->save();
                }
                    break;
                case 'rejected':
                    \App\CampaignSpread::where('campaign_id','=',$value->campaign_id)
                                ->where('recipient_id','=',$value->recipient_id)
                                ->update(['status' => 'REJECTED','sent_at'=>date("Y-m-d H:i:s")]);
                    break;
                case 'complained':
                {
                    \App\CampaignSpread::where('campaign_id','=',$value->campaign_id)
                                ->where('recipient_id','=',$value->recipient_id)
                                ->update(['status' => 'COMPLAINED','sent_at'=>date("Y-m-d H:i:s")]);

                    $recipient          =   $value->recipient;
                    $recipient->status  =   "UNSUBSCRIBED";
                    $recipient->save();

                    break;
                }
                case 'accepted':
                    
                    break;
                
                default:
                {
                    \App\CampaignSpread::where('campaign_id','=',$value->campaign_id)
                                                    ->where('recipient_id','=',$value->recipient_id)
                                                    ->update(['status' => 'SENT','sent_at'=>date("Y-m-d H:i:s")]);

                    $recipient          =   $value->recipient;
                    $recipient->status  =   "VALID";
                    $recipient->save();
                    break;
                }
            }
        }
    }

    public function removeBracket($string)
    {
        $string     =   str_replace("<", "", $string);
        $string     =   str_replace(">", "", $string);
        return $string;
    }
}
