<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class ReminderPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Untuk mengingatkan pengguna yang dropletnya akan mati setiap 3 hari sekali';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Mencari pengguna yang akan expired seminggu lagi dan telah berakhir setelah seminggu');
        $domains    =   \App\Domain::whereHas('droplets',function(){});
        foreach ($domains->get() as $key => $domain) {
            $domain->checkDNS();


            $beforeExpire   =   $domain->expire_at->subDays(7);
            $afterExpire    =   $domain->expire_at->addDays(14);
            $now            =   \Carbon\Carbon::now();

            // dd(($now >= $beforeExpire));
            // dd(($now <= $afterExpire));
            if ($now >= $beforeExpire && $now <= $afterExpire) {
                // Kirimkan Email Notifikasinya dulu
                $data['domain']     =    $domain->name;
                $data['date']       =    $domain->expire_at->format('d M Y');
                $email  =  $domain->user->email;
                $name   =  $domain->user->name;
                \Mail::send('email.dropletExtendReminder', $data, function ($m) use ($email, $name) {
                    $m->to($email, $name)->subject('Domain Reminder');
                });
            }

            /*Jika Sudah Habis setelah seminggu*/
            if ($now > $afterExpire) {
                /*Cabut Dedicated IPnya*/
                $data['domain']     =    $domain->name;
                $email  =  $domain->user->email;
                $name   =  $domain->user->name;
                \Mail::send('email.dropletExtendReminder', $data, function ($m) use ($email, $name) {
                    $m->to($email, $name)->subject('Domain Reminder');
                });


                foreach ($domain->droplets()->get() as $key => $droplet) {
                    /*Access to domain to make nameserver*/
                    config(['remote.connections.production'=>['host'=>$droplet->droplet->public_ip,
                                                        'username'=>Crypt::decrypt($droplet->droplet->username),
                                                        'password'=>Crypt::decrypt($droplet->droplet->password)]]);


                    /*Renaming Hostname*/
                    // $domain->name
                    try {
                        \SSH::run([
                            "sed -i -e 's/"."remail.".$domain->name."/".$droplet->droplet->name."/g' /etc/hosts",
                            "sed -i -e 's/"."remail.".$domain->name."/".$droplet->droplet->name."/g' /etc/hostname",
                            "sed -i -e 's/"."remail.".$domain->name."/".$droplet->droplet->name."/g' /etc/postfix/main.cf",
                            "reboot"
                        ]);
                    } catch (Exception $e) {
                        \Slack::to('#notif-droplet')->send("Droplet ".$value->name." : Tidak dapat dihubungi. Saat Menghapus dari console");
                    }

                    $droplet->delete();
                    \Slack::to('#notif-droplet')->send("Droplet ".$droplet->droplet->name." Dikembalikan ke Kolam droplet");
                }
            }
        }
    }
}
