<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DropletBalancer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'droplet:balancer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'mengatur keseimbangan antara droplet pengirim email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (\App\Droplet::whereHas('domains',function($query){})->get() as $key => $value) {
            /*Droplet yang memiliki domain tetapi tidak memiliki campaign berstatus sending atau queue*/
            $campaign_ammount = $value->domains()->first()->domain()->first()->campaigns()->where('status','=','QUEUE')->orWhere('status','=','SENDING')->count();

            if ($campaign_ammount==0) {
                $value->domains()->first()->domain()->first()->attachDroplets(0);
            }

            if ($value->status=="RESTART") {
                /*Cek Dengan PING*/
                try {
                    $response     =   \cURL::get($value->public_ip);
                } catch (\Exception $e) {
                    continue;
                }

                if ($response->statusCode==200) {
                    $value->status  =   "ONLINE";
                    $value->save();
                }
            }
        }

        // Mengatur droplet yang dapat digunakan
        $droplets = \App\Droplet::whereHas('domains',function($query){},0);

        /*Mengembalikan droplet yang berstatus Restart*/
        foreach ($droplets->get() as $key => $value) {
            if ($value->status=="RESTART") {
                /*Cek Dengan PING*/
                try {
                    $response     =   \cURL::get($value->public_ip);
                } catch (\Exception $e) {
                    continue;
                }
                if ($response->statusCode==200) {
                    $value->status  =   "ONLINE";
                    $value->save();
                }
            }
        }

        $available_droplets = $droplets->count() - 2;
        // $available_droplets = $droplets->count() - 0;
        if ($available_droplets < 0 || $droplets->count()==0) {
            return;
        }

        // Balancing dengan campaign yang berstatus Antrian atau Kirim
        $campaigns = \App\Campaign::where('status','=','QUEUE')->orWhere('status','=','SENDING')->orderBy('updated_at','ASC')->take(3);

        // Menghitung total Email yang dikirim
        $total_recipients   =   0;
        foreach ($campaigns->get() as $key => $value) {
            $total_recipients += $value->spread()->count();
        }

        /*Membagi rata droplet yang ada*/
        foreach ($campaigns->get() as $key => $value) {
            $campaign_percentage_for_droplet    =   ceil(($value->spread()->count() / $total_recipients) * 100);
            var_dump($campaign_percentage_for_droplet." Persentase Droplet");
            $droplets_allocation                =   (integer) ceil(($campaign_percentage_for_droplet * $available_droplets) / 100);

            if ($droplets_allocation > $available_droplets) {
                $droplets_allocation    =   $available_droplets;
            }

            var_dump($available_droplets." Available Droplet");
            var_dump($droplets_allocation." Alokasi Droplet");
            var_dump("---------------------------------------------");
            $value->domain->attachDroplets($droplets_allocation);
        }


        // $domains    =   \App\Domain::whereHas('droplets',function(){});
        // foreach ($domains->get() as $key => $domain) {
        //     $domain->checkDNS(true);
        // }
    }
}
