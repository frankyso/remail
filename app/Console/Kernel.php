<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SpreadEmail::class,
        Commands\TransactionCheckPayment::class,
        Commands\TransactionExtend::class,
        Commands\ReminderPayment::class,
        Commands\DomainTester::class,
        Commands\PullGit::class,
        Commands\AddCampaignRecipients::class,
        Commands\BounceChecker::class,
        Commands\CleanupEmail::class,
        Commands\GenerateDKIM::class,
        Commands\DropletBalancer::class,
        Commands\MailgunRecipientStatus::class,
        Commands\NewMigrate::class,
        Commands\RecipientQueueSending::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // $schedule->command('reminder:payment')
        //          ->daily()->mondays()->thursdays();

        $schedule->command('transaction:topup:check')
                 ->everyThirtyMinutes()->withoutOverlapping();

        // $schedule->command('spread:sendEmail')
        //          ->everyMinute()->withoutOverlapping();

        $schedule->command('domain:tester')
                 ->daily()->mondays()->thursdays()->withoutOverlapping();

        // $schedule->command('mailgun:status')
        //          ->everyMinute()->withoutOverlapping();

        // $schedule->command('droplet:balancer')
        //          ->everyMinute();

        // $schedule->command('recipient:cleanUp')
        //          ->hourly()->withoutOverlapping();
    }
}
