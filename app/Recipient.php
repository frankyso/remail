<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
    protected $table = 'recipients';

    // public static function boot()
    // {
    //     parent::boot();

    //     static::created(function($recipient)
    //     {
    //         $this->dispatch(new ValidateEmailStatus($recipient->id));
    //     });

    //     static::updated(function($recipient)
    //     {
    //         $this->dispatch(new ValidateEmailStatus($recipient->id));
    //     });
    // }

    public function tags()
    {
        return $this->hasMany('App\RecipientTag','recipient_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
    	switch ($this->status) {
    		case 'INVALID':
    			return "<span class='label label-danger'>INVALID</span>";
    			break;

    		case 'VALID':
    			return "<span class='label label-success'>VALID</span>";
    			break;

            case 'UNSUBSCRIBED':
                return "<span class='label label-default'>UNSUBSCRIBED</span>";
                break;
    		
    		default:
    			return "<span class='label label-default'>UNVERIFIED</span>";
    			break;
    	}
    }

    public function listRecipient()
    {
        return $this->belongsTo('App\ListRecipient','list_id');
    }
}
