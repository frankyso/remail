<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'campaigns';

    public function template()
    {
        return $this->belongsTo('App\Template','template_id');
    }

    public function spread()
    {
        return $this->hasMany('App\CampaignSpread','campaign_id');
    }

    public function domain()
    {
    	return $this->belongsTo('App\Domain','domain_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function scopeSent($query)
    {
        $query->where('status','=', 'SENT');
    }

    public function scopeWaiting($query)
    {
        $query->where('status','=', 'WAITING');
    }

    public function scopeSending($query)
    {
        $query->where('status','=', 'SENDING');
    }
    public function scopeQueue($query)
    {
        $query->where('status','=', 'QUEUE');
    }

    public function status()
    {
        switch ($this->status) {
            case 'QUEUE':
                return "<span class='label label-warning'>ANTRIAN</span>";
                break;

            case 'SENT':
                return "<span class='label label-success'>TERKIRIM</span>";
                break;

            case 'SENDING':
                return "<span class='label label-info'>MENGIRIMKAN</span>";
                break;

            case 'PREPARING':
                return "<span class='label label-default'>MENYIAPKAN</span>";
                break;
            
            default:
                return "<span class='label label-default'>SIAP DIKIRIM</span>";
                break;
        }
    }

    public function action()
    {
        switch ($this->status) {
            case 'QUEUE':
                // return "<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Broadcast Hentikan Pengiriman Email\" onclick=\"return false;\" href=\"/campaigns/".$this->id."?_token=".csrf_token()."&_method=PATCH&status=WAITING\" class=\"btn btn-danger btn-xs confirm-post\"><span class=\"fa fa-stop-circle\"></span></a>";
                break;

            case 'SENDING':
                return "";
                // return "<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Broadcast Hentikan Pengiriman Email\" onclick=\"return false;\" href=\"/campaigns/".$this->id."?_token=".csrf_token()."&_method=PATCH&status=WAITING\" class=\"btn btn-danger btn-xs confirm-post\"><span class=\"fa fa-stop-circle\"></span></a>";
                break;

            case 'SENT':
                return "";
                break;

            case 'PREPARING':
                return "";
                break;
            
            default:
                return "<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Broadcast Email Marketing\" onclick=\"return false;\" href=\"/campaigns/".$this->id."?_token=".csrf_token()."&_method=PATCH&status=SENDING\" class=\"btn btn-primary btn-xs confirm-post\"><span class=\"fa fa-send\"></span></a>";
                break;
        }
    }
}
