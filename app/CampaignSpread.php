<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignSpread extends Model
{
    protected $table = 'campaigns_spread';    
    protected $primaryKey = null;
    public $incrementing = false;

    public function scopeSent($query)
    {
    	$query->where('status','!=', 'QUEUE');
        $query->where('status','!=', 'BOUNCED');
    }

    public function scopeOpened($query)
    {
        $query->where('status','!=', 'BOUNCED');
        $query->where('status','!=', 'QUEUE');
        $query->where('status','!=', 'SENT');
        $query->where('status','!=', 'MAILGUN_QUEUE');
    }

    public function scopeBounced($query)
    {
        $query->where('status','=', 'BOUNCED');
    }

    public function scopeClicked($query)
    {
        $query->where('status','=', 'CLICKED');
    }

    public function scopeMailgun($query)
    {
        $query->where('status','=', 'MAILGUN_QUEUE');
    }

    public function scopeQueue($query)
    {
    	$query->where('status','=','QUEUE');
    }

    public function recipient()
    {
    	return $this->belongsTo('App\Recipient','recipient_id');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Campaign','campaign_id');
    }
}
