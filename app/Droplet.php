<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Droplet extends Model
{
    protected $table = 'droplets';

    public function domains()
    {
    	return $this->hasMany('App\DropletDomain','droplet_id');
    }

    public function scopeOnline($query)
    {
    	$query->where('status','=','ONLINE');
    }

    public function scopeRestart($query)
    {
    	$query->where('status','=','RESTART');
    }

    public function scopeOffline($query)
    {
    	$query->where('status','=','OFFLINE');
    }
}
