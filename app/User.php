<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Mail;
use Illuminate\Database\Eloquent\Model;
use Phoenix\EloquentMeta\MetaTrait;

class User extends Authenticatable
{
    use MetaTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $meta_model = 'App\UserMeta';

    public static function boot()
    {
        parent::boot();

        static::created(function($user)
        {
            \cURL::post('https://app.remail.web.id/api/v1/recipient/create', [  'key'   =>   'FS4hIa9uLtkNnjX7jwjJh3hzBaLPvst9bcF1fp4Z',
                                                                                'name'  =>   $user->name,
                                                                                'email' =>   $user->email,
                                                                                'list'   =>   1]);
        });

        // static::updated(function($user)
        // {
            
        // });
    }

    public function sendEmailConfirmation()
    {
        $data['confirmation_url']   =    url('auth/activate/'.\Crypt::encrypt($this->id));

        $email  =  $this->email; 
        $name   =  $this->name;
        Mail::send('email.auth.confirm', $data, function ($m) use ($email, $name) {
            $m->to($email, $name)->subject('Email Confirmation');
        });
    }

    public function campaigns()
    {
        return $this->hasMany('App\Campaign','user_id');
    }

    public function recipients()
    {
        return $this->hasMany('App\Recipient','user_id');
    }

    public function tags()
    {
        return $this->hasMany('App\Tag','user_id');
    }

    public function templates()
    {
        return $this->hasMany('App\Template','user_id');
    }

    public function smtp()
    {
        return $this->hasMany('App\Smtp','user_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction','user_id');
    }

    public function domains()
    {
        return $this->hasMany('App\Domain','user_id');
    }

    public function lists()
    {
        return $this->hasMany('App\ListRecipient','user_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\User','parent_id');
    }

    public function isPremium()
    {
        $now        = \Carbon\Carbon::now();
        $expire     = \Carbon\Carbon::createFromTimeStamp(strtotime($this->expire_at));
        if ($now->diffInDays($expire) < 0) {
            return false;
        }

        return true;
    }

    public function scopePremium($query)
    {
        return $query->where('expire_at','>=',\Carbon\Carbon::now());
    }

    public function scopeVerified($query)
    {
        return $query->where('verified','=',1);
    }

    public function addBalance($ammount)
    {
        $this->balance  +=   $ammount;
        $this->save();
    }

    public function parentReward($transaction   =   false, $price   =   0)
    {
        if ($this->parent()->count()==0) {
            return false;
        }

        $parent     =   $this->parent()->first();
        if ($parent->role=="MERCHANT") {
            $transaction            =   new \App\Transaction;
            $transaction->type      =   "AFFILIATE";
            $transaction->ammount   =   config('remail.price') * 100;
            $transaction->user_id   =   $parent->id;
            $transaction->status    =   "SUCCESS";
            $transaction->save();

            $parent->balance        =   $parent->balance + (config('remail.price') / 2);
            $parent->save();

            $data['name']       =    $parent->name;
            $data['total']      =    config('remail.price') / 2;
            Mail::send('email.affiliateReward', $data, function ($m) use ($parent) {
                $m->to($parent->email, $parent->name)->subject('Reward Affilasi Remail');
            });
        }
        elseif ($parent->role=="RESELLER" && $transaction   ==  true) {
            $transaction            =   new \App\Transaction;
            $transaction->type      =   "AFFILIATE";
            $transaction->ammount   =   floor($price * 0.1);
            $transaction->user_id   =   $parent->id;
            $transaction->status    =   "SUCCESS";
            $transaction->save();

            $parent->balance        =   $parent->balance + $transaction->ammount;
            $parent->save();
        }
    }
}
