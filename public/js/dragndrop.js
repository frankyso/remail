$(document).ready(function(){
	LiamDnD.init();
});

var LiamDnD  	=	{
	currentWidgetLayout:'document',
	currentWidgetEditing:'#document',
	defaultWidgetLayout:'document',
	init:function()
	{
		self = this;
		this.layoutInit();
		this.widgetInit();
		this.sortableInit();
		this.actionInit();
		this.widgetToggleDisplay();
		self.widgetToggleNeedLoad($('.widget-editor-'+self.currentWidgetLayout));
		this.autoSave();

		$("#save").click(function(){
			self.save(true);
		});

		$("#sendPreviewButton").click(function(){
			self.sendPreview();
			$('#sendPreviewModal').modal('hide');
		});
	},
	layoutInit:function()
	{
		/*mengatur ketinggian area agar dapat fullscreen dan juga dapat diresize*/
		screen 	=	$('#dashboard-background').innerHeight();
		navbar 	=	$('.navbar-static-top').innerHeight();
		$('.liamdnd-container').outerHeight(screen - navbar - navbar + 21);

		screen_width  = $('#dashboard-background').innerWidth();

		$(function() {
			preview_minimum_width 	=	screen_width / 2;
			preview_maximum_width	=	$(".preview-area").outerWidth() - 1;
		    $(".preview-area").resizable({
		    	maxWidth: preview_maximum_width,
		    	minWidth: preview_minimum_width,
		    	handles: 'e, w'
		    }).on( "resize", function( event, ui ) {
		    	$(".widget-area").outerWidth(screen_width - ui.size.width);
		    } );
		});


		$('.editor').summernote({
		      height: 200,
		      toolbar: [
		          // [groupName, [list of button]]
				  ['style', ['style']],
		          ['font', ['bold', 'italic', 'underline', 'clear']],
		          ['fontname', ['fontname']],
		          ['fontsize', ['fontsize']],
		          ['color', ['color']],
		          ['para', ['ul', 'ol', 'paragraph']],
		          ['height', ['height']],
		          ['table', ['table']],
		          ['insert', ['link', 'picture', 'hr']],
		          ['view', ['fullscreen', 'codeview','undo','redo']]
		      ],
		      placeholder: 'Ketik Disini...'
		});

		$('.save-widget').on('click',function(){
			self.widgetToggleDisplay();
		});
	},
	widgetInit:function()
	{
		var self = this;
		$(".widget-content-block").draggable({
			revert: "invalid",
			cursor: "move",
			appendTo: 'body',
			containment: 'window',
			scroll: false,
			helper: 'clone',
			connectToSortable:'.drop-area',
			stop:function( event, ui ) {
				self.hideDropAreaBorder();
			},
			cursorAt: { left: 5, top: 5 }
		});
	},
	showDropAreaBorder:function()
	{
		if($(".drop-area").find('.drop-area-wrapper').length == 0)
		{
			$(".drop-area").each(function(){
				wrapper_title 	=	$(this).data('title');
				$(this).wrapInner("<div class='drop-area-wrapper'></div>");
				$('.drop-area-wrapper' ,this).append('<div class="drop-area-wrapper-title">' + wrapper_title + '</div>');
			});
		} 
	},
	hideDropAreaBorder:function()
	{
		$('.drop-area-wrapper-title').unwrap();
		$('.drop-area-wrapper-title').remove();
	},
	appendBlockArea:function(self, ui)
	{
		widget_index 	= 	$(ui.draggable).data('widget');
		type = $('.widget-'+ widget_index +'-block').html();
		// console.log(type);
		ui.draggable.empty();
		// console.log(ui.draggable);
		return ui.draggable.html(type)
	},
	sortableInit:function()
	{
		var self = this;
		$('.drop-area').sortable({
			items: ".can-sort",
			connectWith: ".drop-area, .drop-area-wrapper",
			handle: '.action-sort',
			start: function( event, ui ) {
				self.sortableInit();
				self.showDropAreaBorder();
				// ui.placeholder.height(ui.helper.outerHeight());
				// ui.item.startPos = ui.item.index();
				ui.item.hide();
			},
			stop:function( event, ui ) {
				self.hideDropAreaBorder();
				
				if (ui.item.hasClass("widget-content-block")) {
					widget_index 	= 	ui.item.data('widget');
					type = $('.widget-'+ widget_index +'-block').html();
					// console.log(type);
					ui.item.replaceWith(type);
					ui.item.hide();
				}

				ui.item.fadeIn();

				self.actionInit();
			},
			appendTo: 'body',
			placeholder: "remail-drag-drop-highlight",
			cursorAt: { left: 5, top: 5 }
		}).disableSelection();
	},
	actionInit:function()
	{
		var object = this;
		/*Edit*/
		$('.action-edit').each(function(){
			$(this).unbind().on('click',function(){
				// console.log($(this).parents('table.widget-block'));
				object.widgetToggle($(this).parents('table.widget-block'));
			});
		});

		/*duplicate*/
		$('.action-duplicate').each(function(){
			$(this).unbind().on('click',function(){
				$(this).parents('table.widget-block').clone().appendTo($(this).closest('.drop-area'));
				object.actionInit();
			});
		});

		/*Delete*/
		$('.action-delete').each(function(){
			$(this).unbind().on('click',function(){
				if (confirm("Anda yakin ingin menghapus Block ini?") == true) {
				    $(this).parents('table.widget-block').remove();
				}
				else
				{
					return false;
				}
			});
		});
	},
	widgetToggleDisplay:function(name)
	{
		self = this;
		if (name) {
			// console.log(name);
			$('.widget-editor-'+self.currentWidgetLayout).hide('slide');
			self.currentWidgetLayout 	=	name;
			$('.widget-editor-'+self.defaultWidgetLayout).hide('slide');
			$('.widget-editor-'+self.currentWidgetLayout).show('slide');
		}
		else
		{
			$('.widget-editor-'+self.currentWidgetLayout).hide('slide');
			$('.widget-editor-'+self.defaultWidgetLayout).show('slide');
			self.currentWidgetEditing =	'#document';
		}
		// console.log(self.currentWidgetEditing);
	},
	widgetToggle:function(ui)
	{
		self  = this;

		var index 	= ui.find('.index');
		var type 	= index.data('type');
		var id 	 	= ui.attr('id',type + '-' + self.makeID());

		self.currentWidgetEditing 	=	id;
		self.widgetToggleDisplay(type);
		self.widgetToggleNeedLoad($('.widget-editor-'+self.currentWidgetLayout));
		// console.log(type);
	},
	widgetToggleNeedLoad:function(ui)
	{	
		$('.need-load-style', ui).each(function(){
			var me 			= $(this);
			var style 		= me.data('style-type');
			var form 		= me.data('form-type');
			var action 		= me.data('action') || false;
			var target_style_type	= me.data('target-type') || false;

			// console.log(target_style_type);
			if (target_style_type == "attr") {
				var cssStyle 	= $(me.data('target'), self.currentWidgetEditing).attr(style);
			}
			else if(target_style_type == "html")
			{
				var cssStyle 	= $(me.data('target'), self.currentWidgetEditing).html();
			}
			else if(target_style_type == "button")
			{
				var cssStyle 	= $(me.data('target'), self.currentWidgetEditing).css('background-color');
			}
			else
			{
				var cssStyle 	= $(me.data('target'), self.currentWidgetEditing).css(style);
			}

			if (form=="text") {
				me.val(cssStyle);
				if (me.hasClass('colorpicker')) {
					me.colorpicker('setValue', cssStyle);
				}
			}
			else if (form=="select") {
				me.val(cssStyle);
			}


			me.on('keyup keypress blur change', function(e) {
				if (action) {
					$('.preview-area').append('<style>'+ me.data('target') + "{"+ style + ":"+ $(this).val() +"; }" +'</style>');			    	
			    }
			    else
			    {
			    	if (target_style_type == "attr") {
			    		$(me.data('target'), self.currentWidgetEditing).attr(style, $(this).val());
			    	}
					else if (target_style_type == "html") 
					{
						$(me.data('target'), self.currentWidgetEditing).html($(this).val());
					}
					else if(target_style_type == "button")
					{
						$(me.data('target'), self.currentWidgetEditing).css('background-color',$(this).val());
						$(me.data('target'), self.currentWidgetEditing).css('border-color',$(this).val());
					}
			    	else
			    	{
			    		$(me.data('target'), self.currentWidgetEditing).css(style, $(this).val());
			    	}
			    }
			});
		});

		$('.need-load-text', ui).each(function(){
			var me 			= $(this);
			var text 		= $(".target-text", self.currentWidgetEditing).html();
			me.summernote("code", text);
			me.on("summernote.change", function (we, contents, $editable) { 
			    $(".target-text", self.currentWidgetEditing).html(contents);
			});
		});
	},
	makeID:function()
	{
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 5; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	},
	autoSave:function(){
		self = this;
		setInterval(function(){
			self.save();
		}, 20000);
	},
	save:function(redirect = false){
		var template = $('.preview-area').html();
		$.ajax({
		  type: "POST",
		  url: "/templates/dragndrop/autosave",
		  data: {template_html:template, id:$('#id').val(), subject:$('#subject_email').val(), name:$('#name_template').val()},
		  success: function(data){
		  	// console.log(data);
		  	$.toast({
		  	    heading: '',
		  	    text: data.text,
		  	    icon: data.icon,
		  	    hideAfter : 5000,
		  	    position: 'top-right',
		  	});

		  	if (redirect && data.icon =="success") {
		  		window.location = '/templates';
		  	}
		  },
		  dataType: "json"
		});

		// $.post( "/templates/dragndrop/autosave", {template_html:template, id:$('#id').val()} , function( data ) {
		// 	data = JSON.parse(data);
		// 	console.log(data);
			

		// 	if (redirect && data.icon =="success") {
		// 		window.location = '/templates';
		// 	}
		// });
	},
	sendPreview:function(){
		var email =	$('#emailPreview').val();
		var template = $('.preview-area').html();

		$.ajax({
		  type: "POST",
		  url: "/templates/dragndrop/sendpreview",
		  data: {id:$('#id').val(), email:email,template_html:template},
		  success: function(data){
		  	$.toast({
		  	    heading: '',
		  	    text: data.text,
		  	    icon: data.icon,
		  	    hideAfter : 5000,
		  	    position: 'top-right',
		  	});

		  	if (redirect && data.icon =="success") {
		  		window.location = '/templates';
		  	}
		  },
		  dataType: "json"
		});

		// $.post( "/templates/dragndrop/autosave", {template_html:template, id:$('#id').val()} , function( data ) {
		// 	data = JSON.parse(data);
		// 	console.log(data);
			

		// 	if (redirect && data.icon =="success") {
		// 		window.location = '/templates';
		// 	}
		// });
	}
}
//# sourceMappingURL=dragndrop.js.map
