var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
    mix.sass('connector.scss');

    mix.scripts(['resources/assets/vendor/jquery/dist/jquery.js',
    			'resources/assets/vendor/bootstrap-sass/assets/javascripts/bootstrap.js',
          'resources/assets/vendor/select2/dist/js/select2.full.min.js',
          'resources/assets/vendor/placecomplete/jquery.placecomplete.js',
    			'resources/assets/vendor/bootstrap-filestyle/bootstrap-filestyle.js',
          'resources/assets/vendor/summernote/dist/summernote.min.js',
          'resources/assets/vendor/jquery-sticky/jquery.sticky.js',
          'resources/assets/vendor/wdt-loading/wdtLoading.js',
          'resources/assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
          'resources/assets/vendor/jquery-confirm/jquery.confirm.js',
          'resources/assets/vendor/highcharts/highcharts.js',
          'resources/assets/vendor/highcharts/modules/no-data-to-display.js',
          'resources/assets/vendor/highcharts/modules/exporting.js',
          'resources/assets/vendor/jquery-knob/dist/jquery.knob.min.js',
          'resources/assets/vendor/gmap3/dist/gmap3.js',
          'resources/assets/vendor/jquery-toast-plugin/dist/jquery.toast.min.js',
          'resources/assets/vendor/jquery-dayparts/js/jquery.dayparts.js',
          'resources/assets/vendor/jquery.tablecheckbox/jquery.tablecheckbox.js',
          'resources/assets/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js',
          'resources/assets/vendor/jquery-ui/jquery-ui.js',
    			'resources/assets/js/main.js', 
    			],'public/js/','./');

    mix.scripts(['resources/assets/js/_dragndrop.js'
          ],'public/js/dragndrop.js','./');

    mix.scripts(['resources/assets/vendor/jquery/dist/jquery.js',
          'resources/assets/js/_connector.js'
          ],'public/js/connector.js','./');

    mix.copy('resources/assets/vendor/bootstrap-sass/assets/fonts/', 'public/fonts/');
    mix.copy('resources/assets/vendor/font-awesome/fonts/', 'public/fonts/');
    mix.copy('resources/assets/vendor/Ionicons/fonts/', 'public/fonts/');
});
